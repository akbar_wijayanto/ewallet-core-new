package com.ewallet.service.converters.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.service.contract.bean.BaseAccount;
import com.ewallet.service.converters.api.WebServiceConverter;
import com.ewallet.util.BarcodeGenerator;
import com.google.zxing.WriterException;

@Component("coreUserConverter")
@Scope("prototype")
public class CoreUserConverterImpl implements WebServiceConverter<CoreUser, BaseAccount> {

	@Override
	public CoreUser fromContract(BaseAccount contract) throws RetailException {
		if(contract==null) return null;
		CoreUser coreUser = new CoreUser();
		// TODO convert from web service object to model object
		return coreUser;
	}

	@Override
	public BaseAccount toContract(CoreUser model) throws RetailException {
		if(model==null) return null;
		BaseAccount baseAccount = new BaseAccount();
		baseAccount.setId(model.getId());
		baseAccount.setEmail(model.getEmail());
		baseAccount.setFirstName(model.getFirstName());
		baseAccount.setLastName(model.getLastName());
		baseAccount.setName(model.getFirstName() + " " + model.getLastName());
//		baseAccount.setLimitAmount(model.getLimitAmount());
		baseAccount.setUsername(model.getUsername());
		baseAccount.setStatus(model.getStatus());
		baseAccount.setPhoneNumber(model.getPhoneNumber());
//		baseAccount.setCompany(model.getCompany());
		baseAccount.setAccountEnabled(model.getAccountEnabled());
//		if (model.getSva()!=null) {
			try {
				byte[] image = BarcodeGenerator.generateQRCodeToByteArray(model.getPhoneNumber());
				String url = "data:image/png;base64," + Base64.encodeBase64String(image);
				baseAccount.setQrCode(url);
			} catch (WriterException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
//		}
		return baseAccount;
	}

	@Override
	public List<CoreUser> fromContract(List<BaseAccount> contract)
			throws RetailException {
		return null;
	}

	@Override
	public List<BaseAccount> toContract(List<CoreUser> model)
			throws RetailException {
		List<BaseAccount> baseAccounts = new ArrayList<BaseAccount>();
		for(CoreUser coreUser : model){
			baseAccounts.add(toContract(coreUser));
		}
		return baseAccounts;
	}

}
