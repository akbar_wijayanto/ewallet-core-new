/**
 * 
 */
package com.ewallet.service.converters.api;

import java.util.List;

import com.ewallet.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Jan 29, 2015 3:01:58 PM
 */
public interface WebServiceConverter<M extends Object, C extends Object> {

	M fromContract(C contract) throws RetailException;
	C toContract(M model) throws RetailException;

	List<M> fromContract(List<C> contract) throws RetailException;
	List<C> toContract(List<M> model) throws RetailException;
}
