/**
 * 
 */
package com.ewallet.service.converters.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.contract.bean.Transaction;
import com.ewallet.service.converters.api.WebServiceConverter;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto
 * Date Nov 3, 2015 3:41:51 PM
 */
@Component("transactionHistoryConverter")
public class TransactionHistoryConverterImpl implements WebServiceConverter<TrxRecord, Transaction>{

	@Override
	public TrxRecord fromContract(Transaction contract) throws RetailException {
		return null;
	}

	@Override
	public Transaction toContract(TrxRecord model) throws RetailException {
		if(model==null) return null;
		Transaction contract = new Transaction();
		contract.setAccountNumber(model.getAccountNumber());
		contract.setAmount(model.getAmount());
		contract.setCustomerName(model.getCoreUser().getFirstName().concat(" ").concat(model.getCoreUser().getLastName()));
		contract.setId(model.getId().toString());
		contract.setPairedAccount(model.getPairedAccount());
		contract.setSellerName(model.getCoreSeller().getFirstName().concat(" ").concat(model.getCoreSeller().getLastName()));
		if (model.getTransactionDate()!=null) {
			contract.setTransactionDate(DateUtil.toXMLGregorian(model.getTransactionDate()));
		}
		contract.setTransactionId(model.getTransactionId());
		contract.setTransactionType(model.getTransactionType());
		return contract;
	}

	@Override
	public List<TrxRecord> fromContract(List<Transaction> contract)
			throws RetailException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transaction> toContract(List<TrxRecord> model)
			throws RetailException {
		// TODO Auto-generated method stub
		return null;
	}

}
