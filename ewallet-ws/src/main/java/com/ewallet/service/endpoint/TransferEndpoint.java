/**
 * 
 */
package com.ewallet.service.endpoint;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.service.VirtualAccountManager;
import com.ewallet.service.contract.AccountInformationRequest;
import com.ewallet.service.contract.AccountInformationResponse;
import com.ewallet.service.contract.ConfirmTransferRequest;
import com.ewallet.service.contract.ConfirmTransferResponse;
import com.ewallet.service.contract.CreateTransferRequest;
import com.ewallet.service.contract.CreateTransferResponse;
import com.ewallet.util.AesUtil;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto
 *
 */
@Endpoint
public class TransferEndpoint extends BaseEndpoint {

	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private VirtualAccountManager virtualAccountManager;
	
	@PayloadRoot(localPart = "CreateTransferRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload CreateTransferResponse createTrf(@RequestPayload CreateTransferRequest request) throws RetailException {
		CreateTransferResponse response = new CreateTransferResponse();

		try {
			CoreUser coreUser = coreUserManager.getUserByUsername(request.getAccountFrom());
			if(coreUser==null) throw new RetailException("trf01 - Customer not found");
			
			VirtualAccount accountTo = virtualAccountManager.get(request.getAccountTo());
			if(accountTo==null) throw new RetailException("trf02 - Destination account not found");
			
			TrxRecord record = new TrxRecord();
			String trxId = DateUtil.toBundleDateFormat(new Date());
			record.setTransactionId(trxId);
			record.setAccountNumber(coreUser.getPhoneNumber());
			record.setPairedAccount(accountTo.getId());
			record.setAmount(request.getAmount());
			record.setTransactionDate(new Date());
			record.setTransactionType("TRANSFERQR");
			record.setCoreUser(coreUser);
			record.setStatus(RecordStatusEnum.INAU.getName());
			
			record = trxRecordManager.save(record);
			
			response.setAccountFrom(record.getAccountNumber());
			response.setAccountFromName(coreUser.getFullName());
			response.setAccountTo(record.getPairedAccount());
			response.setAccountToName(accountTo.getCoreUser().getFullName());
			response.setAmount(record.getAmount());
			response.setReference(record.getReference());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RetailException("trf05 - Transaction Failed");
		}
		return response;
	}
	
	@PayloadRoot(localPart = "ConfirmTransferRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ConfirmTransferResponse confirmTrf(@RequestPayload ConfirmTransferRequest request) throws RetailException {
		ConfirmTransferResponse response = new ConfirmTransferResponse();
		TrxRecord record = trxRecordManager.getByTransactionReference(request.getReference());
		if(record!=null){
			// validasi pin
			CoreUser coreUser = record.getCoreUser();
			AesUtil aesUtil = new AesUtil();
			if(coreUser.getToken().equals(aesUtil.encrypt(aesUtil.SALT, request.getPin()))){
				record.setStatus(RecordStatusEnum.LIVE.getName());
				record = trxRecordManager.save(record);
				
				// transaction
				virtualAccountManager.debit(record.getAccountNumber(), record.getAmount());
				virtualAccountManager.credit(record.getPairedAccount(), record.getAmount());
				
				response.setReference(record.getReference());
				response.setTransactionId(record.getTransactionId());
			} else {
				throw new RetailException("trf06 - Invalid pin");
			}
		}
		return response;
	}
	
}