/**
 * 
 */
package com.ewallet.service.endpoint;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;

import com.ewallet.util.BarcodeGenerator;
import com.google.zxing.WriterException;

/**
 * @author akbar.wijayanto
 * Date Sep 11, 2015 5:46:45 PM
 */
public class BaseEndpoint {

	protected static final String TARGET_NAMESPACE = "http://ewallet.com/service/contract";

	String result = "";
	InputStream inputStream;

	public String getText(String msgCode) {
		try {
			Properties prop = new Properties();
			String propFileName = "errormessage.properties";
			inputStream = getClass().getClassLoader().getResourceAsStream(
					propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName
						+ "' not found in the classpath");
			}
			
			result = prop.getProperty(msgCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public String generateByteCode(String object) {
		try {
			byte[] image = BarcodeGenerator.generateQRCodeToByteArray(object);
			String url = "data:image/png;base64," + Base64.encodeBase64String(image);
			return url;
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
