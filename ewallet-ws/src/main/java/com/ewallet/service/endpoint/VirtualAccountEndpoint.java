/**
 * 
 */
package com.ewallet.service.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.VirtualAccountManager;
import com.ewallet.service.contract.AccountInformationRequest;
import com.ewallet.service.contract.AccountInformationResponse;

/**
 * @author akbar.wijayanto
 *
 */
@Endpoint
public class VirtualAccountEndpoint extends BaseEndpoint {

	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private VirtualAccountManager virtualAccountManager;
	
	@PayloadRoot(localPart = "AccountInformationRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload AccountInformationResponse getBalance(@RequestPayload AccountInformationRequest request) throws RetailException {
		AccountInformationResponse response = new AccountInformationResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if(coreUser!=null){
			VirtualAccount account = virtualAccountManager.get(coreUser.getPhoneNumber());
			if(account!=null){
				response.setAccountNumber(account.getId());
				response.setAccountStatus(account.getAccountStatus());
				response.setBalance(account.getBalance());
			}
		}
		
		return response;
	}
	
}