/**
 * 
 */
package com.ewallet.service.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.service.contract.GetTransactionHistoryRequest;
import com.ewallet.service.contract.GetTransactionHistoryResponse;
import com.ewallet.service.contract.bean.BaseAccount;
import com.ewallet.service.contract.bean.Transaction;
import com.ewallet.service.converters.api.WebServiceConverter;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 10:58:39 PM
 */
@Endpoint
public class TransactionEndpoint extends BaseEndpoint {

	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired
	@Qualifier("transactionHistoryConverter")
	private WebServiceConverter<TrxRecord, Transaction> transactionHistoryConverter;
	
	@PayloadRoot(localPart = "GetTransactionHistoryRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetTransactionHistoryResponse getTransactionHistory(@RequestPayload GetTransactionHistoryRequest request) throws RetailException {
		GetTransactionHistoryResponse response = new GetTransactionHistoryResponse();
		
//		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
//		if (coreUser==null) {
//			throw new RetailException("01 - Customer not found.");
//		}
		
		List<TrxRecord> trxRecords = trxRecordManager.getByUsername(request.getUsername());
		for (TrxRecord trxRecord : trxRecords) {
			response.getTransaction().add(transactionHistoryConverter.toContract(trxRecord));
		}
		
		return response;
	}
	
}