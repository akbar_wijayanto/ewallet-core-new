/**
 * 
 */
package com.ewallet.service.endpoint;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.ewallet.businesslogic.util.MailObject;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.Feedback;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.CoreRoleManager;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.FeedbackManager;
import com.ewallet.service.VirtualAccountManager;
import com.ewallet.service.contract.ActivationRequest;
import com.ewallet.service.contract.ActivationResponse;
import com.ewallet.service.contract.CreateFeedbackRequest;
import com.ewallet.service.contract.CreateFeedbackResponse;
import com.ewallet.service.contract.GetUserPreferenceRequest;
import com.ewallet.service.contract.GetUserPreferenceResponse;
import com.ewallet.service.contract.LoginRequest;
import com.ewallet.service.contract.LoginResponse;
import com.ewallet.service.contract.ProfileRequest;
import com.ewallet.service.contract.ProfileResponse;
import com.ewallet.service.contract.RegistrationRequest;
import com.ewallet.service.contract.RegistrationResponse;
import com.ewallet.service.contract.SettingUserPreferenceRequest;
import com.ewallet.service.contract.SettingUserPreferenceResponse;
import com.ewallet.service.contract.bean.BaseAccount;
import com.ewallet.service.contract.bean.KeyValuePair;
import com.ewallet.service.contract.bean.UserDetail;
import com.ewallet.service.converters.api.WebServiceConverter;
import com.ewallet.util.AesUtil;
import com.ewallet.util.NameUtil;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 6:35:23 PM
 */
@Endpoint
public class CustomerEndpoint extends BaseEndpoint {

	@Value("${domain}") 		protected String DOMAIN;
	@Value("${subdomain}") 		protected String SUBDOMAIN;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private FeedbackManager feedbackManager;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	@Qualifier("coreUserConverter")
	private WebServiceConverter<CoreUser, BaseAccount> coreUserConverter;
	
	@Autowired
	@Qualifier("mailObject")
	private MailObject mailObject;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private CoreRoleManager coreRoleManager;
	
	@Autowired
	private VirtualAccountManager virtualAccountManager;
	
	@PayloadRoot(localPart = "LoginRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload LoginResponse loginCustomer(@RequestPayload LoginRequest request) throws RetailException {
		LoginResponse response = new LoginResponse();

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					request.getUsername(), request.getPassword()));
		} catch (Exception e) {
			throw new RetailException("0000 - "+e.getMessage());
		}
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		
		if (coreUser!=null) {
			if (!coreUser.getAccountEnabled()) {
				throw new RetailException("1002 - Your account not activated for this application. Please activation first");
			}
			response.setUser(coreUserConverter.toContract(coreUser));
		} else {
			throw new RetailException("1001 - Customer not found.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "ProfileRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload ProfileResponse profil(@RequestPayload ProfileRequest request) throws RetailException {
		ProfileResponse response = new ProfileResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("1001 - Customer not found.");
		} else {
			UserDetail userDetail = new UserDetail();
			userDetail.setEmail(coreUser.getEmail());
			userDetail.setFirstName(coreUser.getFirstName());
			userDetail.setId(coreUser.getId().toString());
			userDetail.setLastName(coreUser.getLastName());
			userDetail.setUsername(coreUser.getUsername());
			userDetail.setAccountEnabled(coreUser.getAccountEnabled());
//			userDetail.setCompany(coreUser.getCompany());
//			userDetail.setQrCode(generateByteCode(coreUser.getSva()));
			// Get account balance dan set ke KeyValuePair -> key : accountNumber, value : balance
//			AccountDto dto = accountAdapter.balanceInquiry(coreUser.getSva());
			KeyValuePair keyValuePair = new KeyValuePair();
//			keyValuePair.setKey(dto.getAccountNumber());
//			keyValuePair.setValue(dto.getAmount().toString());
//			userDetail.getBalance().add(keyValuePair);
//			userDetail.setAsMember(dto.isAsMember());
			response.setUserDetail(userDetail);
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "SettingUserPreferenceRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload SettingUserPreferenceResponse settingUserPreference(@RequestPayload SettingUserPreferenceRequest request) throws RetailException {
		SettingUserPreferenceResponse response = new SettingUserPreferenceResponse();
		
		// Get account ke kkas dan set untuk jumlah limit payroll nya
//		AccountDto dto = new AccountDto();
//		dto.setAccountNumber(request.getAccountNumber());
//		dto.setLimitPayroll(request.getLimitPayroll());
//		dto.setAllowedPayroll(request.isAllowedPayroll());
//		
//		dto = accountAdapter.updateLimitPayroll(dto);
		
//		response.setAccountNumber(dto.getAccountNumber());
//		response.setAllowedPayroll(dto.isAllowedPayroll());
//		response.setLimitPayroll(dto.getLimitPayroll());
		
		return response;
	}

	@PayloadRoot(localPart = "CreateFeedbackRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload CreateFeedbackResponse createFeedback(@RequestPayload CreateFeedbackRequest request) throws RetailException {
		CreateFeedbackResponse response = new CreateFeedbackResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("01 - Customer not found.");
		}
		
		Feedback feedback = new Feedback();
		feedback.setCoreUser(coreUser);
		feedback.setMessage(request.getMessage());
		feedback.setRating(request.getRating());
		
		switch (request.getType()) {
		case 0: // for seller
			feedback.setCoreSeller(coreSellerManager.getByQRCode(request.getSellerId()));
			break;
		case 1:
			feedback.setManagement("1");
			break;
		default:
			break;
		}
		
		feedback = feedbackManager.save(feedback);
		
		if (feedback != null) {
			response.setStatus("3601 - Success.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "GetUserPreferenceRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetUserPreferenceResponse getUserPreference(@RequestPayload GetUserPreferenceRequest request) throws RetailException {
		GetUserPreferenceResponse response = new GetUserPreferenceResponse();
		
		CoreUser coreUser = coreUserManager.getUserByUsername(request.getUsername());
		if (coreUser==null) {
			throw new RetailException("01 - Customer not found.");
		}
		
		return response;
	}
	
	@PayloadRoot(localPart = "RegistrationRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload RegistrationResponse register(@RequestPayload RegistrationRequest request) throws RetailException {
		RegistrationResponse response = new RegistrationResponse();
		
		CoreUser coreUser = new CoreUser();
		coreUser.setFirstName(NameUtil.getName(request.getFullName()));
		coreUser.setLastName(NameUtil.getSurname(request.getFullName()));
		coreUser.setPhoneNumber(request.getPhoneNumber());
		coreUser.setEmail(request.getEmail());
		coreUser.setUsername(request.getUsername());
		coreUser.setPassword(request.getPassword());
		coreUser.setSessionTimeout(10L);
		coreUser.setActiveRole(coreRoleManager.get(2));
		coreUser.setAccountEnabled(true);
		AesUtil aesUtil = new AesUtil();
		coreUser.setToken(aesUtil.encrypt(aesUtil.SALT, request.getPin()));
		
		coreUser = coreUserManager.save(coreUser);
		
		VirtualAccount account = new VirtualAccount();
		account.setId(coreUser.getPhoneNumber());
		account.setAccountStatus(true);
		account.setCoreUser(coreUser);
		account = virtualAccountManager.save(account);
		
		response.setEmail(coreUser.getEmail());
		response.setFullName(coreUser.getFirstName().concat(coreUser.getLastName()));
		response.setPhoneNumber(coreUser.getPhoneNumber());
		response.setUsername(coreUser.getUsername());
		
		return response;
	}
}
