<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <%@ include file="/common/meta.jsp" %>
    <title><decorator:title/> | <fmt:message key="webapp.name"/></title>
    <link rel="stylesheet" href="<c:url value='/styles/AdminLTE.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/bootstrap.css' />" />
    <link rel="stylesheet" href="<c:url value='/styles/font-awesome.css'/>" />
    <link rel="stylesheet" href="<c:url value='/styles/inonicons.min.css' />" />
	
	<script type="text/javascript" src="<c:url value='/scripts/jquery.min.js' />"></script>

    <script type="text/javascript">
        <%@ include file="/scripts/variable.js"%>
    </script>
    <%-- <script type="text/javascript" src="<c:url value='/scripts/common.js' />"></script> --%>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<c:url value='/styles/ie/ie.css' />" />
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="<c:url value='/scripts/ie/html5.js' />"></script>
    <script src="<c:url value='/scripts/ie/respond.min.js' />"></script>
    <script src="<c:url value='/scripts/excanvas.min.js' />"></script>
    <![endif]-->

    <decorator:head/>
</head>
<body class="skin-orange">
    <div class="wrapper row-offcanvas row-offcanvas-left" style='background : #f9f9f9;'>
    		 <!-- Content Header (Page header) -->
             <section class="content-header">  
                 <ol class="breadcrumb">
                 	 <i class="fa fa-dashboard"></i>&nbsp;
                 	 <c:set var="crumbs" value=""/>
	             	 <c:forEach  var="entry" items="${CB_BREADCRUMBS}">
	                     <c:set var="crumbs" value="${crumbs}/${entry}"/>
	                     <li>
	                         <a href="<c:url value='${crumbs}' />"><fmt:message key="menu.${entry}" /></a>
	                     </li>
	                 </c:forEach>
                 </ol>
             </section>
             
             <section class="content">
				<jsp:include page="/common/messages.jsp"/>        
	            <decorator:body/>	            	            
             </section>
             <jsp:include page="/common/footer.jsp"/>
    </div>
	
    <script type="text/javascript" src="<c:url value='/scripts/jquery-ui-1.10.3.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/bootstrap.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/scripts/AdminLTE/app.js' />"></script>    
</body>
</html>
