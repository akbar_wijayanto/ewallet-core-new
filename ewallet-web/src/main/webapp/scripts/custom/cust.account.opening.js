jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	$('#save').click(function(){
		$('#customerAccountOpening').validationEngine();
	});
	$('#cancel').click(function(){
		$('#customerAccountOpening').validationEngine('detach');
	});

	$('form').on('change', '#rate', function () {
		getRate();
	});

	$('form').on('change', '#specialRate', function () {
		getRate();
	});

	function getRate(){
		var rate = parseFloat($('#rate').val());
		var specialRate =parseFloat($('#specialRate').val());
		var finalRate =parseFloat(rate + specialRate);

		$('#finalRate').val(finalRate);
	}

	/*$('form').on('change', '#productId', function () {

		if($(this).val()){
			var url = base_url +"/core/product/parameter/interest/page/"+$(this).val();
			$('#tableRate').dataTable().fnDestroy();
			$('#tableRate').dataTable( {
				"aoColumnDefs": [
				                 { "mDataProp": "interestBaseId.baseCode", "aTargets": [0] },
				                 { "mDataProp": "interestBaseId.rate", "aTargets": [1] }
				                 ],
				                 "bFilter": false,
				                 "bSort": false,
				                 "bProcessing": true,
				                 "bServerSide": true,
				                 "sAjaxSource": url,
				                 "bScrollCollapse": true
			});
		}
	});*/
	
	
/*	$("#overdraft").on("change", function() {
		checkoverdraft();
	});
	$("#overdraft").click({
		alert("test");
	});*/

	$("#limit").hide();
	$('#overdraft').on('ifUnchecked', function(event){
		 
		$("#limit").hide();
	});
	$('#overdraft').on('ifChecked', function(event){
		 
		$("#limit").show();
		});
	
	$("#productId").click(function () {
		 
		getCurrency($('#productId').val());
		getCharges($('#productId').val());
		getTax($('#productId').val());
	});
	
	$("#debetSpecialRate").change(function () {
		
		getFinalDebet();
		
	});
	function getFinalDebet(){
		var debetRate= $("#debetRate").val();
		var debetSpecialRate =$("#debetSpecialRate").val()
		var debetFinalRate = parseFloat(debetRate) + parseFloat(debetSpecialRate);
		$("#debetFinalRate").val(debetFinalRate);
		
	}
	
	$("#creditSpecialRate").change(function () {
		
		getFinalCredit();
		
	});
	function getFinalCredit(){
		var creditRate= $("#creditRate").val();
		var creditSpecialRate =$("#creditSpecialRate").val()
		var creditFinalRate = parseFloat(creditRate) + parseFloat(creditSpecialRate);
		$("#creditFinalRate").val(creditFinalRate);
		
	}
	
	$("#overdraftSpecialRate").change(function () {
		
		getFinalOverdraft();
		
	});
	function getFinalOverdraft(){
		var overdraftRate= $("#overdraftRate").val();
		var overdraftSpecialRate =$("#overdraftSpecialRate").val()
		var overdraftFinalRate = parseFloat(overdraftRate) + parseFloat(overdraftSpecialRate);
		$("#overdraftFinalRate").val(overdraftFinalRate);
		
	}
	function getCurrency(productId){
		
		if (productId != "") {
			 
			$.post(base_url + "customer/accountopening/getCurrency", {productId: productId}, function(data){
				if (data != "") {
					 
					$("#currency2").val(data[0]);
					$("#debetRate").val(data[1]);
					$("#creditRate").val(data[2]);
					$("#overdraftRate").val(data[3]);
					getFinalCredit();
					getFinalDebet();
					getFinalOverdraft();
				} else {
					alert("Data not found!");
				}
			});
		} else {
			//alert("Please fill field account!");
		}
	} 
	
	function getCharges(productId){
		if (productId != "") {
			$.post(base_url + "customer/accountopening/getCharges", {productId: productId}, function(data){
				if (data != "") {
//					$('#selectedTable tbody').remove();
//					var a= "<tbody>";
//					$.each(data, function (i, item) {
//						 a +="<tr>"; 
//						 if(item.checklistStatus == true){
//							 a +="<td><input type=\"checkbox\" name=\"chargeCode\" value=\""+item.chargeId+"\" checked></td>"; 
//						 }else{
//							 a +="<td><input type=\"checkbox\" name=\"chargeCode\" value=\""+item.chargeId+"\" ></td>";
//						 }
//						 a +="<td>"+item.chargeId+"</td>";
//						 a +="<td>"+item.description+"</td>";
//						 a +="<td>"+item.chargeRate+"</td>";
//						 a +="<td>"+item.chargeAmount+"</td>";
//						 a +="</tr>"; 
//						
//					  });
//					  a += "</tbody>";
//					 
//					   $("#selectedTable").append(a)
					  //alert(data);
					  $.each(data, function (i, item) {
						  if(item.checklistStatus == true){
							  alert(item.chargeId);
							  $('#'+item.chargeId).iCheck('check');
						  }
					  });
					
				} else {
					 
				}
			});
		} else {
			//alert("Please fill field account!");
		}
	} 
	
	function getTax(productId){
		
		if (productId != "") {
			 
			$.post(base_url + "customer/accountopening/getTax", {productId: productId}, function(data){
				if (data != "") {
					$('#taxList tbody').remove();
					var a= "<tbody>";
					$.each(data, function (i, item) {
						 a +="<tr>"; 
						 a +="<td>"+item.typeInterest+"</td>";
						 a +="<td>"+item.taxCode+"</td>";
						 a +="<td>"+item.taxName+"</td>";
						 a +="<td>"+format2(item.percentage);+"</td>";
						 a +="<td>"+format2(item.minimumBalance);+"</td>";
						 a +="</tr>"; 
						
					  });
					  a += "</tbody>";
					 
					   $("#taxList").append(a)
					  
					
				} else {
					
				}
			});
		} else {
			//alert("Please fill field account!");
		}
	} 
/*	function checkoverdraft() {
		alert(overdraft);
		var overdraft = $("#overdraft").val();
		if (overdraft == 1) {
			
		} else{
			
		}
	}*/
	
	
	/*$('#idCif').keydown(function(event){
	     if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	                // Allow: Ctrl+A
	               (event.keyCode == 65 && event.ctrlKey === true) || 
	                // Allow: home, end, left, right
	               (event.keyCode >= 35 && event.keyCode <= 39) ||
	               // Allow: Ctrl C
	               (event.ctrlKey==true && event.keyCode == 67)||
	               // Allow: Ctrl V
	               (event.ctrlKey==true && event.keyCode == 86))
	      {
	                    // let it happen, don't do anything
	                    return;
	           }
	           else {
	               // Ensure that it is a number and stop the keypress
	               if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                   event.preventDefault(); 
	               }   
	           }
	    });
	    
	    $('#printedName').keydown(function(event){
	     if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 
	       || event.keyCode == 13 || event.keyCode == 32 || event.keyCode == 44 ||
	                // Allow: Ctrl+A
	               (event.keyCode == 65 && event.ctrlKey == true) || 
	                // Allow: home, end, left, right
	               (event.keyCode >= 35 && event.keyCode <= 39) ||
	               // Allow: Ctrl C
	               (event.ctrlKey==true && event.keyCode == 67)||
	               // Allow: Ctrl V
	               (event.ctrlKey==true && event.keyCode == 86))
	         
	      {
	                    // let it happen, don't do anything
	                    return;
	           }else{
	       if((event.keyCode < 65 && event.keyCode > 57)||(event.shiftKey && (event.keyCode >= 48 && event.keyCode <= 57))||event.keyCode>90){
	        event.preventDefault();
	       }
	           }
	    });*/
	function format2(n) {
	    return n.toFixed(4).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}
});
