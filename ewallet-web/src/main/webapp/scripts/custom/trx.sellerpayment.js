$(document).ready(function() {
	$.ajaxSetup ({
		cache: false
	});
	
	$('#saveRefund').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var sellerId=$('#sellerId').val();
        $('#trxRefund').validationEngine({promptPosition : "bottomLeft"});
		if ($('#trxRefund').validationEngine('validate')) {
			$.post(base_url+'trx/refund',{sellerId:sellerId,startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		$('#sellerId').val()=sellerId;
		return false;
    });
	
	$('#saveOrder').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var sellerId=$('#sellerId').val();
        $('#trxOrder').validationEngine({promptPosition : "bottomLeft"});
		if ($('#trxOrder').validationEngine('validate')) {
			$.post(base_url+'trx/order',{sellerId:sellerId,startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		$('#sellerId').val()=sellerId;
		return false;
    });
	
	$('#saveSeller').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var sellerId=$('#sellerId').val();
        $('#coreSeller').validationEngine({promptPosition : "bottomLeft"});
		if ($('#coreSeller').validationEngine('validate')) {			
			$.post(base_url+'core/seller/report/trxseller',{sellerId:sellerId,startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		$('#sellerId').val()=sellerId;
		return false;
    });
	
	$('#saveBuyer').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
        $('#trxBuyer').validationEngine({promptPosition : "bottomLeft"});
		if ($('#trxBuyer').validationEngine('validate')) {
			$.post(base_url+'core/user/buyer',{sellerId:sellerId,startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		return false;
    });
	
	$('#saveSettle').click(function(){
		/*var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
        $('#trxSellPay').validationEngine({promptPosition : "bottomLeft"});
		if ($('#trxSellPay').validationEngine('validate')) {
			$.post(base_url+'trx/seller/payment/infoSettle',{startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		return false;*/
		window.location = base_url+'trx/seller/payment/infoSettle?startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
    });
	
	$('#saveSettleReport').click(function(){
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
        $('#settleReport').validationEngine({promptPosition : "bottomLeft"});
		if ($('#settleReport').validationEngine('validate')) {
			$.post(base_url+'trx/seller/payment/history',{startDate:startDate,endDate:endDate});
		}
		$('#startDate').val()=startDate;
		$('#endDate').val()=endDate;
		return false;
    });
	
	$("#downloadpdf").click(function(){
    	window.location = base_url+'trx/seller/payment/download/all/pdf/';
    });
	
	$("#downloadxls").click(function(){
		window.location = base_url+'trx/seller/payment/download/all/xls/';
	});
	
	$("#trxSellerSettle").click(function(){
		//window.location = base_url+'trx/seller/payment/settle?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
		window.location = base_url+'trx/seller/payment/settle';
	});
	
	$("#trxsellerdownloadxls").click(function(){
		window.location = base_url+'core/seller/download/all/xls/trxseller?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#trxsellerdownloadpdf").click(function(){
		window.location = base_url+'core/seller/download/all/pdf/trxseller?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#settlementdownloadxls").click(function(){
		window.location = base_url+'trx/seller/payment/history/download/all/xls/settlement?startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#settlementdownloadpdf").click(function(){
		window.location = base_url+'trx/seller/payment/history/download/all/pdf/settlement?startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#downloadhistoryxls").click(function(){
		window.location = base_url+'trx/seller/payment/history/download/xls/history';
	});
	
	$("#downloadhistorypdf").click(function(){
		window.location = base_url+'trx/seller/payment/history/download/pdf/history';
	});
	
	$("#trxorderdownloadxls").click(function(){
	  window.location = base_url+'trx/order/download/all/xls?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	 
	$("#trxorderdownloadpdf").click(function(){
	  window.location = base_url+'trx/order/download/all/pdf?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#trxrefunddownloadxls").click(function(){
	  window.location = base_url+'trx/refund/download/all/xls/trxrefund?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	 
	$("#trxrefunddownloadpdf").click(function(){
		window.location = base_url+'trx/refund/download/all/pdf/trxrefund?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#trxbuyerdownloadxls").click(function(){
		window.location = base_url+'core/user/buyer/download/all/xls?sellerId='+$('#sellerId').val()+'&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
	
	$("#trxbuyerdownloadpdf").click(function(){
		window.location = base_url+'core/user/buyer/download/all/pdf?startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val();
	});
});
