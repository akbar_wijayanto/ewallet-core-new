$(document).ready(function() {
	$.ajaxSetup ({
		cache: false
	});
	
	$("#downloadpdf").click(function(){
    	window.location = base_url+'trx/buyer/payment/download/all/pdf/';
    });
	
	$("#downloadxls").click(function(){
		window.location = base_url+'trx/buyer/payment/download/all/xls/';
	});
	
	$("#trxbuyerdownloadxls").click(function(){
		window.location = base_url+'core/user/buyer/download/all/xls';
	});
	
	$("#trxbuyerdownloadpdf").click(function(){
		window.location = base_url+'core/user/buyer/download/all/pdf';
	});
});