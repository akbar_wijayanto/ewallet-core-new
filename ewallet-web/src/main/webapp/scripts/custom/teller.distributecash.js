$(document).ready(function(){
	doCheck();
	function doCheck (){
		var currency = $('#currency').val();
		 
		$.post(base_url + "trx/vault/check", {currency: currency}, function(data){
			if(data == "" ){
				$('#success').css("display","block");
				$('#failed').css("display","none");
			}else{
				$("#validAmount").val(data);
				$('#success').css("display","none");
				$('#failed').css("display","block");
				$('#tellerId').val(null);
			}
		});
	}
	 
		
 
	 
	
	
	
	$('#cancel').click(function(){
		$('#trxVault').validationEngine('detach');
	});
	
	$('body').on('click','#addDenom',function(index){   	 	
    	$('#denoms tbody tr:last').clone().insertAfter('#denoms tbody tr:last').slideDown(function(){
            var elements = $(this).find("input");
			for(i=0; i<elements.length; i++){	            	
				$(elements[i]).val('');
				getElementDenom();
             }					
		});
    });
	
	function getElementDenom(){
		$('#denoms tbody tr').each(function(index) {
			$(this).find("#denominationMap\\.denom").attr("name", "denominationMappings["+index+"].denomination.id");
			$(this).find("#denominationMap\\.quantity").attr("name", "denominationMappings["+index+"].quantity");
		});	
	}

	$('body').on('click','#delDenom',function() {
		if($('#denoms tbody tr').length > 1){
			$(this).parent().parent().remove();
			getElementDenom();
		}			
	});
	
//	$('.release-calculation').on('keyup', function(){
//		var amount = $(this).parent().parent().find('#denomination\\.nominal').val();
//		var totalAmount = parseFloat(amount.replace(/,/g,"").replace(".",",")) * parseFloat($(this).val());
//		$(this).parent().parent().find('#denomination\\.totalAmount').val(totalAmount);
//		$(this).parent().parent().find('#denomination\\.totalAmount').number(true, 2);
//		
//		var grandTotal = parseFloat(0);
//		$('.denom-totalAmount').each(function(i, obj) {
//		   grandTotal = parseFloat(grandTotal) + parseFloat(obj.value.replace(/,/g,"").replace(".",","));
//		});
//		
//		$('#grandTotal').val(grandTotal);
//		$('#grandTotal').number(true, 2);
//	});
	
	$('.release-calculation').on('keyup', function(){
		var amount = $(this).parent().parent().find('#denomination\\.nominal').text();
		var totalQty = parseFloat($(this).val().replace(/,/g,"").replace(".",",")) / parseFloat(amount.replace(/,/g,"").replace(".",",")) ;
		$(this).parent().parent().find('#denomination\\.quantity').text(totalQty);
		
		var grandTotal = parseFloat(0);
		$('.denom-totalAmount').each(function(i, obj) {
		   grandTotal = parseFloat(grandTotal) + parseFloat(obj.value.replace(/,/g,"").replace(".",","));
		});
		
		$('#grandTotal').val(grandTotal);
		$('#grandTotal').number(true, decimalPoint);
	});
	
	initialize();
	
});

function equaliser(field, rules, i, options) {
	var grandTotal = field.val();
	var totalAmount = $('#totalAmount').val();
	console.log(grandTotal);
	console.log(totalAmount);
	if (totalAmount != grandTotal)
		return options.allrules.equaliserAmount.alertText;
}

function initialize(){
	$('.release-calculation').each(function(idx, objx) {
		var amount = $(this).parent().parent().find('#denomination\\.nominal').text();
		console.log(amount);
		console.log($(this).val());
		var totalQty = parseFloat($(this).val().replace(/,/g,"").replace(".",",")) / parseFloat(amount.replace(/,/g,"").replace(".",",")) ;
		$(this).parent().parent().find('#denomination\\.quantity').text(totalQty);
		
		var grandTotal = parseFloat(0);
		$('.denom-totalAmount').each(function(i, obj) {
		   grandTotal = parseFloat(grandTotal) + parseFloat(obj.value.replace(/,/g,"").replace(".",","));
		});
		
		$('#grandTotal').val(grandTotal);
		$('#grandTotal').number(true, decimalPoint);
	});
}