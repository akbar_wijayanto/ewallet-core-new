$(document).ready(function(){
	$('.closing').css("display", "none");

	$('#checkaccount').click(function(){
		var account_number = $('.account').val();
		$.post(base_url + "customer/closingaccount/getclosingaccountdto", {accountnumber: account_number}, function(datas) {
			if(datas !=null){
				$('.closing').css("display", "block");
				$('#codeAccount').val(datas.codeAccount);
				$('#accountId').val(datas.accountId);
				$('#name').val(datas.name);
				$('#address').val(datas.address);
				$('#motherMaidenName').val(datas.motherMaidenName);
				$('#cifId').val(datas.cifId);
				$('#onlineBalance').val(datas.onlineBalance);
				$('#accrualBalanceJG').val(datas.accrualBalanceJG);
				$('#accrualBalancePrk').val(datas.accrualBalancePrk);
			}
		});
	});
	
	
	$('.btn-primary').on('click', function(){
	    if($(this).find('input').attr('id') == 'eod'){
	    	$('.closing').css("display", "none");
	    }
	    
	    if($(this).find('input').attr('id') == 'byrequest'){
	    	$('.closing').css("display", "block");
	    }
	    
	});
	

});