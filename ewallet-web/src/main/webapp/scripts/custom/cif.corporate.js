jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
	$('#save').click(function(){
        $('#cifcorporatedto').validationEngine();
    });
    $('#cancel').click(function(){
        $('#cifcorporatedto').validationEngine('detach');
    });
    
    $('#tableList').dataTable( {
        "aoColumnDefs": [
            { "mDataProp": "cifId", "aTargets": [0] },
            { "mDataProp": "corporateType", "aTargets": [1] },
            { "mDataProp": "name", "aTargets": [2] },
            { "mDataProp": "numberDeedOfIncorporation", "aTargets": [3] },
            { "mDataProp": "publishedDateLegality", "aTargets": [4] },   
            { "mDataProp": null, "sDefaultContent": "", "mRender" : function ( oObj ) {
                return '<a href=\"'+ base_url+ 'customer/cif/editcorporatelong/'+oObj['id']+'\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>';
                }, "aTargets" : [5]}
        ],
        "bFilter": false,
        "bSort": false,
        "bProcessing": true,
        "bServerSide": true,
//        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url +"/customer/cif/corporate/page",
        "sScrollX": "90%",
        "sScrollXInner":"100%",
        "bScrollCollapse": true
    });
});
