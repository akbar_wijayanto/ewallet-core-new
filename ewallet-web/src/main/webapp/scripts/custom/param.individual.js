jQuery(document).ready(function(){
	$.ajaxSetup ({
		cache: false
	});
    $('#save').click(function(){
        $('#paramcifdto').validationEngine();
    });
    $('#cancel').click(function(){
        $('#paramcifdto').validationEngine('detach');
    });
    
    $('#addCif').click(function(){
        window.location.href='/casa-web/customer/cif/addindividuallong';
    });
    
    $('#addCifCorporate').click(function(){
        window.location.href='/casa-web/customer/cif/addcorporatelong';
    });
    
    $('#addCifOrganization').click(function(){
        window.location.href='/casa-web/customer/cif/addorganizationlong';
    });
    
    $('form').on('change', '#cifType', function () {
    	 this.form.submit();
	});
});
