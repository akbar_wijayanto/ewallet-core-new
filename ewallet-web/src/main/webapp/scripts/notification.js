$(document).ready(function(){
	$('#usernameLogin').hide();
	var username = $('#usernameLogin').html();
	
//	setInterval(function(){getNotifNumber()}, 10000);
//	getNotifNumber();
	
	function getNotifNumber() {
		$.post(base_url + "notification/getNotificationNumber", {username: username},function(notificationNumber){
			$('#notificationNumber').html(notificationNumber);
		});
	}
	
	$('#notificationButton').click(function(){
		var notifNumber = $('#notificationNumber').html();
		var notifBody = "";
		if(notifNumber == 0) {
			notifBody += "<li class='header'>Inbox Empty<li>";
			$('#notifBody').html(notifBody);
		} else {
			notifBody += "<li class='header'>You have "+ notifNumber +" pending task</li>";
			$.post(base_url + "notification/getMessageList", {username: username}, function(datas){
				notifBody += "<li><ul class='menu' style='overflow: hidden; width: 100%; height: 200px;'>";
				for (var i = 0; i < datas.length; i++){
					var data = datas[i];
					notifBody += "<li>" + 
						             "<a href='" +base_url+ "trx/loancustomeroffer/approval/" +i+ "'>" + 
						             	"<h4>" + data.customerId.id + " - " + data.name + " - " + formatCurr(data.offerAmount) + "</h4>" +
						             "</a>" + 
					             "</li>";
				}
				notifBody += "</ul></li>";
				notifBody += "<li class='footer'><a href='" +base_url+ "trx/loancustomeroffer/'>See All Messages</a></li>"
				$('#notifBody').html(notifBody);
			});
		}
	});
	
	function formatCurr(n) {
	    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}
});