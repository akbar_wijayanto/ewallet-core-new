/**
 * @author -U-
 */

/*$('input:text.amount').on('change', function() {
	$(this).number(true, 2);
});	

$('input:text.rateAmount').on('change', function() {
	$(this).number(true, 2);
});*/

 $('input:text.amount').on('blur', function() {
        $('input:text.amount').each(function() {
            $(this).parseNumber({format:amount, locale:preferredLocale})
            $(this).formatNumber({format:amount, locale:preferredLocale});
        });
    });

    $('input:text.rateAmount').on('blur', function() {
        $('input:text.rateAmount').each(function() {
            $(this).parseNumber({format:amountRate, locale:preferredLocale})
            $(this).formatNumber({format:amountRate, locale:preferredLocale});
        });
    });
