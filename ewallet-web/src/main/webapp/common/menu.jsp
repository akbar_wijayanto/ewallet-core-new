<%@ include file="/common/taglibs.jsp"%>
<aside class="left-side sidebar-offcanvas">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar" style="overflow: hidden; width: auto;">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<c:forEach var="menus" items="${userMenus}">
				<li class="treeview">
					<a class="itemSide" href="#" style="border-top: none;"> 
						<span><i class="fa fa-folder-open iconStyle"></i> <fmt:message key="${menus.parentMenu.name}" /></span> 
						<i class="fa fa-angle-down pull-right" style="margin-top:10px"></i>
					</a>
					<ul class="treeview-menu bg-orange">
						<c:forEach var="menu" items="${menus.childMenus}">
							<c:choose>
								<c:when test="${empty menus.childSubMenus}">
									<li><a href="<c:url value='${menu.permalinks }' />"><i
											class="fa fa-angle-double-right"></i> <fmt:message
												key="${menu.name }" /></a></li>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${empty menu.permalinks or menu.permalinks == '#'}">									
											<li class="treeview"><a href="#"><i class="fa fa-plus-square-o"></i><fmt:message key="${menu.name }" /></a>
												<ul class="treeview-menu">
													<c:forEach var="submenu" items="${menus.childSubMenus}">
														<c:if test="${submenu.parentId == menu.id }">
															<li><a href="<c:url value='${submenu.permalinks }' />"><i
																	class="fa fa-angle-double-right"></i> <fmt:message
																		key="${submenu.name }" /></a></li>
														</c:if>
													</c:forEach>
												</ul>
											 </li>
										</c:when>
										<c:otherwise>	
											<li><a href="<c:url value='${menu.permalinks }' />"><i class="fa fa-angle-double-right"></i><fmt:message key="${menu.name }"/></a></li>
										</c:otherwise>
									</c:choose>
								 </c:otherwise>
							 </c:choose>
						</c:forEach>
					</ul>
				</li>
			</c:forEach>
		</ul>
	</section>
</aside>
