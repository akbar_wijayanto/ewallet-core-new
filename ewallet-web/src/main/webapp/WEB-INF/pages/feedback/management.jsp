<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="feedback.title"/></title>
    <meta name="heading" content="<fmt:message key='feedback.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/feedback.js' />"></script>
</head>
<div class="row">
<!-- 	<div class="col-xs-12">	  -->
<%-- <%-- 		<security:authorize access="hasRole('CORE:USER:INPUT:*')"> --%>
<%-- 	       	<a href="<c:url value='/core/seller/add' />" class="btn btn-primary pull-right"> --%>
<!-- 	       		<i class="fa fa-plus"></i> -->
<%-- 	       		<fmt:message key="coreSeller.message.add" /></a> --%>
<%-- <%-- 	    </security:authorize> --%>
<!-- 	</div> -->
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='feedback.management.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
<%-- 			                <th><fmt:message key='feedback.id'/></th> --%>
			                <th><fmt:message key='feedback.message'/></th>
			                <th><fmt:message key='feedback.rating'/></th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="feedback" items="${feedbacks}">
			            	<tr>
<%-- 				            	<td>${feedback.id}</td> --%>
				            	<td>${feedback.message}</td>
				            	<td>
				            		<c:forEach var="i" begin="1" end="${feedback.rating}">
				            			<a href="#"><i class="fa fa-star"></i></a>&nbsp;
				            		</c:forEach>
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>