<%@ page language="java" isErrorPage="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<title><fmt:message key="errorPage.title"/></title>
<head>
    <meta name="decorator" content="failure" />
    <meta name="heading" content="Security Violation"/>
</head>
<div class="error_box">
    <h1><fmt:message key="errorPage.heading"/></h1>
    <pre>
    <c:out value="${requestScope.exception.message}"/>
    <!--
    <%
    ((Exception) request.getAttribute("exception")).printStackTrace(new java.io.PrintWriter(out));
    %>
    -->
    </pre>
    <a href="javascript:history.back()" class="back_link btn btn-danger">Go back</a>
</div>