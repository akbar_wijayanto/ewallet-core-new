<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="trxOrder.title" /></title>
<meta name="heading" content="<fmt:message key='trxOrder.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/scripts/custom/trx.order.js' />"></script> --%>
<script type="text/javascript"
	src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxOrder.message.list' />
			</div>

			<spring:url var="action" value='/trx/order' />
			<form:form commandName="trxOrder" name="trxOrder" action="${action}" id="trxOrder" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-2 col-md-2">
							<label for="sellerId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.sellerId" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="sellerId" name="sellerId"
								class="form-control"
								class="form-control has-error validate[required]" 
								value="${sellerId}"/>
						</div>
						<div class="col-xs-1 col-md-1">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.startDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="startDate" name="startDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]" 
								value="${startDate}"/>
						</div>

						<div class="col-xs-1 col-md-1">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.endDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="endDate" name="endDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${endDate}" />
						</div>

						<%-- <div class="col-xs-1 col-md-1">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.submit"/>" />
							</div>
						</div> --%>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveOrder"
										id="saveOrder" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="trxorderdownloadpdf" id="trxorderdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="trxorderdownloadxls" id="trxorderdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='trxOrder.orderId' /></th>
							<th><fmt:message key='trxOrder.status' /></th>
							<th><fmt:message key='trxOrder.orderDate' /></th>
							<th><fmt:message key='trxOrder.paidDate' /></th>
							<th><fmt:message key='trxOrder.sellerId' /></th>
							<th><fmt:message key='trxOrder.coreUser' /></th>
							<th><fmt:message key='trxOrder.totHapok' /></th>
							<th><fmt:message key='trxOrder.totHajul' /></th>
							<th width="100px">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="order" items="${trxOrders}">
							<tr>

								<td>${order.orderId}</td>
								<td>${order.status}</td>
								<td>${order.orderDate}</td>
								<td>${order.paidDate}</td>
								<td>${order.sellerId}</td>
								<td>${order.userName}</td>
								<td><fmt:formatNumber pattern="${curr}" value="${order.totHargaPokok}" /></td>
								<td><fmt:formatNumber pattern="${curr}" value="${order.totHargaJual}" /></td>
								<%-- <td>${order.coreUser.firstName} ${order.coreUser.lastName}</td> --%>
								<td><a
									href="<c:url value='/trx/order/detail/${order.orderId}' />"
									title="View"><i class="fa fa-eye"></i></a>&nbsp;</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

			<div class="box-body">
				<div class="form-group">
					<div class="col-xs-4 col-md-4">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right"> <fmt:message
								key="trxOrder.grandTotalPokok" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="grandTotal" readonly="true"
							class="form-control amount validate[required]"
							class="form-control amount has-error validate[required]"
							value="<fmt:formatNumber pattern="${curr}" value="${grandTotalPokok}"/>" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-md-4">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right"> <fmt:message
								key="trxOrder.grandTotalJual" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="grandTotal" readonly="true"							
							class="form-control amount validate[required]"
							class="form-control amount has-error validate[required]"
							value="<fmt:formatNumber pattern="${curr}" value="${grandTotal}"/>" />
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>