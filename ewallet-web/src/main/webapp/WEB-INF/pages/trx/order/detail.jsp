<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='coreSeller.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreseller.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxOrder.message.view' />
			</div>
	        <spring:url var="action" value='/trx/order/detail/${order.orderId}' />
	        <form:form commandName="trxOrder" method="post" action="${action}"  id="trxOrder" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="orderId" path="orderId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.orderId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="orderId" id="orderId" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="orderId" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="orderDate" path="orderDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.orderDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="orderDate" id="orderDate" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="orderDate" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="paidDate" path="paidDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.paidDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="paidDate" id="paidDate" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="paidDate" cssClass="has-error" />
						</div>
					</div>					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:hidden path="coreUser.firstName" />
							<label for="coreUser" class="control-label pull-right">
							<fmt:message key="trxOrder.coreUser" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input id="coreUser" value="${trxOrder.coreUser.firstName} ${trxOrder.coreUser.lastName}"
								class="form-control s_error" readonly="true" />
							<form:errors path="coreUser.firstName" cssClass="s_error" />
						</div>
					</div>
					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:hidden path="TrxOrderItemDetail.description" />
							<label for="TrxOrderItemDetail" class="control-label pull-right">
							<fmt:message key="TrxOrderItemDetail.description" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input id="coreUser" value="${trxOrder.coreUser.firstName} ${trxOrder.coreUser.lastName}"
								class="form-control s_error" readonly="true" />
							<form:errors path="TrxOrderItemDetail.description" cssClass="s_error" />
						</div>
					</div> --%>
					<div class="form-group">
					<div class="box-body table-responsive">
						<table id="tableList" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th><fmt:message key='trxOrderDetail.itemName' /></th>
									<th><fmt:message key='trxOrderDetail.sellingPrice' /></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="orderDetail" items="${orderDetails}">
									<tr>		
										<td>${orderDetail.itemName}</td>
										<td><fmt:formatNumber pattern="${curr}" value="${orderDetail.sellingPrice}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					</div>
					
		            <%-- <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div> --%>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>