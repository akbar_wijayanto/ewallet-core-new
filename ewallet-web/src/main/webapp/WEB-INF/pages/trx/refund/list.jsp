<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="trxRefund.title" /></title>
<meta name="heading" content="<fmt:message key='trxRefund.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/scripts/custom/trx.refund.js' />"></script> --%>
<script type="text/javascript"
	src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxRefund.message.list' />
			</div>

			<spring:url var="action" value='/trx/refund' />
			<form:form commandName="trxRefund" name="trxRefund"
				action="${action}" id="trxRefund" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-2 col-md-2">
							<label for="sellerId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.sellerId" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="sellerId" name="sellerId"
								class="form-control" class="form-control has-error validate[required]" 
								value="${sellerId}"/>
						</div>
						<div class="col-xs-1 col-md-1">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.startDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="startDate" name="startDate" value="${startDate}"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${startDate}" />
						</div>
						<div class="col-xs-1 col-md-1">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.endDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="endDate" name="endDate" value="${endDate}"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${endDate}" />
						</div>
						<%-- <div class="col-xs-1 col-md-1">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.submit"/>" />
							</div>
						</div> --%>
					</div>

					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveRefund"
										id="saveRefund" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="trxrefunddownloadpdf" id="trxrefunddownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> <input
										type="button" class="btn btn-primary"
										name="trxrefunddownloadxls" id="trxrefunddownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='trxRefund.refundId' /></th>
							<th><fmt:message key='trxRefund.refundDate' /></th>
							<th><fmt:message key='trxRefund.sellerId' /></th>
							<th><fmt:message key='trxRefund.coreSeller' /></th>
							<th><fmt:message key='trxRefund.coreUser' /></th>
							<th><fmt:message key='trxRefund.TotalRefund' /></th>
							<!-- <th width="100px">&nbsp;</th> -->
						</tr>
					</thead>
					<tbody>
						<c:forEach var="trxRefund" items="${trxRefund}">
							<tr>
								<td>${trxRefund.refundId}</td>
								<td>${trxRefund.refundDate}</td>
								<td>${trxRefund.sellerId}</td>
								<td>${trxRefund.sellerName}</td>
								<td>${trxRefund.userName}</td>
								<td><fmt:formatNumber pattern="${curr}" value="${trxRefund.totalRefund}" /></td>
								<%-- <td><a
									href="<c:url value='/trx/refund/detail/${refund.refundId}' />"
									title="View"><i class="fa fa-eye"></i></a>&nbsp;</td> --%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right"> <fmt:message
								key="trxRefund.grandTotal" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="grandTotal"
							class="form-control amount validate[required]"
							class="form-control has-error validate[required]"
							value="<fmt:formatNumber pattern="${curr}" value="${grandTotal}"/>"
							readonly="true" />
					</div>
				</div>
			</div>

		</div>
	</div>
</div>