<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='coreSeller.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
<%-- 		<security:authorize access="hasRole('CORE:USER:INPUT:*')"> --%>
<%-- 	       	<a href="<c:url value='/trx/seller/add' />" class="btn btn-primary pull-right"> --%>
<!-- 	       		<i class="fa fa-plus"></i> -->
<%-- 	       		<fmt:message key="trxSellerPayment.message.add" /></a> --%>
<%-- 	    </security:authorize> --%>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='trxSellerPayment.message.list.auth'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			            	<th><fmt:message key='trxSellerPayment.sellerName'/></th>
			                <th><fmt:message key='trxSellerPayment.totalTransaction'/></th>
			                <th><fmt:message key='trxSellerPayment.grandTotal'/></th>
			                <th><fmt:message key='trxSellerPayment.percentage'/></th>
			                <th><fmt:message key='trxSellerPayment.charge'/></th>
			                <th><fmt:message key='trxSellerPayment.totalAmount'/></th>
			            	<th><fmt:message key='trxSellerPayment.accountNumber'/></th>
			            	<th><fmt:message key='trxSellerPayment.action'/></th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="settlement" items="${settlementDtos}">
			            	<tr>
			            		<td>${settlement.coreSeller.firstName} ${settlement.coreSeller.lastName}</td>
				            	<td>${settlement.totalTransaction}</td>
				            	<td>${settlement.grandTotal}</td>
				            	<td>${settlement.percentage}</td>
				            	<td>${settlement.charge}</td>
				            	<td>${settlement.totalAmount}</td>
				            	<td>${settlement.coreSeller.accountNumber}</td>
				            	<td>
				            		<a href="<c:url value='/trx/seller/payment/auth/detail/${settlement.paymentId}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
			
			<%-- <div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-12">
						<div class="pull-right">
			                <input type="button" class="btn btn-primary" name="downloadpdf" id="downloadpdf"  value="<fmt:message key="button.downloadPdf"/>"/>
			                <input type="button" class="btn btn-primary" name="downloadxls" id="downloadxls"  value="<fmt:message key="button.downloadExcel"/>"/>
	            		</div>
	            	</div>
            	</div>
            </div> --%>
			
		</div>
    </div>
</div>