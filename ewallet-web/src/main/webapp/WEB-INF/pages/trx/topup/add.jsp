<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="trxTopup.title"/></title>
    <meta name="heading" content="<fmt:message key='trxTopup.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.trxtopup.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='trxTopup.message.add' />
			</div>
	        <spring:url var="action" value='/trx/topup/add' />
	        <form:form commandName="trxRecord" method="post" action="${action}"  id="trxRecord" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="pairedAccount" path="pairedAccount" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxRecord.pairedAccount" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="pairedAccount" id="pairedAccount" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="pairedAccount" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="amount" path="amount" cssClass="control-label pull-right "
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxRecord.amount" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="amount" id="amount" cssClass="form-control  validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="amount" cssClass="has-error" />
						</div>
					</div>
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>