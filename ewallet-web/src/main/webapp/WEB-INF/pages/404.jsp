<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="404.heading"/></title>
    <meta name="heading" content="<fmt:message key='404.heading'/>"/>
    <meta name="decorator" content="failure">
</head>

<!-- Error info area -->
<div class="error-page">
    <h2 class="headline text-info"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> <fmt:message key="404.title"/></h3>
        <p>
            <fmt:message key="404.message" >
                <fmt:param><c:url value='/mainMenu' /></fmt:param>
            </fmt:message>
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
