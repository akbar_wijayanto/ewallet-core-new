<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="activationSuccess.heading" /></title>
<meta name="heading" content="<fmt:message key='activationSuccess.heading'/>" />
</head>

<div class="error-page-wrap">
	<article class="error-page gradient">
		<hgroup>
			<img src="<c:url value="/images/rolls.png"/>" class="colcustom" ></img>
			<h4>
				<i class="fa fa-warning text-yellow">Meanwhile, you may return to <a href="#">dashboard.</a></i>
			</h4>
		</hgroup>
	</article>
</div>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Registering successfully</title>
<meta name="viewport" content="width=device-width">
<!--[if gte IE 9]>
	<style type="text/css">
		.gradient {
		   filter: none;
		}
	</style>
	<![endif]-->
</head>

</html>