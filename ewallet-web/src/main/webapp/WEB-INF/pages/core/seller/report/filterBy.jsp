<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreSeller.title" /></title>
<meta name="heading" content="<fmt:message key='coreSeller.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.coreseller.js"/>">
	
</script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreSeller.message.reportFilter' />
			</div>
			<spring:url var="action" value='/core/seller/report/filterBy' />
			<form:form commandName="trxOrder" method="post" action="${action}"
				id="trxOrder" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreUser.username" path="coreUser.username"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreUser.username" id="coreUser.username"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="coreUser.username" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreSeller.sellerId" path="coreSeller.sellerId"
								cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.sellerId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="coreSeller.sellerId" id="coreSeller.sellerId"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="coreSeller.sellerId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="startDate" path="startDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.startDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="startDate" id="startDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="startDate" cssClass="error" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="endDate" path="endDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.endDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="endDate" id="endDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="endDate" cssClass="error" />
							</div>
						</div>
					</div>
					
					<%-- <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="category" path="category" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="trxOrder.category" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<select id="gender" name="gender" class="form-control">
								<c:forEach items="${ category }" var="category">
									<option value="${ category.value }">${ category.name }</option>
								</c:forEach>
							</select>
						</div>
					</div> --%>
					
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
				</div>
			</form:form>
		</div>
	</div>
</div>
