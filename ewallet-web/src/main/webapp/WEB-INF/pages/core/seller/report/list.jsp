<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.settlement.title"/></title>
    <meta name="heading" content="<fmt:message key='coreSeller.settlement.heading'/>"/>
<%--     <script type="text/javascript" src="<c:url value='/scripts/custom/coreSeller.settlement.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreSeller.settlement.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreSeller.settlement.sellerName'/></th>
			                <th><fmt:message key='coreSeller.settlement.grandTotal'/></th>
			                <th><fmt:message key='coreSeller.settlement.totalTransaction'/></th>
			                <th><fmt:message key='coreSeller.settlement.charge'/></th>
			                <th><fmt:message key='coreSeller.settlement.accountNumber'/></th>
			                <th><fmt:message key='coreSeller.settlement.totalAmount'/></th>
			                <th><fmt:message key='coreSeller.settlement.settlementDate'/></th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="settlement" items="${settlement}">
			            	<tr>
				            	<td>${settlement.coreSeller.firstName}</td>
				            	<td>${settlement.transactionId}</td>
				            	<td>${settlement.accountNumber}</td>
				            	<td>${settlement.coreUser.firstName}</td>
				            	<td>${settlement.amount}</td>
				            	<td>${settlement.transactionType}</td>
				            	<td>${settlement.transactionDate}</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
<!-- 	        	<table id="tableList" class="table table-bordered table-striped"> -->
<!-- 		            <thead> -->
<!-- 			            <tr> -->
<%-- 			                <th><fmt:message key='coreSeller.settlement.sellerName'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.grandTotal'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.totalTransaction'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.charge'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.accountNumber'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.totalAmount'/></th> --%>
<%-- 			                <th><fmt:message key='coreSeller.settlement.settlementDate'/></th> --%>
<!-- 			                <th width="280px">&nbsp;</th> -->
<!-- 			            </tr> -->
<!-- 		            </thead> -->
<!-- 		            <tbody> -->
<%-- 		            	<c:forEach var="settlement" items="${settlement}"> --%>
<!-- 			            	<tr> -->
			            		
<%-- 				            	<td>${settlement.sellerName}</td> --%>
<%-- 				            	<td>${settlement.grandTotal}</td> --%>
<%-- 				            	<td>${settlement.totalTransaction}</td> --%>
<%-- 				            	<td>${settlement.charge}</td> --%>
<%-- 				            	<td>${settlement.accountNumber}</td> --%>
<%-- 				            	<td>${settlement.countOfSettlement}</td> --%>
<%-- 				            	<td>${settlement.settlementDate}</td> --%>
<!-- 				            </tr> -->
<%-- 				    	</c:forEach> --%>
<!-- 		            </tbody> -->
<!-- 		        </table> -->
			</div>
			<div class="box-body">
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right">
							<fmt:message key="coreSeller.settlement.totalAmount" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="totalAmount" class="form-control validate[required]"
							class="form-control has-error validate[required]" />
					</div>
				</div>
            </div>
		</div>
    </div>
</div>