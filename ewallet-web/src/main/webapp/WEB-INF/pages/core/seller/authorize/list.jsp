<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='coreSeller.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreseller.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreSeller.message.listAuth'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreSeller.sellerId'/></th>
			                <th><fmt:message key='coreSeller.name'/></th>
			                <th><fmt:message key='coreSeller.streetAddress1'/></th>
			                <th><fmt:message key='coreSeller.phoneNumber'/></th>
			                <th width="100px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="seller" items="${coreSellers}">
			            	<tr>
			            		
				            	<td>${seller.sellerId}</td>
				            	<td>${seller.firstName} ${seller.lastName}</td>
				            	<td>${seller.streetAddress1}</td>
				            	<td>${seller.phoneNumber}</td>
				            	<td>
		                            <a href="<c:url value='/core/seller/authorize/authSeller/${seller.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>