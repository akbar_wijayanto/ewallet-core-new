<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreSeller.trxSeller.title" /></title>
<meta name="heading"
	content="<fmt:message key='coreSeller.trxSeller.heading'/>" />
<%--     <script type="text/javascript" src="<c:url value='/scripts/custom/coreSeller.settlement.js' />"></script> --%>
<script type="text/javascript"
	src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreSeller.trxSeller.message.list' />
			</div>

			<spring:url var="action" value='/core/seller/report/trxseller' />
			<form:form commandName="coreSeller" name="coreSeller"
				action="${action}" id="coreSeller" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-2 col-md-2">
							<label for="sellerId" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.sellerId" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="sellerId" name="sellerId"
								class="form-control" class="form-control has-error validate[required]" 
								value="${sellerId}"/>
						</div>
						<div class="col-xs-1 col-md-1">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.startDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="startDate" name="startDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]" 
								value="${startDate}"/>								
						</div>

						<div class="col-xs-1 col-md-1">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.endDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="endDate" name="endDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]" 
								value="${endDate}"/>
						</div>

						<%-- <div class="col-xs-1 col-md-1">
						<div class="pull-right">
							<input type="submit" class="btn btn-primary" name="save" id="save"
								value="<fmt:message key="button.submit"/>"/>
						</div>
					</div> --%>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveSeller"
										id="saveSeller" value="<fmt:message key="button.submit"/>" />
									<input type="button" class="btn btn-primary"
										name="trxsellerdownloadpdf" id="trxsellerdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="trxsellerdownloadxls" id="trxsellerdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='trxOrder.sellerId' /></th>
							<th><fmt:message key='trxOrder.orderDate' /></th>
							<th><fmt:message key='trxOrder.itemId' /></th>
							<th><fmt:message key='trxOrder.itemName' /></th>
							<th><fmt:message key='trxOrder.quantity' /></th>
							<th><fmt:message key='trxOrder.costPrice' /></th>
							<th><fmt:message key='trxOrder.total' /></th>
							<!-- <th width="280px">&nbsp;</th> -->
						</tr>
					</thead>
					<tbody>
						<c:forEach var="trxOrder" items="${trxOrder}">
							<tr>
								<td>${trxOrder.sellerId}</td>
								<td>${trxOrder.transactionDate}</td>
								<td>${trxOrder.itemId}</td>
								<td>${trxOrder.itemName}</td>
								<td>${trxOrder.quantity}</td>
								<td><fmt:formatNumber pattern="${curr}"
										value="${trxOrder.costPrice}" /></td>
								<td><fmt:formatNumber pattern="${curr}"
										value="${trxOrder.total}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="box-body">
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right"> <fmt:message
								key="trxOrder.grandTotal" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="grandTotal"
							class="form-control amount validate[required]"
							class="form-control has-error validate[required]"
							value="<fmt:formatNumber pattern="${curr}" value="${grandTotal}"/>"
							readonly="true" />
					</div>
				</div>
			</div>

		</div>
	</div>
</div>