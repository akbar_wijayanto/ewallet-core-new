<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreSeller.title"/></title>
    <meta name="heading" content="<fmt:message key='coreSeller.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='trxSellerPayment.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			            	<th><fmt:message key='trxSellerPayment.sellerName'/></th>
			                <th><fmt:message key='trxSellerPayment.totalTransaction'/></th>
			                <th><fmt:message key='trxSellerPayment.grandTotal'/></th>
			                <th><fmt:message key='trxSellerPayment.percentage'/></th>
			                <th><fmt:message key='trxSellerPayment.charge'/></th>
			                <th><fmt:message key='trxSellerPayment.totalAmount'/></th>
			            	<th><fmt:message key='trxSellerPayment.accountNumber'/></th>
			            	<th><fmt:message key='trxSellerPayment.flagPaid'/></th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="settlement" items="${trxSet}">
			            	<tr>
			            		<td>${settlement.sellerName}</td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.totalTransaction}" /></td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.grandTotal}" /></td>
				            	<td>${settlement.percentage}</td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.charge}" /></td>
				            	<td><fmt:formatNumber pattern="${curr}" value="${settlement.totalAmountPaid}" /></td>
				            	<td>${settlement.accountNumber}</td>
				            	<td>${settlement.flagPaid}</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
			
			<div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-12">
						<div class="pull-right">
			                <input type="button" class="btn btn-primary" name="settlementdownloadpdf" id="settlementdownloadpdf"  value="<fmt:message key="button.downloadPdf"/>"/>
			                <input type="button" class="btn btn-primary" name="settlementdownloadxls" id="settlementdownloadxls"  value="<fmt:message key="button.downloadExcel"/>"/>
	            		</div>
	            	</div>
            	</div>
            </div>
			
		</div>
    </div>
</div>