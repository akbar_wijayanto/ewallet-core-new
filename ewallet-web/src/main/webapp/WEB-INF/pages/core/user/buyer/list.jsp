<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreUser.buyer.title" /></title>
<meta name="heading"
	content="<fmt:message key='coreUser.buyer.heading'/>" />
<script type="text/javascript"
	src="<c:url value='/scripts/custom/trx.sellerpayment.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.buyer.message.list' />
			</div>

			<spring:url var="action" value='/core/user/buyer' />
			<form:form commandName="trxBuyer" name="trxBuyer" action="${action}" id="trxBuyer" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<div class="form-group">
						<div class="col-xs-2 col-md-2">
							<label for="startDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.startDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="startDate" name="startDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${startDate}" />
						</div>

						<div class="col-xs-1 col-md-1">
							<label for="endDate" class="control-label pull-right"
								class="control-label pull-right"> <fmt:message
									key="trxOrder.endDate" />
							</label>
						</div>
						<div class="col-xs-2 col-md-2">
							<input id="endDate" name="endDate"
								class="datepicker form-control validate[required]"
								class="datepicker form-control has-error validate[required]"
								value="${endDate}" />
						</div>

						<%-- <div class="col-xs-1 col-md-1">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.submit"/>" />
							</div>
						</div> --%>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-12">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary" name="saveBuyer"
										id="saveBuyer" value="<fmt:message key="button.submit"/>" /> 
									<input type="button" class="btn btn-primary"
										name="trxbuyerdownloadpdf" id="trxbuyerdownloadpdf"
										value="<fmt:message key="button.downloadPdf"/>" /> 
									<input type="button" class="btn btn-primary"
										name="trxbuyerdownloadxls" id="trxbuyerdownloadxls"
										value="<fmt:message key="button.downloadExcel"/>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>

			<div class="box-body table-responsive">
				<table id="tableList" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><fmt:message key='trxBuyer.id' /></th>
							<th><fmt:message key='trxBuyer.company' /></th>
							<th><fmt:message key='trxBuyer.nama' /></th>
							<th><fmt:message key='trxBuyer.totalTrx' /></th>
							<%-- <th><fmt:message key='trxBuyer.status' /></th> --%>
							<%-- <th><fmt:message key='trxBuyer.simpananwajib' /></th>
							<th><fmt:message key='trxBuyer.simpananpokok' /></th>
							<th><fmt:message key='trxBuyer.grandTotal' /></th> --%>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="trxBuyer" items="${trxBuyer}">
							<%-- declare variable on controller --%>
							<tr>
								<td>${trxBuyer.id}</td>
								<td>${trxBuyer.company}</td>
								<td>${trxBuyer.nama}</td>
								<td><fmt:formatNumber pattern="${curr}" value="${trxBuyer.totalTrx}" /></td>
								<%-- <td>${trxBuyer.status}</td> --%>
								<%-- <td><fmt:formatNumber pattern="${curr}" value="${trxBuyer.simpananwajib}" /></td>
								<td><fmt:formatNumber pattern="${curr}" value="${trxBuyer.simpananpokok}" /></td>
								<td><fmt:formatNumber pattern="${curr}" value="${trxBuyer.grandTotal}" /></td> --%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

			<%-- <div class="box-body">
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<label for="id" class="control-label pull-right"
							class="control-label pull-right"> <fmt:message
								key="trxOrder.grandTotal" />
						</label>
					</div>
					<div class="col-xs-8 col-md-6">
						<input id="grandTotal"
							class="form-control amount validate[required]"
							class="form-control has-error validate[required]"
							value="<fmt:formatNumber pattern="${curr}" value="${grandTotal}"/>"
							readonly="true" />
					</div>
				</div>
			</div> --%>

		</div>
	</div>
</div>