<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreItem.title" /></title>
<meta name="heading" content="<fmt:message key='coreItem.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.coreitem.js"/>"></script>
</head>
<div class="row">
	<div id="breadcumbTitle">
		<fmt:message key='coreItem.message.view' />
	</div>
	<spring:url var="action" value='/core/item/delete/${coreItem.id}' />
	<form:form commandName="coreItem" method="post" action="${action}"
		id="coreItem" class="form-horizontal">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />
					<form:hidden path="itemId"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="itemId" path="itemId"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.itemId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
<%-- 							<form:input path="itemId" id="itemId" --%>
<%-- 								cssClass="form-control validate[required]" --%>
<%-- 								cssErrorClass="form-control s_error validate[required]" --%>
<%-- 								readonly="true" /> --%>
								<img src='<c:out value="${image}"></c:out>' width="200" height="200">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="filePath" path="filePath"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.filePath" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
								<img src='<c:out value="${ctx}${coreItem.filePath}"></c:out>'width="200" height="200">
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="itemName" path="itemName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.itemName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="itemName" id="itemName" cssClass="form-control"
								cssErrorClass="form-control s_error" readonly="true" />
							<form:errors path="itemName" cssClass="s_error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="coreItemType" path="coreItemType" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.coreItemType" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<select id="coreItemType" name="coreItemType" class="form-control" readonly="true">
								<c:forEach items="${ coreItemTypes }" var="coreItemTypes">
									<option value="${ coreItemTypes.id }">${ coreItemTypes.name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="costPrice" path="costPrice"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.costPrice" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="costPrice" id="costPrice"
								cssClass="form-control amount validate[required]"
								cssErrorClass="form-control amount s_error validate[required]"
								readonly="true" />
							<form:errors path="costPrice" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sellingPrice" path="sellingPrice"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.sellingPrice" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sellingPrice" id="sellingPrice" cssClass="form-control amount"
								cssErrorClass="form-control amount s_error" readonly="true" />
							<form:errors path="sellingPrice" cssClass="s_error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="entryDate" path="entryDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.entryDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="entryDate" id="entryDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" readonly="true" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="entryDate" cssClass="error" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="expiredDate" path="expiredDate" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItem.expiredDate" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<div class="input-group">
								<form:input path="expiredDate" id="expiredDate" cssClass="datepicker form-control"
										cssErrorClass=" error datepicker form-control validate[required]" readonly="true" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<form:errors path="expiredDate" cssClass="error" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="description" path="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreItem.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description"
								cssClass="form-control" cssErrorClass="form-control s_error"
								readonly="true" />
							<form:errors path="description" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:hidden path="coreSeller.id"/>
							<label for="coreSeller" 
								class="control-label pull-right">
								<fmt:message key="coreItem.coreSeller" />
							</label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input id="coreSeller" value="${coreItem.coreSeller.firstName} ${coreItem.coreSeller.lastName}"
								class="form-control s_error" readonly="true" />
							<form:errors path="coreSeller.id" cssClass="s_error" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="form-group">
				<div class="col-xs-12 col-md-9">
					<div class="pull-right">
						<input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.delete"/>"/>
						<input type="submit" class="btn btn-primary" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" />
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>