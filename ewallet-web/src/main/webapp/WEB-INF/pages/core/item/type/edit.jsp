<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreItemType.title"/></title>
    <meta name="heading" content="<fmt:message key='coreItemType.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreitemtype.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreItemType.message.edit' />
			</div>
	        <spring:url var="action" value='/core/item/type/edit/${coreItemType.id}' />
	        <form:form commandName="coreItemType" method="post" action="${action}"  id="coreItemType" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="id" path="id" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItemType.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true"/>
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="name" path="name" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreItemType.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>
					
		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
					                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
					                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
			            		</div>
			            	</div>
		            	</div>
		            </div>
	            </div>
	        </form:form>
     	</div>
    </div>
</div>