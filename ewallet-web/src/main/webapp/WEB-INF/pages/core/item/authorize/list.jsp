<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreItem.title"/></title>
    <meta name="heading" content="<fmt:message key='coreItem.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreitem.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreItem.message.listAuth'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			                <th><fmt:message key='coreItem.itemId'/></th>
			                <th><fmt:message key='coreItem.itemName'/></th>
			                <th><fmt:message key='coreItem.description'/></th>
			                <th><fmt:message key='coreItem.sellingPrice'/></th>
			                <th><fmt:message key='coreItem.coreSeller'/></th>
			                <th width="100px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="item" items="${coreItems}">
			            	<tr>
				            	<td>${item.itemId}</td>
				            	<td>${item.itemName}</td>
				            	<td>${item.description}</td>
				            	<td>${item.sellingPrice}</td>
				            	<td>${item.coreSeller.firstName} ${item.coreSeller.lastName}</td>
				            	<td>
		                            <a href="<c:url value='/core/item/authorize/authItem/${item.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>