<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="systemWide.title"/></title>
    <meta name="heading" content="<fmt:message key='systemWide.heading'/>"/>

</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-primary">
			<div id="breadcumbTitle">
				<fmt:message key='systemWide.message.sview' />
			</div>
		
		<spring:url var = "action" value='/core/system/sview' />
		<form:form commandName="systemWide" method="post" action="${action}"  id="systemWide" class="form-horizontal">
        	<div class="box-body">			          
            	<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="createdBy" path="createdBy" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="menu.createdBy" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:input path="createdBy" id="createdBy" cssClass="form-control validate[maxSize[255]]" maxlength="255" cssErrorClass="form-control has-error validate[required,maxSize[255]]" readonly="true"/>
	                    <form:errors path="createdBy" cssClass="has-error" />
					</div>
				</div>		          
            	<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="updatedBy" path="updatedBy" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="menu.updatedBy" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:input path="updatedBy" id="updatedBy" cssClass="form-control validate[maxSize[255]]" maxlength="255" cssErrorClass="form-control has-error validate[required,maxSize[255]]" readonly="true"/>
	                    <form:errors path="updatedBy" cssClass="has-error" />
					</div>
				</div>
            	<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="" path="" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="systemWide.online" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <input type="text" readonly="readonly" value="${status}" class="form-control" />
						<form:errors path="" cssClass="has-error" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="todayDate" path="todayDate" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="systemWide.today" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:input path="todayDate" readonly="true" class="form-control"/>
	                    <form:errors path="todayDate" cssClass="has-error"/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="nextDate" path="nextDate" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="systemWide.nextDate" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:input path="nextDate" readonly="true" class="form-control"/>
	                    <form:errors path="nextDate" cssClass="has-error"/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="previousDate" path="previousDate" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="systemWide.prevDate" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:input path="previousDate" readonly="true" class="form-control"/>
	                    <form:errors path="previousDate" cssClass="has-error"/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label for="" path="" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="systemWide.defaultHoliday" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <input type="text" readonly="readonly" value="${defaultHoliday}" class="form-control"/>
						<form:errors path="" cssClass="has-error" />
					</div>
				</div>

	            <table class="table table-condensed table-striped table-bordered">
		            <thead>
		            <tr>
	            		<th><fmt:message key='entity.field'/></th>
	            		<th><fmt:message key='entity.value.old'/></th>
	                    <th><fmt:message key='entity.value.new'/></th>
		            </tr>
		            </thead>
		            <tbody>
		            	<tr>
		            		<td><fmt:message key="systemWide.localCurrency" /></td>
		            		<td>${systemWide.localCurrency.id}</td>
		            		<td>${systemWide.newValue.localCurrency.id}</td>
		            	</tr>
		            	<tr>
		            		<td><fmt:message key="systemWide.reportingCurrency" /></td>
		            		<td>${systemWide.reportingCurrency.id}</td>
		            		<td>${systemWide.newValue.reportingCurrency.id}</td>
		            	</tr>
<!-- 		            	<tr> -->
<%-- 		            		<td><fmt:message key="systemWide.crossCurrency" /></td> --%>
<%-- 		            		<td>${systemWide.crossCurrency.id}</td> --%>
<%-- 		            		<td>${systemWide.newValue.crossCurrency.id}</td> --%>
<!-- 		            	</tr> -->
		            	<tr>
		            		<td><fmt:message key="systemWide.companyName" /></td>
		            		<td>${systemWide.companyName}</td>
		            		<td>${systemWide.newValue.companyName}</td>
		            	</tr>
		            	
		            </tbody>
		        </table>
		    </div>
            
<!--             <div class="box-footer"> -->
<!-- 				<div class="form-group"> -->
<!-- 					<div class="col-xs-6"> -->
<!-- 						<div class="pull-right"> -->
<%-- 			               <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.approve"/>"/> --%>
<%-- 			               <input type="submit" class="btn btn-primary" name="reject" id="reject"  value="<fmt:message key="button.reject"/>"/> --%>
<!-- 	            		</div> -->
<!-- 	            	</div> -->
<!--             	</div> -->
<!--             </div> -->
        </form:form>
    </div>
</div>
</div>