<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="systemWide.title" /></title>
<meta name="heading" content="<fmt:message key='systemWide.heading'/>" />

</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='systemWide.message.param' />
			</div>
			<spring:url var="action" value='/core/system/param' />
			<form:form commandName="systemWide" method="post" action="${action}"
				id="systemWide" class="form-horizontal">
				<div class="box-body">
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="serviceCash" path="serviceCash"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="systemWide.serviceCash" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="serviceCash" id="serviceCash"
								cssClass="form-control" value="" />
							<form:errors path="serviceCash" cssClass="has-error" />
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.save"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>