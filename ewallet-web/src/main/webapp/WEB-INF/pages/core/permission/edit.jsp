<%@ include file="/common/taglibs.jsp"%>
<head>
	<title><fmt:message key="corePermission.title" /></title>
	<meta name="heading" content="<fmt:message key='corePermission.heading'/>" />
	<script type="text/javascript" src="<c:url value="/scripts/custom/core.permission.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="corePermission.message.edit" />
		</div>
		<spring:bind path="corePermission.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" 
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/permission/edit/${corePermission.id}' />
			<form:form commandName="corePermission" method="post" action="${action}" id="corePermission" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from" value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="id" path="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="name" path="name"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Permission.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="description" path="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Permission.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="moduleName" path="moduleName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.moduleName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="moduleName" id="moduleName"
								cssClass="form-control validate[required]"
								items="${moduleTypeEnums}" itemValue="moduleValue"
								itemLabel="moduleType" />
							<form:errors path="moduleName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="applicationName" path="applicationName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.applicationName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="applicationName" id="applicationName"
								cssClass="form-control validate[required]"
								items="${applicationTypeEnums}" itemValue="applicationValue"
								itemLabel="applicationType" />
							<form:errors path="applicationName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="actionName" path="actionName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.actionName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="actionName" id="actionName"
								cssClass="form-control validate[required]"
								items="${actionTypeEnums}" itemValue="id"
								itemLabel="lable" />
							<form:errors path="actionName" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="recordId" path="recordId"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.recordId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:select path="recordId" id="recordId"
								cssClass="form-control validate[required]"
								items="${recordTypeEnums}" itemValue="recordValue"
								itemLabel="recordType" />
							<form:errors path="recordId" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="preview" path=""
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="corePermission.preview" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<input type="text" id="preview" class="form-control"
								readonly="readonly">
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.edit"/>" /> 
								<input type="submit" class="btn btn-primary" name="cancel" 
									id="cancel" value="<fmt:message key="button.cancel"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>
</div>