<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreMenu.title"/></title>
    <meta name="heading" content="<fmt:message key='coreMenu.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.branch.type.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreMenu.message.delete' />
			</div>
        <spring:url var = "action" value='/core/menu/delete' />
        <form:form commandName="coreMenu" method="post" action="${action}"  id="coreMenu" class="form-horizontal">
			<div class="box-body">
	            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="id" for="id" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.id" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="id" id="id"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
						<form:errors path="id" cssClass="has-error" />
					</div>
				</div>
       			
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="description" for="description" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.description" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="description" id="description"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
						<form:errors path="description" cssClass="has-error" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="name" for="name" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.name" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="name" id="name"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
						<form:errors path="name" cssClass="has-error" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="orderMenu" for="orderMenu" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.orderMenu" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="orderMenu" id="orderMenu"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
						<form:errors path="orderMenu" cssClass="has-error" />
					</div>
				</div>
	            
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="parentId" for="parentId" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.parentId" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="parentId" id="parentId"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />	
	                    <form:errors path="parentId" cssClass="has-error"/>
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="permalinks" for="permalinks" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.permalinks" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="permalinks" id="permalinks"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
						<form:errors path="permalinks" cssClass="has-error" />
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="system" for="system" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.system" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:checkbox  path="system" id="system" disabled="true" cssErrorClass="has-error" readonly="true"/>
	                    <form:errors path="system" cssClass="has-error"/>
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="type" for="type" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.type" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="type" id="type"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" readonly="true" />
	                    <form:errors path="type" cssClass="has-error"/>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-9">
						<div class="pull-right">
			                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.delete"/>"/>
			                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
	            		</div>
	            	</div>
            	</div>
            </div>
        </form:form>
    </div>
</div>
</div>