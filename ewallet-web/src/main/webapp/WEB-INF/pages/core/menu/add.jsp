<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreMenu.title"/></title>
    <meta name="heading" content="<fmt:message key='coreMenu.heading'/>"/>
    <script type="text/javascript" src="<c:url value="/scripts/custom/core.menu.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreMenu.message.add' />
			</div>

        <spring:url var = "action" value='/core/menu/add' />
        <form:form commandName="coreMenu" method="post" action="${action}"  id="coreMenu" class="form-horizontal">
            <div class="box-body">
	            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
       			
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="description" for="description" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.description" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="description" id="description"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" />
						<form:errors path="description" cssClass="has-error pull-right" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="name" for="name" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.name" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="name" id="name"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" />
						<form:errors path="name" cssClass="has-error pull-right" />
					</div>
				</div>
	
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="orderMenu" for="orderMenu" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.orderMenu" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="orderMenu" id="orderMenu"
							cssClass="form-control validate[required, max[50]]" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]]" />
						<form:errors path="orderMenu" cssClass="has-error pull-right" />
					</div>
				</div>
	            
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="parentId" for="parentId" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.parentId" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:select path="parentId" id="parentId"  cssClass="form-control validate[required]" cssErrorClass="form-control has-error validate[required]">
	                        <form:option value="" label="--- Select ---"/>
	                        <form:options items="${coreMenus}" itemValue="id" itemLabel="name" />
	                    </form:select>
	                    <form:errors path="parentId" cssClass="has-error pull-right"/>
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="permalinks" for="permalinks" cssClass="control-label pull-right"
							cssErrorClass="control-label pull-right">
							<fmt:message key="Menu.permalinks" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
						<form:input path="permalinks" id="permalinks"
							cssClass="form-control validate[required, max[50]] pull-right" maxlength="50"
							cssErrorClass="form-control has-error validate[required, max[50]] pull-right" />
						<form:errors path="permalinks" cssClass="has-error pull-right" />
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="system" for="system" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.system" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	                    <form:checkbox  path="system" id="system"  cssErrorClass="has-error"/>
	                    <form:errors path="system" cssClass="has-error pull-right" />
					</div>
				</div>
				
       			<div class="form-group">
					<div class="col-xs-4 col-md-3">
						<form:label path="type" for="type" cssClass="control-label pull-right"
							cssErrorClass="control-label">
							<fmt:message key="coreMenu.type" />
						</form:label>
					</div>
					<div class="col-xs-8 col-md-6">
	  				    <form:select path="type" id="type" items="${menus}" itemValue="menuValue" itemLabel="menuType" cssClass="form-control validate[required]" cssErrorClass="form-control  has-error validate[required]" />
	                    <form:errors path="type" cssClass="has-error pull-right"/>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="form-group">
					<div class="col-xs-12 col-md-9">
						<div class="pull-right">
			                <input type="submit" class="btn btn-primary" name="save" id="save"  value="<fmt:message key="button.add"/>"/>
			                <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
	            		</div>
	            	</div>
            	</div>
            </div>
        </form:form>
    </div>
</div>
</div>