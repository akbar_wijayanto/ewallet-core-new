<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key='coreReport.title' /></title>
<meta name="heading"
	content="<fmt:message key='coreReport.heading'/>" />
<%-- <script type="text/javascript" src="<c:url value='/resources/assets/custom/core.report.js' />"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key="coreReport.message" /></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
							<th><fmt:message key='coreReport.reportDate' /></th>
							<th width="11%">&nbsp;</th>
						</tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="coreReports" items="${coreReports}">
			            	<tr>
				            	<td>${coreReports}</td>
				            	<td>
				            		<a href="<c:url value='/core/report/cob/${month}/${coreReports}' />" title="Open">
				            		<i class="fa fa-folder-open"></i></a>
				            		<%-- <a href="<c:url value='/core/report/cob/download/pdf/${reportType}/${date}/${coreReports}' />" title="Download PDF">
				            		<i class="fa fa-download"></i>PDF</a>&nbsp;
				            		<a href="<c:url value='/core/report/cob/download/xls/${reportType}/${date}/${coreReports}' />" title="Download Excel">
				            		<i class="fa fa-download"></i>XSL</a></td> --%>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>