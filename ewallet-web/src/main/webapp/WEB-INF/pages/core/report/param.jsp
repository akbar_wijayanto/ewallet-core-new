<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreReport.title"/></title>
    <meta name="heading" content="<fmt:message key='coreReport.heading'/>"/>
</head>

<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key="coreReport.message.download" />
		</div>
		<spring:bind path="coreReport.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
        <spring:url var="actions" value='/core/report/download/${type}/${id}' />
        <form method="post" action="${actions}"  id="coreReport" class="form-horizontal">
        <input type="hidden" name="from" value="<c:out value="${param.from}"/>" />
        <div class="box-body">
			 
			<c:choose>
            <c:when test="${ coreReport.parameter == 'BYUSER' }"> 
            <div class="form-group">
         		<div class="col-xs-2 ">
                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
           		 </div>
           		 <div class="col-xs-4">

           		 	
                     <input type="text" class="datepicker form-control" name="startDate">
           		 </div>
           		 <div class="col-xs-2 ">
                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
           		 </div>
           		 <div class="col-xs-4">
                      <input type="text" class="datepicker form-control" name="endDate">
           		 </div>
           	</div>
             <div class="form-group">
	         		<div class="col-xs-2">
	                <label for="userId" class="control-label pull-right"><fmt:message key="coreReport.user" /></label>
	                <input type="hidden" id="userId"></input>
	           		 </div>
	           		<div class="col-xs-4">
	                    <select name="parameter" id="parameter" class="form-control">
	                    <option value>Select Below</option>
	                    <c:forEach items="${users}" var="user">
	                    	<option value="${user.username}">${user.username}</option>
	                    </c:forEach>
	                       
	                    </select>
	           		 </div>
            	</div>
            </c:when>  
            </c:choose>
            <c:choose>
            <c:when test="${ coreReport.parameter == 'BYAO' }"> 
            <div class="form-group">
         		<div class="col-xs-2 ">
                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
           		 </div>
           		 <div class="col-xs-4">

           		 	
                     <input type="text" class="datepicker form-control" name="startDate">
           		 </div>
           		 <div class="col-xs-2 ">
                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
           		 </div>
           		 <div class="col-xs-4">
                      <input type="text" class="datepicker form-control" name="endDate">
           		 </div>
           	</div>
             <div class="form-group">
	         		<div class="col-xs-2">
	                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.accountOfficer" /></label>
	           		 </div>
	           		<div class="col-xs-4">
	                    <select name="parameter" id="parameter" class="form-control">
	                    <option value>Select Below</option>
	                    <c:forEach items="${accountOfficers}" var="accountOfficer">
	                    	<option value="${accountOfficer.codeAo}">${accountOfficer.codeAo}</option>
	                    </c:forEach>
	                       
	                    </select>
	           		 </div>
            	</div>
            </c:when>  
            </c:choose>
             <c:choose>
            <c:when test="${ coreReport.parameter == 'PRODUCT' }"> 
             <div class="form-group">
         		<div class="col-xs-2 ">
                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
           		 </div>
           		 <div class="col-xs-4">

           		 	
                     <input type="text" class="datepicker form-control" name="startDate">
           		 </div>
           		 <div class="col-xs-2 ">
                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
           		 </div>
           		 <div class="col-xs-4">
                      <input type="text" class="datepicker form-control" name="endDate">
           		 </div>
           	</div>
         	 <div class="form-group">
	         		<div class="col-xs-2">
	                <label for="productId" class="control-label pull-right"><fmt:message key="coreReport.product" /></label>
	           		 </div>
	           		<div class="col-xs-4">
	                    <select name="parameter" id="parameter" class="form-control">
	                    <option value>Select Below</option>
	                    <c:forEach items="${productTypes }" var="proType">
	                  	  <option value="${proType }">${proType.productType }</option>
							 
						</c:forEach>
	                   
	                       
	                    </select>
	           		 </div>
            	</div>
            	</c:when>
            	</c:choose>
 			<c:choose>
            <c:when test="${ coreReport.parameter == 'CURRENCY' }"> 
            	<div class="form-group">
	         		<div class="col-xs-2 ">
	                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">

	           		 	
	                     <input type="text" class="datepicker form-control" name="startDate">
	           		 </div>
	           		 <div class="col-xs-2 ">
	                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">
	                      <input type="text" class="datepicker form-control" name="endDate">
	           		 </div>
           		</div>     	
            	<div class="form-group">
	         		<div class="col-xs-2">
	                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.currency" /></label>
	           		 </div>
	           		<div class="col-xs-4">
	                    <select name="parameter" id="parameter" class="form-control">
	                     <option value>Select Below</option>
	                    <c:forEach items="${currencys}" var="currency">
	                    	<option value="${currency.id}">${currency.id}</option>
	                    </c:forEach>
	                       
	                    </select>
	           		 </div>
            	</div>
            </c:when>
            </c:choose>
            <c:choose>
            <c:when test="${ coreReport.parameter == 'ACCOUNT' }"> 
            	<div class="form-group">
	         		<div class="col-xs-2 ">
	                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">
	                     <input type="text" class="datepicker form-control" name="startDate">
	           		 </div>
	           		 <div class="col-xs-2 ">
	                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">
	                      <input type="text" class="datepicker form-control" name="endDate">
	           		 </div>
	           	</div>
             	<div class="form-group">
	         		<div class="col-xs-2">
	                <label for="userId" class="control-label pull-right"><fmt:message key="coreReport.account" /></label>
	           		 </div>
	           		<div class="col-xs-4">
	                    <select name="parameter" id="parameter" class="form-control">
	                    <option value>Select Below</option>
	                    <c:forEach items="${accounts}" var="account">
	                    	<option value="${account.codeAccount}">${account.codeAccount}</option>
	                    </c:forEach>
	                       
	                    </select>
	           		 </div>
            	</div>
            </c:when>  
            </c:choose>
            <c:choose>
           		 <c:when test="${ coreReport.parameter == 'DATE' }"> 
            	 
            	<div class="form-group">
	         		<div class="col-xs-2 ">
	                	<label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.startDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">
	           		 	<input type="hidden" name="parameter">
	                     <input type="text" class="datepicker form-control" name="startDate">
	           		 </div>
	           		 <div class="col-xs-2 ">
	                <label for="accountOfficer" class="control-label pull-right"><fmt:message key="coreReport.endDate" /></label>
	           		 </div>
	           		 <div class="col-xs-4">
	                      <input type="text" class="datepicker form-control" name="endDate">
	           		 </div>
            	</div>
            	 
            	 </c:when>
            	 </c:choose>
            	<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-12">
							<div class="pull-center" style="text-align:center">
								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.download"/>" /> 
								<a href="<c:url value='/core/report' />" class="btn btn-primary" id="cancel"/><fmt:message key="button.cancel"/></a>
							</div>
		            	</div>
	            	</div>
                </div>
                </div>
        </form>
    </div>
</div></div>