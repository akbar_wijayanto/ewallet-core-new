<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreUser.title" /></title>
<meta name="heading" content="<fmt:message key='coreUser.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script>
</head>
<div class="row">
	<div id="breadcumbTitle">
		<fmt:message key='coreUser.message.view' />
	</div>
	<spring:url var="action" value='/core/coreuser/detail/${coreUser.id}' />
	<form:form commandName="coreUser" method="post" action="${action}"
		id="coreUser" class="form-horizontal">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control s_error validate[required]"
								readonly="true" />
							<form:errors path="username" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="email" path="email"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.email" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control"
								cssErrorClass="form-control s_error" readonly="true" />
							<form:errors path="email" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control s_error validate[required]"
								readonly="true" />
							<form:errors path="firstName" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control s_error" readonly="true" />
							<form:errors path="lastName" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="personnelCode" path="personnelCode"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.personnelCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode"
								cssClass="form-control" cssErrorClass="form-control s_error"
								readonly="true" />
							<form:errors path="personnelCode" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="limitAmount" path="limitAmount"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.limitAmount" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="limitAmount" id="limitAmount"
								cssClass="form-control amount"
								cssErrorClass="form-control amount s_error" readonly="true" />
							<form:errors path="limitAmount" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="sessionTimeout" path="sessionTimeout"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.sessionTimeout" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="sessionTimeout" id="sessionTimeout"
								cssClass="form-control" cssErrorClass="form-control s_error"
								readonly="true" />
							<form:errors path="sessionTimeout" cssClass="s_error" />
						</div>
					</div>

		            <div class="box-footer">
						<div class="form-group">
							<div class="col-xs-12 col-md-9">
								<div class="pull-right">
				                   <a class="btn btn-primary" href="<c:url value='/core/coreuser/resetpassword/${coreUser.id}'></c:url> "><fmt:message key="coreUser.resetPassword"/> </a>
				                   <a class="btn btn-primary" href="<c:url value='/core/coreuser/resetuser/${coreUser.id}'></c:url> "><fmt:message key="coreuser.resetUser"/> </a>
			            		</div>
			            	</div>
		            	</div>
		            </div>

					<!-- start tab  -->
					<div class="nav-tabs-custom">
						<ul id="myTab" class="nav nav-tabs">
							<li class="active"><a href="#managepermission"
								data-toggle="tab"><fmt:message
										key="coreuser.tab.managepermission" /></a></li>
<%-- 							<li><a href="#managebranch" data-toggle="tab"><fmt:message --%>
<%-- 										key="coreuser.tab.managebranch" /></a></li> --%>
							<li><a href="#managerole" data-toggle="tab"><fmt:message
										key="coreuser.tab.role" /></a></li>

						</ul>
					</div>

					<div id="myTabContent" class="tab-content">
						<!-- tab 1 permission -->
<!-- 						<div class="tab-pane fade in active" id="managepermission"> -->
<!-- 							<div class="box-body"> -->
<!-- 								<input type="hidden" name="from" -->
<%-- 									value="<c:out value="${param.from}"/>" /> --%>

<!-- 								<div class="form-group"> -->
<!-- 									<div class="col-xs-4 col-md-3"> -->
<%-- 										<form:label for="corePermissions" path="corePermissions" --%>
<%-- 											cssClass="control-label pull-right" --%>
<%-- 											cssErrorClass="control-label"> --%>
<%-- 											<fmt:message key="coreUser.corePermissions" /> --%>
<%-- 										</form:label> --%>
<!-- 									</div> -->
<!-- 									<div class="col-xs-8 col-md-6"> -->
<!-- 										<table style="width: 400px" -->
<!-- 											class="table table-condensed table-striped table-bordered" -->
<!-- 											id="permissions"> -->
<!-- 											<thead> -->
<!-- 												<tr> -->
<%-- 													<th colspan="13"><fmt:message --%>
<%-- 															key='coreUser.corePermissions' /></th> --%>
<!-- 												</tr> -->
<!-- 											</thead> -->
<!-- 											<tbody> -->
<%-- 												<c:forEach var="dto" items="${dtos}"> --%>
<!-- 													<tr> -->
<%-- 														<td>${dto.applicationName}</label></td> --%>
<%-- 														<c:forEach var="permission" items="${dto.corePermissions}"> --%>
<!-- 															<td><input type="checkbox" name="corePermissions" -->
<%-- 																value="${permission.id}" class="check" --%>
<%-- 																${permission.checklistStatus==true?'checked="checked"': ''} /></td> --%>
<%-- 															<td>${permission.actionName=='*'?'ALL':permission.actionName}</td> --%>
<%-- 														</c:forEach> --%>
<!-- 													</tr> -->
<%-- 												</c:forEach> --%>
<!-- 											</tbody> -->
<!-- 										</table> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- end tab 1 managepermission -->

						<!-- start tab 2 branch -->
<!-- 						<div class="tab-pane fade" id="managebranch"> -->
<!-- 							<div class="form-group"> -->
<!-- 								<div class="col-xs-4 col-md-3"> -->
<%-- 									<form:label for="corePermissions" path="corePermissions" --%>
<%-- 										cssClass="control-label pull-right" --%>
<%-- 										cssErrorClass="control-label"> --%>
<%-- 										<fmt:message key="coreUser.coreBranches" /> --%>
<%-- 									</form:label> --%>
<!-- 								</div> -->
<!-- 								<div class="col-xs-8 col-md-6"> -->
<!-- 									<table style="width: 400px" -->
<!-- 										class="table table-condensed table-striped table-bordered" -->
<!-- 										id="branches"> -->
<!-- 										<thead> -->
<!-- 											<tr> -->
<!-- 												<th class="table_checkbox"><input type="checkbox" -->
<!-- 													id="checkBranch" class="all" /></th> -->
<%-- 												<th><fmt:message key='coreBranch.name' /></th> --%>
<!-- 											</tr> -->
<!-- 										</thead> -->
<!-- 										<tbody> -->
<%-- 											<c:forEach var="cp" items="${branches}"> --%>
<!-- 												<tr> -->
<!-- 													<td><input type="checkbox" name="coreBranches" -->
<%-- 														class="check" value="${cp.value}" --%>
<%-- 														${cp.status==true?'checked="checked"': ''} /></td> --%>
<%-- 													<td>${cp.label}</td> --%>
<!-- 												</tr> -->
<%-- 											</c:forEach> --%>
<!-- 										</tbody> -->
<!-- 									</table> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<!-- end tab 2 branch -->

						<!-- start tab 3 role -->
						<div class="tab-pane fade" id="managerole">
							<div class="form-group">
								<div class="col-xs-4 col-md-3">
									<form:label for="corePermissions" path="corePermissions"
										cssClass="control-label pull-right"
										cssErrorClass="control-label">
										<fmt:message key="coreUser.coreRoles" />
									</form:label>
								</div>
								<div class="col-xs-8 col-md-6">
									<table style="width: 400px"
										class="table table-condensed table-striped table-bordered"
										id="roles">
										<thead>
											<tr>
												<th class="table_checkbox"><input type="checkbox"
													id="checkRole" class="all" /></th>
												<th><fmt:message key='coreRole.name' /></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cp" items="${listCoreRoles}">
												<tr>
													<td><input type="checkbox" name="coreRoles"
														class="check" value="${cp.value}"
														${cp.status==true?'checked="checked"': ''} /></td>
													<td>${cp.label}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- end tab 3 role -->
					</div>
					<!-- end tab -->

				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="form-group">
				<div class="col-xs-12 col-md-9">
					<div class="pull-right">
						<input type="submit" class="btn btn-primary" name="save" id="save"
							value="<fmt:message key="button.save"/>" /> <input type="submit"
							class="btn btn-danger" name="cancel" id="cancel"
							value="<fmt:message key="button.cancel"/>" />
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>