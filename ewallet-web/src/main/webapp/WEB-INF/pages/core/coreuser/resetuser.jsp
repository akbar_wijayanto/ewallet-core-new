<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
   <%--  <script type="text/javascript" src="<c:url value="/scripts/custom/core.coreuser.js"/>"></script> --%>
</head>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div id="breadcumbTitle">
				<fmt:message key='coreUser.message.resetuser' />
			</div>
			
	        <spring:url var = "action" value='/core/coreuser/resetuser/${coreUser.id}' />
	        <form:form commandName="coreUser" method="post" action="${action}"  id="coreUser" class="form-horizontal">
	        	<div class="box-body">
		            <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
		
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="username" path="username" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.username" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="username" id="username" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="username" cssClass="has-error" />
						</div>
					</div>
		           
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="email" path="email" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.email" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="email" id="email" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true"  />
							<form:errors path="email" cssClass="has-error" />
						</div>
					</div>
		            
		            <div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="firstName" path="firstName" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.firstName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="firstName" id="firstName" cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" readonly="true" />
							<form:errors path="firstName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastName" path="lastName" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.lastName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastName" id="lastName" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="lastName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="personnelCode" path="personnelCode" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.personnelCode" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="personnelCode" id="personnelCode" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="personnelCode" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="loginAttempt" path="loginAttempt" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.loginAttempt" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="loginAttempt" id="loginAttempt" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="loginAttempt" cssClass="has-error" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="lastLogon" path="lastLogon" cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreUser.lastLogon" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="lastLogon" id="lastLogon" cssClass="form-control"
								cssErrorClass="form-control has-error" readonly="true" />
							<form:errors path="lastLogon" cssClass="has-error" />
						</div>
					</div>
		        </div>    
	            
	            <div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
				               <input type="submit" class="btn btn-primary" name="reset" id="reset"  value="<fmt:message key="button.reset"/>"/>
				               <input type="submit" class="btn btn-danger" name="cancel" id="cancel"  value="<fmt:message key="button.cancel"/>"/>
		            		</div>
		            	</div>
	            	</div>
	            </div>
	        </form:form>
    	</div>
    </div>
</div>