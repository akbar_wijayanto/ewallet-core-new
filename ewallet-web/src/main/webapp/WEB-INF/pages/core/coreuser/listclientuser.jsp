<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreUser.title"/></title>
    <meta name="heading" content="<fmt:message key='coreUser.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/custom/core.coreuser.js' />"></script>
</head>
<div class="row">
	<div class="col-xs-12">	 
		<security:authorize access="hasRole('CORE:USER:INPUT:*')">
	       	<a href="<c:url value='/core/coreuser/addclientuser' />" class="btn btn-primary pull-right">
	       		<i class="fa fa-plus"></i>
	       		<fmt:message key="coreUser.message.add" /></a>
	    </security:authorize>
	</div>
	<div class="col-xs-12">
	    <div class="box">
	       	<div id="breadcumbTitle"><fmt:message key='coreUser.message.list'/></div>
	       	
	        <div class="box-body table-responsive">
	        	<table id="tableList" class="table table-bordered table-striped">
		            <thead>
			            <tr>
			            	<th><fmt:message key='coreUser.customerCif'/></th>
			                <th><fmt:message key='coreUser.username'/></th>
			                <th width="25%"><fmt:message key='coreUser.email'/></th>
			                <th><fmt:message key='coreUser.firstName'/></th>
			                <th><fmt:message key='coreUser.lastName'/></th>
			                <th><fmt:message key='coreUser.status'/></th>
			                <th width="280px">&nbsp;</th>
			            </tr>
		            </thead>
		            <tbody>
		            	<c:forEach var="cu" items="${coreUsers}">
			            	<tr>
			            		
				            	<td>${cu.customerCif.name}</td>
				            	<td>${cu.username}</td>
				            	<td>${cu.email}</td>
				            	<td>${cu.firstName}</td>
				            	<td>${cu.lastName}</td>
				            	<td>${cu.status}</td>
				            	<td>
		                            <a href="<c:url value='/core/coreuser/detail/${cu.id}' />" title="View"><i class="fa fa-eye"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/edit/${cu.id}' />" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/managepermission/${cu.id}' />" title="Setting Permission"><i class="fa fa-unlock"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/managerole/${cu.id}' />" title="Setting Role"><i class="fa fa-legal"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/managebranch/${cu.id}' />" title="Setting Branch"><i class="fa fa-building-o"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/resetpassword/${cu.id}' />" title="Reset Password"><i class="fa fa-rotate-left"></i></a>&nbsp;
		                            <a href="<c:url value='/core/coreuser/resetuser/${cu.id}' />" title="Reset User"><i class="fa fa-refresh"></i></a>&nbsp;
				            	    <a href="<c:url value='/core/teller/add/${cu.id}' />" title="Set Teller"><i class="fa fa-user"></i></a>&nbsp;
                                    <a href="<c:url value='/core/staff/add/${cu.id}' />" title="Set Staff"><i class="fa fa-male"></i></a>&nbsp;
				            	</td>
				            </tr>
				    	</c:forEach>
		            </tbody>
		        </table>
			</div>
		</div>
    </div>
</div>