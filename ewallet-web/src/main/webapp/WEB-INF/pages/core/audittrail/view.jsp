<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="coreAuditTrail.title"/></title>
    <meta name="heading" content="<fmt:message key='coreAuditTrail.heading'/>"/>
</head>
<div class="row-fluid">
	<div class="span12">
		<div id="breadcumbTitle">
			<fmt:message key="coreAuditTrail.message.view" />
		</div>	
		<spring:bind path="coreAuditTrail.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind> 
		
		<div class="box">
	        <spring:url var = "action" value='/core/audittrail/view/${coreAuditTrail.id}' />
	        <form:form commandName="coreAuditTrail" method="post" action="${action}" id="coreAuditTrail" class="form-horizontal">
            	<div class="box-body">
            		<input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>
            		
            		<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="id" for="id" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="objectName" for="objectName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.objectName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="objectName" id="objectName" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="objectName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="parentId" for="parentId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.parentId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="parentId" id="parentId" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="parentId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="fieldName" for="fieldName" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.fieldName" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="fieldName" id="fieldName" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="fieldName" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="actor" for="actor" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.actor" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="actor" id="actor" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="actor" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="before" for="before" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.before" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="before" id="before" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="before" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="after" for="after" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.after" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="after" id="after" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="after" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="objectId" for="objectId" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.objectId" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="objectId" id="objectId" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="objectId" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="time" for="time" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.time" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="time" id="time" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="time" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="action" for="action" cssClass="control-label pull-right"
								cssErrorClass="control-label pull-right">
								<fmt:message key="coreAuditTrail.action" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="action" id="action" readonly="true"
								cssClass="form-control validate[required,maxSize[255]]"
								maxlength="255"
								cssErrorClass="has-error form-control validate[required,maxSize[255]]" />
							<form:errors path="action" cssClass="has-error" />
						</div>
					</div>
            	</div>
            	
            	<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<a href="<c:url value='/core/audittrail' />" class="btn btn-primary" title="<fmt:message key="coreAuditTrail.title"/>"><fmt:message key="coreAuditTrail.title"/></a>
<%-- 								<input type="submit" class="btn btn-primary" name="save" id="save" value="<fmt:message key="button.add"/>" />  --%>
<%-- 								<input type="submit" class="btn btn-danger" name="cancel" id="cancel" value="<fmt:message key="button.cancel"/>" /> --%>
							</div>
		            	</div>
	            	</div>
                </div>

        	</form:form>
        </div>
    </div>
</div>


