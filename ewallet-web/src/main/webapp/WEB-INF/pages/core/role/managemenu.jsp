<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreRole.title" /></title>
<meta name="heading" content="<fmt:message key='coreRole.heading'/>" />
<script type="text/javascript"
	src="<c:url value="/scripts/custom/core.role.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreRole.message.managemenu' />
		</div>
		<spring:bind path="coreRole.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/role/managemenu/${coreRole.id}' />
			<form:form commandName="coreRole" method="post" action="${action}"
				id="coreRole" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from"
						value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="id" for="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control s_error validate[required]" />
							<form:errors path="id" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="name" for="name"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control s_error validate[required]" />
							<form:errors path="name" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description" for="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control s_error validate[required]" />
							<form:errors path="description" cssClass="s_error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label for="checkMenu" path="coreMenus"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.coreMenus" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<table style="width: 400px"
								class="table table-striped table-bordered" id="menus">
								<thead>
									<tr>
										<th class="table_checkbox"><input type="checkbox" class="all"
											id="checkMenu" /></th>
										<th><fmt:message key='coreMenu.name' /></th>
										<th><fmt:message key='coreMenu.type' /></th>
										<th><fmt:message key='coreMenu.id' /></th>
										<th><fmt:message key='coreMenu.parentId' /></th>
									</tr>
								</thead>
								<tbody>
<%-- 									<c:forEach var="cp" items="${coreMenus}"> --%>
<!-- 										<tr> -->
<!-- 											<td><input type="checkbox" class="check" name="coreMenus" -->
<%-- 												value="${cp.value}" --%>
<%-- 												${cp.status==true?'checked="checked"': ''} /></td> --%>
<%-- 											<td>${cp.label}</td> --%>
											
<!-- 										</tr> -->
<%-- 									</c:forEach> --%>
									
									<c:forEach var="cp" items="${coreMenus}">
										<tr>
											<td><input type="checkbox" class="check" name="coreMenus"
												value="${cp.id}"
												${cp.checklistStatus==true?'checked="checked"': ''} /></td>
											<td>${cp.description}</td>
											<td>${cp.type=='P'?'Parent':'Child'}</td>
											<td>${cp.id}</td>
											<td>${cp.parentId==0?'-':cp.parentId}</td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.save"/>" /> <input
									type="submit" class="btn btn-danger" name="cancel" id="cancel"
									value="<fmt:message key="button.cancel"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>