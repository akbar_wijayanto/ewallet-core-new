<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="coreRole.title" /></title>
<meta name="heading" content="<fmt:message key='coreRole.heading'/>" />
<script type="text/javascript" src="<c:url value="/scripts/custom/core.role.js"/>"></script>
</head>
<div class="row">
	<div class="col-xs-12">
		<div id="breadcumbTitle">
			<fmt:message key='coreRole.message.edit' />
		</div>
		<spring:bind path="coreRole.*">
			<c:if test="${not empty status.errorMessages}">
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<c:forEach var="error" items="${status.errorMessages}">
						<c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:if>
		</spring:bind>

		<div class="box">
			<spring:url var="action" value='/core/role/edit/${coreRole.id}' />
			<form:form commandName="coreRole" method="post" action="${action}" id="coreRole" class="form-horizontal">
				<div class="box-body">
					<input type="hidden" name="from" value="<c:out value="${param.from}"/>" />

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="id" for="id"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="coreRole.id" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="id" id="id" readonly="true"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="id" cssClass="has-error" />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="name" for="name"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.name" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="name" id="name"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="name" cssClass="has-error" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-4 col-md-3">
							<form:label path="description" for="description"
								cssClass="control-label pull-right"
								cssErrorClass="control-label">
								<fmt:message key="Role.description" />
							</form:label>
						</div>
						<div class="col-xs-8 col-md-6">
							<form:input path="description" id="description"
								cssClass="form-control validate[required]"
								cssErrorClass="form-control has-error validate[required]" />
							<form:errors path="description" cssClass="has-error" />
						</div>
					</div>
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-xs-12 col-md-9">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" name="save"
									id="save" value="<fmt:message key="button.edit"/>" /> <input
									type="submit" class="btn btn-danger" name="cancel" id="cancel"
									value="<fmt:message key="button.cancel"/>" />
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>