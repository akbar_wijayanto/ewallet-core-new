/**
 * 
 */
package com.ewallet.webapp.converter;

/**Base converter interface for everything that needs to convert, i.e. from model to dto
 * @author arni.sihombing
 *
 * @param <C1>
 * @param <C2>
 */
public interface BaseConverter<C1, C2> {

	C1 convertToType1(C2 object);
	C2 convertToType2(C1 object);
	
}
