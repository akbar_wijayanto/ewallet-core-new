package com.ewallet.webapp.security;

import java.util.Calendar;

import org.aspectj.lang.JoinPoint;

import com.ewallet.Constants;
import com.ewallet.persistence.model.BaseGenericObject;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.security.bean.UserUtil;

public class BaseGenericObjectAspect {

	public void setGenericObject(JoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		Object newObject = (Object)args[0];	
		CoreUser user = UserUtil.getCurrentUser();

		// save from file set username to SYSTEM
		if (user == null) {
			user = new CoreUser();
			user.setUsername(Constants.SYSTEM);
		}
		if ("save".equals(jp.getSignature().getName()) 
				|| "saveWithAuthorize".equals(jp.getSignature().getName()) 
				&& user != null
				&& newObject instanceof BaseGenericObject) {

			if (((BaseGenericObject) newObject).getCreatedBy() == null) {
				((BaseGenericObject) newObject)
						.setCreatedBy(user.getUsername());
				((BaseGenericObject) newObject).setCreatedTime(Calendar
						.getInstance().getTime());
			}

			((BaseGenericObject) newObject).setUpdatedBy(user.getUsername());
			((BaseGenericObject) newObject).setUpdatedTime(Calendar
					.getInstance().getTime());

		}
	}
}
