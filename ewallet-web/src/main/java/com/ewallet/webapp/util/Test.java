package com.ewallet.webapp.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.ewallet.persistence.dao.impl.TrxOrderDaoImpl;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.util.DateUtil;

public class Test {

	@Autowired
	private static TrxOrderManager trxOrderManager;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*String value = "[CoreLimitReference [id=8203, description=Limit Child, shortName=WOW4, limitType=2, percentageUsage=100.00], CoreLimitReference [id=8201, description=Limit Child, shortName=WOW001, limitType=2, percentageUsage=100.00], CoreLimitReference [id=8202, description=Limit Child, shortName=WOW2, limitType=2, percentageUsage=100.00]]";
		String newValue = null;
		newValue = value.substring(1);
		newValue = newValue.substring(0, newValue.length() - 1);
		String[] firstSplit = newValue.split("\\],");
		firstSplit[firstSplit.length - 1] = firstSplit[firstSplit.length - 1]
				.substring(0, firstSplit[firstSplit.length - 1].length() - 1);
		String[] secondSplit = null;
		String[] thirdSplit = null;
		String[] fourthSplit = null;
		String[] idOnly = new String[firstSplit.length];
		for (int i = 0; i < firstSplit.length; i++) {
			secondSplit = firstSplit[i].split("\\[");
			for (int j = 0; j < secondSplit.length; j++) {
				thirdSplit = secondSplit[1].split(",");
				for (int k = 0; k < thirdSplit.length; k++) {
					fourthSplit = thirdSplit[0].split("=");
					idOnly[i] = fourthSplit[1];
					break;
				}
				break;
			}
		}
		String className = secondSplit[0].trim();
		String ids = StringUtils.join(idOnly, ",");
		newValue = className + " : " + ids;*/
		
		/*String value = "CoreLimitReference : 8203,8201,8202";
		String[] id = null;
		String[] firstSplit = value.split(" : ");
		String[] secondSplit = null;
		for (int i = 0; i < firstSplit.length; i++) {
			secondSplit = firstSplit[1].split(",");
			id = new String[secondSplit.length + 1];
			id[0] = firstSplit[0].trim();
			for (int j = 0; j < secondSplit.length; j++) {
				id[j+1] = secondSplit[j];
			}
		}
		System.out.println(StringUtils.join(id, ","));*/
		//System.out.println(hitOrder(trxOrderManager).size());
		//System.out.println(createDate());
		System.out.println(hardCodeTrxOrder().size());
	}

	private static Date createDate() {
		return new Date();
	}
	
	private static List<TrxOrder> hardCodeTrxOrder() {
		List<TrxOrder> bean = new ArrayList<TrxOrder>();
		TrxOrder t = new TrxOrder();
		CoreSeller cs = new CoreSeller();
		cs.setSellerId("imamNur");
		t.setCoreSeller(cs);
		t.setOrderDate(new Date());
		TrxOrderItemDetail toi = new TrxOrderItemDetail();
		toi.setQuantity(10);
		TrxOrderItemId toii = new TrxOrderItemId();
		CoreItem ci = new CoreItem();
		ci.setItemId("1");
		ci.setItemName("Nasi");
		ci.setSellingPrice(new BigDecimal("4000"));
		toii.setCoreItem(ci);
		toi.setPk(toii);
		t.getTrxOrderItemDetails().add(toi);
		bean.add(t);
		return bean;
	}

}
