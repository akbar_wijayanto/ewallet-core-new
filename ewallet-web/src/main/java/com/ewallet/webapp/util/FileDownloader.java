package com.ewallet.webapp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

public class FileDownloader {

	private static final int BUFFER_SIZE = 4096;
	
	public static void downloadFile(File downloadedFile, HttpServletResponse response, String extension) throws Exception{
		FileInputStream inputStream = new FileInputStream(downloadedFile);
        
        // get MIME type of the file
		String mimeType = getMimeType(extension);
        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
 
        // set content attributes for the response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadedFile.length());
 
        // set headers for the response
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"",
                downloadedFile.getName());
        response.setHeader(headerKey, headerValue);
 
        // get output stream of the response
        OutputStream outStream = response.getOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;
 
        // write bytes read from the input stream into the output stream
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        downloadedFile.delete();
        inputStream.close();
        outStream.close();
	}
	
	private static String getMimeType(String fileExtension){
		if(fileExtension.equals("pdf")){
			return "application/pdf";
		}else if(fileExtension.equals("xls")){
			return "application/vnd.xls";
		}else{
			return null;
		}
	}
	
}
