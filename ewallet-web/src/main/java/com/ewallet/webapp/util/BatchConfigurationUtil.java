/**
 * 
 */
package com.ewallet.webapp.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author <a href="mailto:widigdyo@anabatic.com">Widigdyo</a>
 * @version 1.0, Sep 8, 2015 3:38:24 PM
 */
public class BatchConfigurationUtil {
	
    public static final String BATCH_CONFIG = "META-INF/spring/batch/jobs/batch-execution-config.xml";
    public static final String BATCH_CONFIG_TEMPLATE = "META-INF/spring/batch/jobs/batch-execution-config.xml.template";
    
    public static final String BATCH_CONFIG_FULL_PATH = getFullPath() + "/" + BATCH_CONFIG; 
    public static final String BATCH_CONFIG_TEMPLATE_FULL_PATH = getFullPath() + "/" + BATCH_CONFIG_TEMPLATE;
    
	private static String readTemplate(String templateFileName) {
    	String res = "";
    	File file = new File(templateFileName);
    	if (file.exists()) {
    		BufferedReader br = null;
    		try {
    			String sCurrentLine;
    			br = new BufferedReader(new FileReader(file));
    			while ((sCurrentLine = br.readLine()) != null) {
    				res += sCurrentLine;
    			}
    		} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null)br.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
    	} else {
    		System.err.println("file " + templateFileName + " not found!!!");
    	}
    	
    	return res;
    }
    
    public static boolean generateJobsConfig(String filename, String templateFilename, String jobConfig) {
    	boolean res = false;
    	String template = readTemplate(templateFilename);
    	String strConfig = template.replace("{$jobLocation$}", jobConfig);
    	File file = new File(filename);
    	if (file.exists() && file.isFile()) {
    		file.delete();
    	}
    	try {
        	if (file.createNewFile()) {
        		FileWriter fw = new FileWriter(file);
        		BufferedWriter bw = new BufferedWriter(fw);
        		bw.write(strConfig);
        		bw.close();
        		res = true;
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	System.out.println(strConfig);
    	return res;
    }
    
    private static String getFullPath() {
        ClassLoader classLoader = BatchConfigurationUtil.class.getClassLoader();
        File classpathRoot = new File(classLoader.getResource("").getPath());
        return classpathRoot.getPath();
    }
}
