package com.ewallet.webapp.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.taglibs.standard.lang.jstl.Coercions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ewallet.enumeration.CustomerStatusEnum;
import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreItemType;
import com.ewallet.persistence.model.CoreMenu;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.service.CoreItemManager;
import com.ewallet.service.CoreItemTypeManager;
import com.ewallet.service.CoreMenuManager;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.util.BarcodeGenerator;
import com.ewallet.util.PropertiesUtil;

/**
 * @author akbar.wijayanto Date Oct 19, 2015 5:06:24 PM
 */
@Controller
@RequestMapping("/core/item")
public class CoreItemController extends BaseFormController {

	@Autowired
	private CoreSellerManager coreSellerManager;

	private CoreItemManager coreItemManager=null;

	@Autowired
	private CoreItemTypeManager coreItemTypeManager;

	@Autowired
	private CoreUserManager coreUserManager;

	public CoreItemController() {
		setCancelView("redirect:/core/item");
		setSuccessView("redirect:/core/item");
	}

	@Autowired
    public void setCoreItemManager(CoreItemManager coreItemManager) {
        this.coreItemManager = coreItemManager;
    }
	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("coreItems", coreItemManager.getAllLive());
		return "/core/item/list";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/authorize")
	public String view2(Model model) throws Exception {
		model.addAttribute("coreItems", coreItemManager.getAllAuthItem());
		return "/core/item/authorize/list";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception {
		model.addAttribute("coreItem", new CoreItem());
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		model.addAttribute("coreSellers", coreSellerManager.getAllLive());
		return "core/item/add";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid CoreItem coreItem,
			@RequestParam("file") MultipartFile file, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			System.out.println("cancel " + request.getParameter("cancel"));
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (!file.isEmpty()) {
			byte[] bytes = file.getBytes();

			System.out.println("---------------------------------------------"
					+ file.getOriginalFilename());

			// Creating the directory to store file
			String rootPath = PropertiesUtil.get("filePath");
			File dir = new File(rootPath + File.separator + "img");
			if (!dir.exists())
				dir.mkdirs();

			// Create the file on server
			String fileName = "";
			if (file.getContentType().equals("image/jpeg")) {
				fileName = "item_" + coreItem.getItemId() + ".jpg";
			}
			File serverFile = new File(dir.getAbsolutePath() + File.separator
					+ fileName);
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			System.out.println(serverFile.getAbsolutePath());
			// coreItem.setFilePath(serverFile.getAbsolutePath());
			coreItem.setFilePath("/resources/" + fileName);
		} else {
			coreItem.setFilePath("");
		}

		coreItemManager.save(coreItem);
		saveMessage(
				request,
				getText("coreItem.saved", coreItem.getItemName(),
						request.getLocale()));
		return "redirect:/core/item";
	}

	// @PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model)
			throws Exception {
		model.addAttribute("coreItem", coreItemManager.get(new Long(id)));
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		model.addAttribute("coreSellers", coreSellerManager.getAllLive());
		/*model.addAttribute("statuss", CustomerStatusEnum.values());*/
		return "core/item/edit";
	}

	// @PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@RequestParam("file") MultipartFile file, @Valid CoreItem coreItem, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (errors.hasErrors() && request.getParameter("delete") == null) {
			model.addAttribute("coreItemTypes",
					coreItemTypeManager.getAllLive());
			model.addAttribute("coreSellers", coreSellerManager.getAllLive());
			/*model.addAttribute("statuss", CustomerStatusEnum.values());*/
			return "core/item/edit";
		}
 
		if (!file.isEmpty()) {
			byte[] bytes = file.getBytes();

			System.out.println("---------------------------------------------"
					+ file.getOriginalFilename());

			// Creating the directory to store file
			String rootPath = System.getProperty("catalina.home");
			File dir = new File(rootPath + File.separator + "img");
			if (!dir.exists())
				dir.mkdirs();

			// Create the file on server
			String fileName = "";
			if (file.getContentType().equals("image/jpeg")) {
				fileName = "item_" + coreItem.getItemId() + ".jpg";
			}
			File serverFile = new File(dir.getAbsolutePath() + File.separator
					+ fileName);
			if(serverFile != null){serverFile.delete();}
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			System.out.println(serverFile.getAbsolutePath());
			// coreItem.setFilePath(serverFile.getAbsolutePath());
			coreItem.setFilePath("/resources/img/" + fileName);
		} else {
			String fileName = "item_" + coreItem.getItemId() + ".jpg";
			coreItem.setFilePath("/resources/img/" + fileName);
		}
		
		coreItemManager.save(coreItem);
		coreItemManager.saveWithAuthorize(coreItem);
		saveMessage(
				request,
				getText("coreItem.saved", coreItem.getItemName(),
						request.getLocale()));
		return "redirect:/core/item";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model) throws Exception {
		CoreItem coreItem = coreItemManager.getLiveById(id);
		model.addAttribute("coreItem", coreItem);
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		byte[] image = BarcodeGenerator.generateQRCodeToByteArray(coreItem.getItemId());
		String url = "data:image/png;base64," + Base64.encodeBase64String(image);
		model.addAttribute("image", url);
		return "core/item/detail";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id,
			HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		CoreItem coreItem = coreItemManager.getLiveById(id);
		coreItemManager.save(coreItem);
		return "redirect:/core/item";
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String detailDisplayDel(@PathVariable("id") Long id, Model model) throws Exception {
		CoreItem coreItem = coreItemManager.getLiveById(id);
		model.addAttribute("coreItem", coreItem);
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		byte[] image = BarcodeGenerator.generateQRCodeToByteArray(coreItem.getItemId());
		String url = "data:image/png;base64," + Base64.encodeBase64String(image);
		model.addAttribute("image", url);
		return "core/item/delete";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    protected String confirmDelete(CoreItem coreItem, BindingResult errors, HttpServletRequest request,HttpServletResponse response) throws Exception {
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        coreItemManager.remove(coreItem.getId());
        saveMessage(request, getText("coreItem.deleted", coreItem.getDescription(), request.getLocale()));
        return "redirect:/core/item";
    }
	// @RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	// public String managePermissionProcess(@PathVariable("id") Long id,
	// HttpServletRequest request, Model model) throws Exception {
	// if (request.getParameter("cancel") != null) {
	// if (!StringUtils.equals(request.getParameter("from"), "list")) {
	// return getCancelView();
	// } else {
	// return getSuccessView();
	// }
	// }
	//
	// CoreItem coreItem = coreItemManager.getLiveById(id);
	// coreItemManager.save(coreItem);
	//
	// return "redirect:/core/item";
	// }

	// @PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value = "/authorize/authItem/{id}", method = RequestMethod.GET)
	public String authDisplay(@PathVariable("id") Integer id, Model model)
			throws Exception {
		model.addAttribute("coreItem", coreItemManager.get(new Long(id)));
		CoreItem item = coreItemManager.get(new Long(id));
		model.addAttribute("coreItemTypes", coreItemTypeManager.getAllLive());
		model.addAttribute("coreSellers", coreSellerManager.getAllLive());
		model.addAttribute("statuss", (item.getStatus().equals("INAUACT")) ? "INACT" : "LIVE" );
		return "core/item/authorize/authItem";
	}

	// @PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value = "/authorize/authItem/{id}", method = RequestMethod.POST)
	public String authProcess(@PathVariable("id") Integer id,
			@Valid CoreItem coreItem, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (errors.hasErrors() && request.getParameter("delete") == null) {
			model.addAttribute("coreItemTypes",
					coreItemTypeManager.getAllLive());
			model.addAttribute("coreSellers", coreSellerManager.getAllLive());
			return "core/item/authorize/authItem";
		}

		CoreUser user = coreUserManager.getUserByUsername(coreItem
				.getCreatedBy());
		try {
			if (!user.equals(null)) {
				CoreRole role = user.getActiveRole();
				if (role.getId() == 2) {
					coreItem.setAuthoriser(coreItem.getCreatedBy());
					coreItem.setAuthorizeTime(new Date());
					//coreItem.setStatus(RecordStatusEnum.LIVE.getName());
					
					String statusSeller = coreItem.getStatus();
			         int status = (statusSeller.equals("INAUACT")? 1 : 0);
			         switch (status) {
			 		case 0:
			 			coreItem.setStatus(RecordStatusEnum.LIVE.getName());
			 			break;
			 		case 1:
			 			coreItem.setStatus(RecordStatusEnum.INACT.getName());
			 			break;
			 		default:
			 			break;
			 		}
			        CoreItem corItem = coreItemManager.get(new Long(id));
				    coreItem.setFilePath(corItem.getFilePath());
					coreItemManager.save(coreItem);
					saveMessage(
							request,
							getText("authorization.approve",
									coreItem.getItemName(), request.getLocale()));
					return "redirect:/core/item/authorize";
				} else {
					saveError(
							request,
							getText("authorization.error",
									coreItem.getItemName(), request.getLocale()));
					return "redirect:/core/item/authorize";
				}
			} else {
				saveError(
						request,
						getText("authorization.error", coreItem.getItemName(),
								request.getLocale()));
				return "redirect:/core/item/authorize";
			}
		} catch (Exception ex) {
			saveError(
					request,
					getText("authorization.error", coreItem.getItemName(),
							request.getLocale()));
		}
		return "redirect:/core/item/authorize";
	}
}
