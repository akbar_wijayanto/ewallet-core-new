package com.ewallet.webapp.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.enumeration.CustomerGenderEnum;
import com.ewallet.enumeration.CustomerStatusEnum;
import com.ewallet.enumeration.CustomerTitleEnum;
import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.report.dto.CoreSellerSettlementReportDto;
import com.ewallet.report.dto.CoreSellerTransactionReportDto;
import com.ewallet.report.dto.SellerTransactionBean;
import com.ewallet.report.service.CoreSellerReportService;
import com.ewallet.report.util.ClientReportGenerator;
import com.ewallet.service.CoreBankManager;
import com.ewallet.service.CoreRoleManager;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.util.BarcodeGenerator;
import com.ewallet.util.DateUtil;
import com.ewallet.webapp.util.FileDownloader;
import com.ewallet.webapp.util.UsernameGenerator;

/**
 * @author akbar.wijayanto 
 * Date Oct 16, 2015 9:21:10 PM
 */
@Controller
@RequestMapping("/core/seller")
public class CoreSellerController extends BaseFormController {

	private final static Long ROLE_SELLER = 2L;

	@Autowired
	private CoreSellerManager coreSellerManager;

	@Autowired
	private CoreUserManager coreUserManager;

	@Autowired
	private CoreRoleManager coreRoleManager;

	@Autowired
	private CoreBankManager coreBankManager;

	@Autowired
	private TrxRecordManager trxRecordManager;

	@Autowired
	private TrxOrderManager trxOrderManager;

	@Autowired
	private CoreSellerReportService coreSellerReportService;

	public CoreSellerController() {
		setCancelView("redirect:/core/seller");
		setSuccessView("redirect:/core/seller");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("coreSellers", coreSellerManager.getAllLive());
		return "/core/seller/list";
	}
	
	@RequestMapping(value = "/activation")
	public String aktivasiList(Model model) throws Exception {
		model.addAttribute("coreSellers", coreSellerManager.getInactiveSellerList());
		System.out.println("tampil size:"+coreSellerManager.getInactiveSellerList().size());
		return "/core/seller/activation/list";
	}

	@RequestMapping(value = "/authorize")
	public String authView(Model model) throws Exception {
		model.addAttribute("coreSellers", coreSellerManager.getAllAuthSeller());
		System.out.println("brapa?" +coreSellerManager.getAllAuthSeller().size());
		return "/core/seller/authorize/list";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception {
		model.addAttribute("coreSeller", new CoreSeller());
		model.addAttribute("genders", CustomerGenderEnum.values());
		model.addAttribute("titles", CustomerTitleEnum.values());
		model.addAttribute("coreBanks", coreBankManager.getAllLive());
		//		byte[] image = BarcodeGenerator.generateQRCodeToByteArray("1234567890");
		//		String url = "data:image/png;base64," + Base64.encodeBase64String(image);
		//		model.addAttribute("image", url);
		return "core/seller/add";
	}

	// @PreAuthorize("hasAnyRole('CORE:SECURITY:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid CoreSeller coreSeller,
			BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		String username = UsernameGenerator.generate(coreSeller.getFirstName().concat(" ").concat(coreSeller.getLastName()),
				coreSeller.getDateOfBirth());

		coreSeller.setSellerId(username);
		coreSeller.setUsername(username);
		coreSeller.setSessionTimeout(10L);
		coreSeller.setPassword(username);
		coreSeller.setAccountEnabled(true);
		//		coreSeller.setStatus(RecordStatusEnum.INACT.getName());
		//		coreUserManager.saveWithAuthorize(coreSeller);
		coreUserManager.save(coreSeller);
		saveMessage(request, getText("coreSeller.create", String.valueOf(coreSeller.getId()), request.getLocale()));

		return "redirect:/core/seller";
	}

	//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreSeller", coreSellerManager.get(new Long(id)));
		model.addAttribute("genders", CustomerGenderEnum.values());
		model.addAttribute("titles", CustomerTitleEnum.values());
		model.addAttribute("statuss", CustomerStatusEnum.values());
		model.addAttribute("coreBanks", coreBankManager.getAllLive());
		return "core/seller/edit";
	}

	//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@RequestParam("status") int status, @Valid CoreSeller coreSeller, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{

		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (errors.hasErrors() && request.getParameter("delete") == null) {
			model.addAttribute("genders", CustomerGenderEnum.values());
			model.addAttribute("titles", CustomerTitleEnum.values());
			model.addAttribute("coreBanks", coreBankManager.getAllLive());
			model.addAttribute("statuss", CustomerStatusEnum.values());
			return "core/seller";
		}
		switch (status) {
		case 0:
			coreSeller.setStatus("INAU");
			break;
		case 1:
			coreSeller.setStatus("INAUACT");
			break;
		default:
			break;
		}

		//coreSellerManager.save(coreSeller);
		//CoreRole role = coreUserManager.getUserByUsername(coreSeller.getCreatedBy()).getActiveRole();
		//if(role.getId()==2){

		coreUserManager.save(coreSeller);
		saveMessage(request, getText("coreSeller.edit", String.valueOf(coreSeller.getId()), request.getLocale()));
		//}

		return "redirect:/core/seller";
	}

	//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {
		CoreSeller coreSeller = coreSellerManager.getLiveById(id);
		model.addAttribute("coreSeller", coreSeller);
		byte[] image = BarcodeGenerator.generateQRCodeToByteArray(coreSeller.getSellerId());
		String url = "data:image/png;base64," + Base64.encodeBase64String(image);
		model.addAttribute("image", url);
		model.addAttribute("genders", CustomerGenderEnum.values());
		model.addAttribute("titles", CustomerTitleEnum.values());
		model.addAttribute("coreBanks", coreBankManager.getAllLive());
		return "core/seller/detail";
	}

	//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		CoreSeller coreSeller = coreSellerManager.getLiveById(id);
		coreSellerManager.save(coreSeller);
		return "redirect:/core/seller";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String detailDisplayDel(@PathVariable("id") Long id, Model model)
			throws Exception {
		CoreSeller coreSeller = coreSellerManager.getLiveById(id);
		model.addAttribute("coreSeller", coreSeller);
		byte[] image = BarcodeGenerator.generateQRCodeToByteArray(coreSeller.getSellerId());
		String url = "data:image/png;base64," + Base64.encodeBase64String(image);
		model.addAttribute("image", url);
		model.addAttribute("genders", CustomerGenderEnum.values());
		model.addAttribute("titles", CustomerTitleEnum.values());
		model.addAttribute("coreBanks", coreBankManager.getAllLive());
		return "core/seller/delete";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    protected String confirmDelete(CoreUser coreUser, BindingResult errors, HttpServletRequest request,HttpServletResponse response) throws Exception {
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        try {
        	coreUserManager.removeUser(coreUser.getId().toString());
		} catch (DataIntegrityViolationException e) {
			saveError(request, getText("coreSeller.constraintViolation", String.valueOf(coreUser.getId()), request.getLocale()));
			return "redirect:/core/seller";
		}
//        coreUserManager.setHistoryToSeller(coreUser.getId());
        saveMessage(request, getText("coreUser.deleted", request.getLocale()));
        return "redirect:/core/seller";
    }
	/*@RequestMapping(value = "/report/settlement", method=RequestMethod.GET)
    public String getSellerTransactionReport(Model model) {
//		List<CoreSellerSettlementReportDto> list = new ArrayList<CoreSellerSettlementReportDto>();

		List<TrxRecord> records = trxRecordManager.getSellerSettlementReport(null, "Endang", DateUtil.addDate(new Date(), -4), DateUtil.addDate(new Date(), 2));

    	model.addAttribute("settlement", records);
    	return "core/seller/report/list";
    }*/

	@RequestMapping(value="/report/sellerSettlement", method = RequestMethod.POST)
	public List<CoreSellerSettlementReportDto> getSellerSettlement(
			@RequestParam("startDate") String startDatePar, @RequestParam("endDate") String endDatePar,
			@RequestParam("name") String namePar, @RequestParam("sellerId") String sellerIdPar,
			/*@RequestParam("usernamePar") String usernamePar,*/
			HttpServletRequest request) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat(getText("date.format", request.getLocale()));
		Date startDate = null;
		Date endDate = null;

		if(!startDatePar.isEmpty() && !endDatePar.isEmpty()){
			try {
				startDate = sdf.parse(startDatePar);
				endDate = sdf.parse(endDatePar);
				endDate = DateUtil.addDate(endDate, 1);
			} catch (ParseException e) {
				startDate = null;
				endDate = null;
			}
		}

		List<CoreSellerSettlementReportDto> list = new ArrayList<CoreSellerSettlementReportDto>();
		List<TrxRecord> listSettlement = trxRecordManager.getSellerSettlementReport(sellerIdPar, namePar, startDate, endDate);

		for (TrxRecord trxRecord : listSettlement) {
			CoreSellerSettlementReportDto dto = new CoreSellerSettlementReportDto();
		}

		return list;

	}

	//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value="/authorize/authSeller/{id}", method = RequestMethod.GET)
	public String authDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreSeller", coreSellerManager.get(new Long(id)));
		CoreSeller seller = coreSellerManager.get(new Long(id));
		model.addAttribute("genders", CustomerGenderEnum.values());
		model.addAttribute("titles", CustomerTitleEnum.values());
		//model.addAttribute("statuss", CustomerStatusEnum.values());
		//model.addAttribute("statuss", (seller.getStatus().equals("INAUACT")) ? "INACT" : "LIVE" );
		model.addAttribute("coreBanks", coreBankManager.getAllLive());
		return "core/seller/authorize/authSeller";
	}

	//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
	@RequestMapping(value="/authorize/authSeller/{id}", method = RequestMethod.POST)
	public String authProcess(@Valid CoreSeller coreSeller, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (errors.hasErrors() && request.getParameter("delete") == null ) {
			return "core/seller/authorize/authSeller";
		}

		CoreUser user = coreUserManager.getUserByUsername(coreSeller.getCreatedBy());
		try {
			if(!user.equals(null)){
				CoreRole role = user.getActiveRole();
				if (role.getId()==2){
					coreSeller.setAuthoriser(coreSeller.getCreatedBy());
					coreSeller.setAuthorizeTime(new Date());
					//coreSeller.setStatus(RecordStatusEnum.LIVE.getName());
					String statusSeller = coreSeller.getStatus();
		         int status = (statusSeller.equals("INAUACT")? 1 : 0);
		         switch (status) {
		 		case 0:
		 			coreSeller.setStatus(RecordStatusEnum.LIVE.getName());
		 			break;
		 		case 1:
		 			coreSeller.setStatus(RecordStatusEnum.INACT.getName());
		 			break;
		 		default:
		 			break;
		 		}
					coreSellerManager.save(coreSeller);
					saveMessage(request, getText("authorization.approve", String.valueOf(coreSeller.getId()), request.getLocale()));
					return "redirect:/core/seller/authorize/";
				} else {
					saveError(request, getText("authorization.error", String.valueOf(coreSeller.getId()), request.getLocale()));
					return "redirect:/core/seller/authorize/";
				}
			} else{
				saveError(request, getText("authorization.error", String.valueOf(coreSeller.getId()), request.getLocale()));
				return "redirect:/core/seller/authorize/";
			}
		}catch(Exception ex) {
			saveError(request, getText("authorization.error", String.valueOf(coreSeller.getId()), request.getLocale()));
		}
		return "redirect:/core/seller/authorize/";

	}

	@RequestMapping(value = "/report/trxseller", method=RequestMethod.GET)
	public String reportView(Model model) throws Exception {
		return "/core/seller/report/trxseller/list";
	}

	@RequestMapping(value = "/report/trxseller", method=RequestMethod.POST)
	public String reportPost(@RequestParam(value="sellerId", required=false) String sellerId,
			@RequestParam("startDate") @DateTimeFormat(iso=ISO.DATE) Date startDate, 
			@RequestParam("endDate") @DateTimeFormat(iso=ISO.DATE) Date endDate,
			HttpServletRequest request, Model model) throws Exception{
			
		String sDate = "";
    	String eDate ="";
    	
    	if(startDate.compareTo(endDate)>0){
    	      saveError(request, getText("startdate.error", request.getLocale()));      
    	     }else if (startDate.compareTo(new Date())>0 || endDate.compareTo(new Date())>0){
    	      saveError(request, getText("endstart.over.error", request.getLocale()));
    	     }else if (startDate.equals(null)){
    	      saveError(request, getText("startdate.null", request.getLocale()));
    	     }else if (endDate.equals(null)){
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	      Date date = new Date();
    	      eDate = spdf.format(date);
    	      sDate = spdf.format(startDate);
    	     }else {
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	         sDate = spdf.format(startDate);
    	         eDate = spdf.format(endDate);
    	     }
    		model.addAttribute("sellerId", sellerId);
    		model.addAttribute("startDate", new SimpleDateFormat("dd-MM-yyyy").format(startDate));
    		model.addAttribute("endDate", new SimpleDateFormat("dd-MM-yyyy").format(endDate));
    		try {
    			if (sellerId.equals("")||sellerId.equals(null)){
        			model.addAttribute("trxOrder", coreSellerReportService.getAllSellerTransactionPeriodicReport(sDate, eDate).getCoreSellerTransactionReportDtoBean());
        			model.addAttribute("grandTotal", coreSellerReportService.getAllSellerTransactionPeriodicReport(sDate, eDate).getGrandTotal());
        			return "/core/seller/report/trxseller/list";
            	}else {
            		model.addAttribute("trxOrder", coreSellerReportService.getAllSellerTransactionPeriodicReportBySellerId(sellerId, sDate, eDate).getCoreSellerTransactionReportDtoBean());
            		model.addAttribute("grandTotal", coreSellerReportService.getAllSellerTransactionPeriodicReportBySellerId(sellerId, sDate, eDate).getGrandTotal());
            		return "/core/seller/report/trxseller/list";
            	}
			} catch (Exception e) {
				return "/core/seller/report/trxseller/list";
			}
    		
	}

	@RequestMapping(value = "/report/settlement", method=RequestMethod.GET)
	public String reportView2(Model model) throws Exception {
		model.addAttribute("trxSet", coreSellerReportService.getAllSellerSettlementReport());
		return "/core/seller/report/settlement/list";
	}

	@RequestMapping(value = "/download/all/{type}/{category}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadAllData(@PathVariable("type") String type, @PathVariable("category") String category,
			@RequestParam(value="sellerId", required=false) String sellerId,
			@RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = ISO.DATE) Date endDate,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String sDate = "";
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		sDate = spdf.format(startDate);
		eDate = spdf.format(endDate);
		System.out.println("seller"+sellerId+"id");
		//List<CoreSellerTransactionReportDto> dtoTrxSeller = coreSellerReportService.getAllSellerTransactionPeriodicReportBySellerId(sellerId, sDate, eDate).getCoreSellerTransactionReportDtoBean();
		List<CoreSellerTransactionReportDto> dtoTrxSeller = new ArrayList<CoreSellerTransactionReportDto>();
		if (sellerId.equals("")||sellerId.equals(null)){
			dtoTrxSeller = coreSellerReportService.getAllSellerTransactionPeriodicReport(sDate, eDate).getCoreSellerTransactionReportDtoBean();
			return getExport(dtoTrxSeller, type, response, category);
		}else {
			dtoTrxSeller = coreSellerReportService.getAllSellerTransactionPeriodicReportBySellerId(sellerId, sDate, eDate).getCoreSellerTransactionReportDtoBean();
			return getExport(dtoTrxSeller, type, response, category);
		}		
		
	}
	
	@RequestMapping(value = "/settlement/download/all/{type}/{category}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadAllData2(@PathVariable("type") String type, @PathVariable("category") String category,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		List<CoreSellerSettlementReportDto> dtoSettlement = coreSellerReportService.getAllSellerSettlementReport();
		if (dtoSettlement == null) {
			dtoSettlement = new ArrayList<CoreSellerSettlementReportDto>();
		}
		return getExport(dtoSettlement, type, response, category);
	}

	private <T> String getExport(List<T> dtos, String type, HttpServletResponse response, String category) throws Exception {
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		eDate = spdf.format(date);
		if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			File downloadFile = getFile(category.equals("trxseller") ? "trxSeller_"+eDate : "settlement_"+eDate, "pdf", category.equals("trxseller") ? "trxSeller_all.jrxml" : "settlement_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		if (type.equals("xls")) {
			response.setContentType("application/vnd.xls");
			File downloadFile = getFile(category.equals("trxseller") ? "trxSeller_"+eDate : "settlement_"+eDate, "xls", category.equals("trxseller") ? "trxSeller_all.jrxml" : "settlement_all.jrxml", dtos);
			FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
		}
		return null;
	}

	private <T> File getFile(String fileName, String format, String jrxmlFileName,
			List<T> dtos) throws RetailException {
		return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws RetailException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		return downloadFile;
	}
}
