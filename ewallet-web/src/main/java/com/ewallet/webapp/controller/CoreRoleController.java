package com.ewallet.webapp.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.persistence.model.CoreMenu;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.service.CoreMenuManager;
import com.ewallet.service.CoreRoleManager;
import com.ewallet.service.CoreUserManager;

@Controller
@RequestMapping(value="/core/role")
public class CoreRoleController extends BaseFormController {
	private CoreRoleManager coreRoleManager = null;
	private CoreMenuManager coreMenuManager;
	private CoreUserManager coreUserManager;
	
	
	public CoreRoleController(){
		setCancelView("redirect:/core/role");
		setSuccessView("redirect:/core/role");
	}

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	@Autowired
	public void setCoreRoleManager(CoreRoleManager coreRoleManager){
		this.coreRoleManager = coreRoleManager;
	}
	
	@Autowired
	public void setCoreMenuManager(CoreMenuManager coreMenuManager){
		this.coreMenuManager = coreMenuManager;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="")
	public String allDisplay(Model model) throws Exception{
		model.addAttribute("coreRoles", coreRoleManager.getAll());
		return "core/role/list";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.GET)
	public String addDisplay(Model model) throws Exception{
		model.addAttribute("coreRole", new CoreRole());
		return "core/role/add";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/add", method = RequestMethod.POST)
	public String addProcess(@Valid CoreRole coreRole, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (errors.hasErrors() && request.getParameter("delete") == null) {
        	return "core/role/add";
        }
        CoreRole existing = this.coreRoleManager.getRole(coreRole.getName());
        if (existing != null) {
            saveError(request, getText("errors.exists", coreRole.getName(), request.getLocale()));
            return "core/role/add";
        }
//        try {
//        	if(coreRole.getId() == null){
//        		coreRole.setId(Integer.MIN_VALUE);
//        	}
//            coreRoleManager.get(coreRole.getId());
//            saveError(request, getText("errors.exists", String.valueOf(coreRole.getId()), request.getLocale()));
//            return "core/role/add";
//
//        } catch (ObjectRetrievalFailureException ex) {
            coreRoleManager.save(coreRole);
            saveMessage(request, getText("coreRole.saved", coreRole.getName(), request.getLocale()));
//        }
        
        return "redirect:/core/role";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:DELETE:*')")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public String deleteDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/delete";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:DELETE:*')")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
	public String deleteProcess(CoreRole coreRole, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

		
		if(!coreUserManager.getCheckRoleOnUser(coreRole.getId()).isEmpty()){
			//System.out.println("lewat");
			saveError(request, getText("coreRole.roleUsedByUser", coreRole.getId().toString(), request.getLocale()));
			return "core/role/delete";
		}
        coreRoleManager.remove(coreRole.getId());
        saveMessage(request, getText("coreRole.deleted", coreRole.getName(), request.getLocale()));
        return "redirect:/core/role";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/edit";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:INPUT:*')")
    @RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid CoreRole coreRole, BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (errors.hasErrors() && request.getParameter("delete") == null) {
            return "core/role/edit";
        }
        coreRoleManager.save(coreRole);
        saveMessage(request, getText("coreRole.saved", String.valueOf(coreRole.getId()), request.getLocale()));
        return "redirect:/core/role";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/detail/{id}")
	public String detailDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		model.addAttribute("coreRole", coreRoleManager.getWithListAll(id));
		return "core/role/detail";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/managemenu/{id}", method = RequestMethod.GET)
	public String addMenuDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		 
		 CoreRole coreRole = coreRoleManager.getWithListAll(id);
		 List<CoreMenu> cps = coreMenuManager.getAll();
	     Set<CoreMenu> ucps = coreRole.getCoreMenus();
//	     List<LabelValueStatus> lvs = new ArrayList<LabelValueStatus>();
	     for (CoreMenu cp : cps) {
	          Boolean status =false;
	          if (ucps.contains(cp)) {
	              status = true;
	          }
	          cp.setChecklistStatus(status);
//	          lvs.add(new LabelValueStatus(cp.getDescription(), cp.getId().toString(), status));
	     }
	     model.addAttribute("coreRole", coreRole);
	     model.addAttribute("coreMenus",cps );
		return "core/role/managemenu";
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/managemenu/{id}", method = RequestMethod.POST)
	public String saveMenuDisplay(@PathVariable("id") Integer id, @RequestParam(value="coreMenus",required=false) Integer[] menuIDs, 
			HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception{
		
		if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

		if(menuIDs==null){
			CoreRole coreRole = coreRoleManager.getWithListAll(id);
			 List<CoreMenu> cps = coreMenuManager.getAll();
		     Set<CoreMenu> ucps = coreRole.getCoreMenus();
//		     List<LabelValueStatus> lvs = new ArrayList<LabelValueStatus>();
		     for (CoreMenu cp : cps) {
		          Boolean status =false;
		          if (ucps.contains(cp)) {
		              status = true;
		          }
		          cp.setChecklistStatus(status);
//		          lvs.add(new LabelValueStatus(cp.getDescription(), cp.getId().toString(), status));
		     }
		     model.addAttribute("coreRole", coreRole);
		     model.addAttribute("coreMenus",cps );
			model.addAttribute("errors", "please select role first");
    		return "core/role/managemenu";
		}
            
		CoreRole coreRole = coreRoleManager.getWithListAll(id);
		
		Set<CoreMenu> coreMenus = new HashSet<CoreMenu>();
		for (Integer menuID : menuIDs) {
			coreMenus.add(coreMenuManager.get(menuID));
		}
		
		coreRole.setCoreMenus(coreMenus);
		coreRoleManager.save(coreRole);
		return "redirect:/core/role/detail/" + id;
	}
	
//	@PreAuthorize("hasAnyRole('CORE:ROLE:READ:*')")
    @RequestMapping(value="/userrole/{id}")
	public String userRoleDisplay(@PathVariable("id") Integer id, Model model) throws Exception{
		CoreRole coreRole = coreRoleManager.getWithListAll(id);	
		model.addAttribute("coreRole", coreRole);
		model.addAttribute("coreUserRole", coreUserManager.getUserRoleList(id));
		return "core/role/userrole";
	}
}
