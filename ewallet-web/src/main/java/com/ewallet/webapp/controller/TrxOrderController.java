package com.ewallet.webapp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.CoreItem;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.report.dto.OrderDto;
import com.ewallet.report.service.GenerateReportService;
import com.ewallet.report.util.ClientReportGenerator;
import com.ewallet.service.TrxOrderItemDetailManager;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.webapp.util.FileDownloader;

/**
 * @author akbar.wijayanto
 * Date Oct 21, 2015 5:41:33 PM
 */
@Controller
@RequestMapping("/trx/order")
public class TrxOrderController extends BaseFormController {

	@Autowired
	private TrxOrderManager trxOrderManager;

	@Autowired
	private TrxOrderItemDetailManager trxOrderItemDetailManager;
	
	@Autowired
	private GenerateReportService generateReportService;
	
	public TrxOrderController() {
		setCancelView("redirect:/trx/order");
		setSuccessView("redirect:/trx/order");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public String view(Model model) throws Exception {
		//model.addAttribute("trxOrders", trxOrderManager.getAll());
		return "/trx/order/list";
	}

	@RequestMapping(value = "", method=RequestMethod.POST)
	public String reportPost(@RequestParam(value="sellerId", required=false) String sellerId,
			@RequestParam("startDate") @DateTimeFormat(iso=ISO.DATE) Date startDate, 
			@RequestParam("endDate") @DateTimeFormat(iso=ISO.DATE) Date endDate,
			HttpServletRequest request, Model model) throws Exception{
		String sDate = "";
    	String eDate ="";
    	
//    	if(startDate.compareTo(endDate)>0){
//    	      saveError(request, getText("startdate.error", request.getLocale()));      
//    	     }else if (startDate.compareTo(new Date())>0 || endDate.compareTo(new Date())>0){
//    	      saveError(request, getText("endstart.over.error", request.getLocale()));
//    	     }else if (startDate.equals(null)){
//    	      saveError(request, getText("startdate.null", request.getLocale()));
//    	     }else if (endDate.equals(null)){
//    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
//    	      Date date = new Date();
//    	      eDate = spdf.format(date);
//    	      sDate = spdf.format(startDate);
//    	     }else {
    	      SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
    	         sDate = spdf.format(startDate);
    	         eDate = spdf.format(endDate);
//    	     }
	    	model.addAttribute("sellerId", sellerId);
			model.addAttribute("startDate", new SimpleDateFormat("dd-MM-yyyy").format(startDate));
			model.addAttribute("endDate", new SimpleDateFormat("dd-MM-yyyy").format(endDate));
			
			try {
				if (sellerId.equals("")||sellerId.equals(null)){
			    	model.addAttribute("trxOrders", generateReportService.getListOrderDownload(sDate, eDate).getOrderDtoBean());
			    	model.addAttribute("grandTotal", generateReportService.getListOrderDownload(sDate, eDate).getGrandTotal());
			    	model.addAttribute("grandTotalPokok", generateReportService.getListOrderDownload(sDate, eDate).getGrandTotalPokok());
			    	return "/trx/order/list"; 
				}else{
					model.addAttribute("trxOrders", generateReportService.getListOrderBySellerId(sellerId, sDate, eDate).getOrderDtoBean());
			    	model.addAttribute("grandTotal", generateReportService.getListOrderBySellerId(sellerId, sDate, eDate).getGrandTotal());
			    	model.addAttribute("grandTotalPokok", generateReportService.getListOrderBySellerId(sellerId, sDate, eDate).getGrandTotalPokok());
			    	return "/trx/order/list"; 
				}
			} catch (Exception e) {
				return "/trx/order/list";
			}
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") String id, Model model)
			throws Exception {
		//TrxOrder trxOrder = trxOrderManager.getLiveById(id);
		TrxOrder trxOrder = trxOrderManager.get(id);
		model.addAttribute("trxOrder", trxOrder);
		List<CoreItem> items = new ArrayList<CoreItem>();
		for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
			CoreItem item = new CoreItem();
			item.setItemName(orderDet.getPk().getCoreItem().getItemName());
			item.setSellingPrice(orderDet.getPk().getCoreItem().getSellingPrice());
			items.add(item);
		}
		model.addAttribute("orderDetails", items);
		return "trx/order/detail";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") String id, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		TrxOrder trxOrder = trxOrderManager.getLiveById(id);
		trxOrderManager.save(trxOrder);
		return "redirect:/trx/order";
	}
 
	@RequestMapping(value = "/download/all/{type}", method = {RequestMethod.POST, RequestMethod.GET})
	 public String downloadAllData(@PathVariable("type") String type,
			 @RequestParam(value="sellerId", required=false) String sellerId,
			 @RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) Date startDate,
			 @RequestParam("endDate") @DateTimeFormat(iso = ISO.DATE) Date endDate,
	   HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		String sDate = "";
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		sDate = spdf.format(startDate);
		eDate = spdf.format(endDate);
		
		List<OrderDto> trxOrder = new ArrayList<OrderDto>();
		if(sellerId.equals("")||sellerId.equals(null)){
			trxOrder = generateReportService.getListOrderDownload(sDate, eDate).getOrderDtoBean();
			return getExport(trxOrder, type, response);
		}else{
			trxOrder = generateReportService.getListOrderBySellerId(sellerId, sDate, eDate).getOrderDtoBean();
			return getExport(trxOrder, type, response);
		}
	    
	 }
	    
	    private <T> String getExport(List<T> dtos, String type, HttpServletResponse response) throws Exception {
	     if (type.equals("pdf")) {
	            response.setContentType("application/pdf");
	            File downloadFile = getFile("trxOrder_all", "pdf", "trxOrder_all.jrxml", dtos);
	            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
	        }
	        if (type.equals("xls")) {
	            response.setContentType("application/vnd.xls");
	            File downloadFile = getFile("trxOrder_all", "xls", "trxOrder_all.jrxml", dtos);
	            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
	        }
	        return null;
	 }

	    private <T> File getFile(String fileName, String format, String jrxmlFileName, List<T> dtos) throws RetailException {
	     return getData(fileName, jrxmlFileName, format, dtos);
	 }

	 private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws RetailException {
	  File downloadFile = null;
	  if (format.equals("pdf")) {
	   downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
	  }
	  if (format.equals("xls")) {
	   downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
	  }
	  return downloadFile;
	 }	
	
}
