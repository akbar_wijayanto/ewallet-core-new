package com.ewallet.webapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.enumeration.RecordTypeEnum;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.service.CoreUserManager;

/**
 * @author akbar.wijayanto
 * Date Oct 26, 2015 11:20:12 AM
 */
@Controller
@RequestMapping(value = "/registration")
public class RegistrationController extends BaseFormController {
	private CoreUserManager coreUserManager;
	
	public RegistrationController() {
		setCancelView("redirect:/mainMenu");
		setSuccessView("redirect:/mainMenu");
	}

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String manageRoleDisplay(@PathVariable("id") String id, Model model, HttpServletRequest request)
			throws Exception {
		
		CoreUser coreUser = coreUserManager.getByToken(id);
		if (coreUser==null) {
			saveError(request, getText("coreuser.not.found", "Data not found", request.getLocale()));
		} else if (coreUser.getAccountEnabled()==true) {
			saveError(request, getText("coreuser.already.activated", "Data already activated", request.getLocale()));
			return "activationSuccess";
		}
		
		coreUser.setStatus(RecordStatusEnum.LIVE.getName());
		coreUser.setAccountEnabled(true);
		coreUser = coreUserManager.save(coreUser);
		saveMessage(request, getText("coreuser.activated", "Your data has been activated", request.getLocale()));
		return "redirect:/activationSuccess";
	}
}
