package com.ewallet.webapp.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxRefund;
import com.ewallet.report.dto.RefundDto;
import com.ewallet.report.dto.SellerTransactionBean;
import com.ewallet.report.service.GenerateReportService;
import com.ewallet.report.util.ClientReportGenerator;
import com.ewallet.service.TrxOrderManager;
import com.ewallet.service.TrxRefundManager;
import com.ewallet.webapp.util.FileDownloader;

/**
 * @author akbar.wijayanto Date Oct 22, 2015 9:58:42 AM
 */
@Controller
@RequestMapping("/trx/refund")
public class TrxRefundController extends BaseFormController {

	@Autowired
	private TrxRefundManager trxRefundManager;

	@Autowired
	private TrxOrderManager trxOrderaManager;

	@Autowired
	private GenerateReportService generateReportService;
	
	public TrxRefundController() {
		setCancelView("redirect:/trx/refund");
		setSuccessView("redirect:/trx/refund");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String view(Model model) throws Exception {
		// model.addAttribute("trxRefunds", trxRefundManager.getAllLive());
		return "/trx/refund/list";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String reportPost(@RequestParam(value="sellerId", required=false) String sellerId,
			@RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = ISO.DATE) Date endDate,
			HttpServletRequest request, Model model) throws Exception {
		String sDate = "";
		String eDate = "";

		if (startDate.compareTo(endDate) > 0) {
			saveError(request, getText("startdate.error", request.getLocale()));
			return "redirect:/trx/refund/";
		} else if (startDate.compareTo(new Date()) > 0
				|| endDate.compareTo(new Date()) > 0) {
			saveError(request,
					getText("endstart.over.error", request.getLocale()));
			return "redirect:/trx/refund/";
		} else if (startDate.equals(null)) {
			saveError(request, getText("startdate.null", request.getLocale()));
			return "redirect:/trx/refund/";
		} else if (endDate.equals(null)) {
			SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			eDate = spdf.format(date);
			sDate = spdf.format(startDate);
		} else {
			SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
			sDate = spdf.format(startDate);
			eDate = spdf.format(endDate);
		}
		model.addAttribute("sellerId", sellerId);
		model.addAttribute("startDate", new SimpleDateFormat("dd-MM-yyyy").format(startDate));
		model.addAttribute("endDate", new SimpleDateFormat("dd-MM-yyyy").format(endDate));
		try {
			if (sellerId.equals("")||sellerId.equals(null)){
				//model.addAttribute("trxRefunds", trxOrderaManager.getPeriodicRefund(sDate, eDate));
				model.addAttribute("trxRefund", generateReportService.getListRefundDownload(sDate, eDate).getRefundDtoBean());
				model.addAttribute("grandTotal", generateReportService.getListRefundDownload(sDate, eDate).getGrandTotal());
				return "/trx/refund/list";
			}else{
				model.addAttribute("trxRefund", generateReportService.getListRefundBySellerId(sellerId, sDate, eDate).getRefundDtoBean());
				model.addAttribute("grandTotal", generateReportService.getListRefundBySellerId(sellerId, sDate, eDate).getGrandTotal());
				return "/trx/refund/list";
			}
		} catch (Exception e) {
			return "/trx/refund/list";
		}
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") String id, Model model)
			throws Exception {
		//TrxRefund trxRefund = trxRefundManager.getLiveById(id);
		TrxOrder trxRefund = trxOrderaManager.get(id);
		model.addAttribute("trxRefund", trxRefund);
		return "trx/refund/detail";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id,
			HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		TrxRefund trxRefund = trxRefundManager.getLiveById(id);
		model.addAttribute("trxRefund", trxRefund);
		return "redirect:/trx/refund";
	}

	@RequestMapping(value = "/download/all/{type}/{category}", method = {RequestMethod.POST, RequestMethod.GET})
	public String downloadAllData(@PathVariable("type") String type, @PathVariable("category") String category,
			@RequestParam(value="sellerId", required=false) String sellerId,
			@RequestParam("startDate") @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam("endDate") @DateTimeFormat(iso = ISO.DATE) Date endDate,
			HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
		System.out.println("Category Report : "+category);
		String sDate = "";
		String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		sDate = spdf.format(startDate);
		eDate = spdf.format(endDate);

		List<RefundDto> dtoTrxRefund = new ArrayList<RefundDto>();
		if (sellerId.equals("")||sellerId.equals(null)){
			dtoTrxRefund = generateReportService.getListRefundDownload(sDate, eDate).getRefundDtoBean();
			return getExport(dtoTrxRefund, type, response, category);
		}else {
			dtoTrxRefund = generateReportService.getListRefundBySellerId(sellerId, sDate, eDate).getRefundDtoBean();
			return getExport(dtoTrxRefund, type, response, category);
		}
       
	}
    
    private <T> String getExport(List<T> dtos, String type, HttpServletResponse response, String category) throws Exception {
    	String eDate = "";
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		eDate = spdf.format(date);
    	if (type.equals("pdf")) {
            response.setContentType("application/pdf");
            File downloadFile = getFile("trxRefund_"+eDate, "pdf", "trxRefund_all.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
        if (type.equals("xls")) {
            response.setContentType("application/vnd.xls");
            File downloadFile = getFile("trxRefund_"+eDate, "xls", "trxRefund_all.jrxml", dtos);
            FileDownloader.downloadFile(downloadFile, response, FilenameUtils.getExtension(downloadFile.getAbsolutePath()));
        }
        return null;
	}

    private <T> File getFile(String fileName, String format, String jrxmlFileName,
			List<T> dtos) throws RetailException {
    	return getData(fileName, jrxmlFileName, format, dtos);
	}

	private <T> File getData(String fileName, String jrxmlFileName, String format, List<T> list) throws RetailException {
		File downloadFile = null;
		if (format.equals("pdf")) {
			downloadFile = ClientReportGenerator.generatePDF(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		if (format.equals("xls")) {
			downloadFile = ClientReportGenerator.generateXLS(jrxmlFileName, list, ClientReportGenerator.readProperty("jrxml.tempDir")+fileName);
		}
		return downloadFile;
	}

}
