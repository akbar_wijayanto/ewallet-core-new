package com.ewallet.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.persistence.model.TrxTopup;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.TrxRecordManager;
import com.ewallet.service.TrxTopupManager;
import com.ewallet.service.VirtualAccountManager;
import com.ewallet.util.DateUtil;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:32:27 AM
 */
@Controller
@RequestMapping("/trx/topup")
public class TrxTopupController extends BaseFormController {

	@Autowired
	private TrxTopupManager trxTopupManager;
	
	@Autowired
	private VirtualAccountManager virtualAccountManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	public TrxTopupController() {
		setCancelView("redirect:/trx/topup");
		setSuccessView("redirect:/trx/topup");
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String view(Model model) throws Exception {
		model.addAttribute("trxTopups", trxTopupManager.getAllLive());
		return "/trx/topup/list";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addDisplay(Model model)
			throws Exception {
		model.addAttribute("trxRecord", new TrxRecord());
		return "trx/topup/add";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid TrxRecord trxRecord,
			BindingResult errors, HttpServletRequest request,
			HttpServletResponse response, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		
		VirtualAccount accountTo = virtualAccountManager.get(trxRecord.getPairedAccount());
		if(accountTo==null) {
			saveError(request, getText("trxTopup.invalid.account.to", trxRecord.getAccountNumber(), request.getLocale()));
			return "trx/topup/add";
		} else {
			trxRecord.setCoreUser(accountTo.getCoreUser());
			trxRecord.setAccountNumber("PETTYCASH");
			trxRecord.setTransactionDate(new Date());
			trxRecord.setTransactionId(DateUtil.toBundleDateFormat(new Date()));
			trxRecord.setTransactionType("TOPUP");
			
			virtualAccountManager.debit(trxRecord.getAccountNumber(), trxRecord.getAmount());
			virtualAccountManager.credit(accountTo.getId(), trxRecord.getAmount());
			
			trxRecord = trxRecordManager.save(trxRecord);
			
			TrxTopup trxTopup = new TrxTopup();
			trxTopup.setAmount(trxRecord.getAmount());
			trxTopup.setCoreUser(trxRecord.getCoreUser());
			trxTopup.setReference(trxRecord.getReference());
			trxTopup.setTopupDate(trxRecord.getTransactionDate());
			trxTopup.setTopupId(trxRecord.getTransactionId());
			trxTopup = trxTopupManager.save(trxTopup);
			
			saveMessage(request, getText("trxTopup.create.success", String.valueOf(trxTopup.getId()), request.getLocale()));
			return "redirect:/trx/topup";
		}
	}
	
//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {
		TrxTopup trxTopup = trxTopupManager.getLiveById(id);
		model.addAttribute("trxTopup", trxTopup);
		return "trx/topup/detail";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(@PathVariable("id") Long id, HttpServletRequest request, Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}
		TrxTopup trxTopup = trxTopupManager.getLiveById(id);
		model.addAttribute("trxTopup", trxTopup);
		return "redirect:/trx/topup";
	}
    
}
