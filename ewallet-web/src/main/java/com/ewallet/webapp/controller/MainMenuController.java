package com.ewallet.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewallet.persistence.model.CoreUser;
import com.ewallet.security.bean.UserUtil;
import com.ewallet.service.CoreMenuManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.CoreUserManager;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 *         edited by : Arni Sihombing
 * @version : 1.0, 7/12/12, 4:54 PM
 */

@Controller
@RequestMapping("/mainMenu")
public class MainMenuController extends BaseFormController {

    @Autowired
    private CoreMenuManager coreMenuManager;
    /*@Autowired
    private CoreCalendarManager coreCalendarManager;

    @Autowired
    private CoreCurrencyManager coreCurrencyManager;*/

    @Autowired
    private CoreUserManager coreUserManager;

    @Autowired
    private CoreSystemManager coreSystemManager;
    
    @RequestMapping(method = RequestMethod.GET, value = "")
    public String dashboard(Model model, HttpServletRequest request, HttpServletResponse response) {
        CoreUser coreUser = coreUserManager.getUserByUsername(UserUtil.getCurrentUsername());
        
        CoreUser user = coreUserManager.getUserByUsername(coreUser.getUsername());
         
        //DEPRECATED
//        CoreSystem coreSystem = coreSystemManager.getCurrentSystem();
        if (coreUser != null) {
            model.addAttribute("coreUser", user);
            return "mainMenu";
        } else {
            return "redirect:/login";
        }
    }

}
