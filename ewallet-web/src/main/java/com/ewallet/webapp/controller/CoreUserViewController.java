package com.ewallet.webapp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ewallet.persistence.model.CorePermission;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.LabelValueStatus;
import com.ewallet.service.CorePermissionManager;
import com.ewallet.service.CoreRoleManager;
import com.ewallet.service.CoreUserManager;

@Controller
@RequestMapping(value = "/core/coreuser")
public class CoreUserViewController extends BaseFormController {
	private CoreUserManager coreUserManager;
	private CorePermissionManager corePermissionManager;
	private CoreRoleManager coreRoleManager;

	public CoreUserViewController() {
		setCancelView("redirect:/core/coreuser");
		setSuccessView("redirect:/core/coreuser");
	}

	@Autowired
	public void setCoreUserManager(CoreUserManager coreUserManager) {
		this.coreUserManager = coreUserManager;
	}

	@Autowired
	public void setCorePermissionManager(
			CorePermissionManager corePermissionManager) {
		this.corePermissionManager = corePermissionManager;
	}

	@Autowired
	public void setCoreRoleManager(CoreRoleManager coreRoleManager) {
		this.coreRoleManager = coreRoleManager;
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "")
	public String allDisplay(Model model) throws Exception {
		model.addAttribute("coreUsers", coreUserManager.getAllOrderByUsername());
		return "core/coreuser/list";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/managerole/{id}", method = RequestMethod.GET)
	public String manageRoleDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {

		CoreUser user = coreUserManager.getWithListAll(id);
		List<CoreRole> cps = coreRoleManager.getAll();
		Set<CoreRole> ucps = user.getCoreRoles();
		List<LabelValueStatus> lvs = new ArrayList<LabelValueStatus>();
		for (CoreRole cp : cps) {
			Boolean status = false;
			if (ucps.contains(cp)) {
				status = true;
			}
			lvs.add(new LabelValueStatus(cp.getDescription(), cp.getId()
					.toString(), status));
		}
		model.addAttribute("coreUser", user);
		model.addAttribute("listCoreRoles", lvs);

		return "core/coreuser/managerole";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/managerole/{id}", method = RequestMethod.POST)
	public String manageRoleProcess(
			@PathVariable("id") Long id,
			@RequestParam(value = "coreRoles", required = false) Integer[] roleIDs,
			HttpServletRequest request, HttpServletResponse response,
			Model model) throws Exception {
		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (roleIDs == null) {
			saveError(request,
					getText("coreUser.errorNoRole", request.getLocale()));
			return manageRoleDisplay(id, model);
		}

		CoreUser coreUser = coreUserManager.getWithListAll(id);

		Set<CoreRole> coreRoles = new HashSet<CoreRole>();

		for (Integer roleID : roleIDs) {
			coreRoles.add(coreRoleManager.getWithListAll(roleID));
		}
		coreUser.setCoreRoles(coreRoles);
		coreUserManager.save(coreUser);
		saveMessage(
				request,
				getText("coreUser.manageRole", coreUser.getUsername(),
						request.getLocale()));
		
//		return "redirect:/core/coreuser/detail/" + id;
		return "redirect:/core/coreuser";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/resetuser/{id}", method = RequestMethod.GET)
	public String viewUser(@PathVariable("id") Long id, Model model) {
		model.addAttribute("coreUser", coreUserManager.getWithListAll(id));
		return "core/coreuser/resetuser";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/resetuser/{id}", method = RequestMethod.POST)
	public String resetUser(CoreUser coreUser, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		CoreUser cu = coreUserManager.getWithListAll(coreUser.getId());
		cu.setAccountLocked(false);
		cu.setLoginAttempt(0);
//		cu.setIpAddress("");
		cu.setSession("");
		cu.setAccountExpired(false);
		cu.setCredentialsExpired(false);
		cu.setAccountEnabled(true);

		coreUserManager.save(cu);
		saveMessage(
				request,
				getText("coreUser.resetUser", coreUser.getUsername(),
						request.getLocale()));

		return "redirect:/core/coreuser";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/resetpassword/{id}", method = RequestMethod.GET)
	public String resetPassword(@PathVariable("id") Long id, Model model) {
		model.addAttribute("coreUser", coreUserManager.getWithListAll(id));
		return "core/coreuser/resetpassword";
	}

	// @PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/resetpassword/{id}", method = RequestMethod.POST)
	public String resetPassword(CoreUser coreUser, BindingResult errors,
			HttpServletRequest request, HttpServletResponse response,
			Model model) {

		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		CoreUser user = coreUserManager.getWithListAll(coreUser.getId());
		user.setPassword(coreUser.getPassword());
		user.setConfirmPassword(coreUser.getConfirmPassword());
//		user.setPasswordHint(coreUser.getPasswordHint());
		coreUserManager.save(user);
		saveMessage(
				request,
				getText("coreUser.resetPassword",
						String.valueOf(coreUser.getUsername()),
						request.getLocale()));

		return "redirect:/core/coreuser";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:READ:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public String detailDisplay(@PathVariable("id") Long id, Model model)
			throws Exception {
		model.addAttribute("coreUser", coreUserManager.getWithListAll(id));
		CoreUser user = coreUserManager.getWithListAll(id);
		List<LabelValueStatus> lvs = new ArrayList<LabelValueStatus>();

		List<CoreRole> cps = coreRoleManager.getAll();
		Set<CoreRole> ucpsr = user.getCoreRoles();
		for (CoreRole cpr : cps) {
			Boolean status = false;
			if (ucpsr.contains(cpr)) {
				status = true;
			}
			lvs.add(new LabelValueStatus(cpr.getDescription(), cpr.getId()
					.toString(), status));
		}

		model.addAttribute("listCoreRoles", lvs);
		model.addAttribute("coreUser", user);
		return "core/coreuser/detail";
	}

//	@PreAuthorize("hasAnyRole('CORE:USER:INPUT:*')")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.POST)
	public String managePermissionProcess(
			@PathVariable("id") Long id,
			@RequestParam(value = "corePermissions", required = false) Integer[] permissionIDs,
			@RequestParam(value = "coreBranches", required = false) String[] branchIDs,
			@RequestParam(value = "coreRoles", required = false) Integer[] roleIDs,
			HttpServletRequest request, Model model) throws Exception {

		if (request.getParameter("cancel") != null) {
			if (!StringUtils.equals(request.getParameter("from"), "list")) {
				return getCancelView();
			} else {
				return getSuccessView();
			}
		}

		if (permissionIDs == null) {
			saveError(request,
					getText("coreUser.errorNoPermission", request.getLocale()));
			return detailDisplay(id, model);
		}

		CoreUser coreUser = coreUserManager.getWithListAll(id);

		Set<CorePermission> corePermissions = new HashSet<CorePermission>();

		for (Integer permissionID : permissionIDs) {
			corePermissions.add(corePermissionManager.get(permissionID));
		}

		Set<CoreRole> coreRoles = new HashSet<CoreRole>();

		for (Integer roleID : roleIDs) {
			coreRoles.add(coreRoleManager.getWithListAll(roleID));
		}

//		coreUser.setCorePermissions(corePermissions);
		coreUser.setCoreRoles(coreRoles);
		coreUserManager.save(coreUser);

		return "redirect:/core/coreuser";
	}
}
