package com.ewallet.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ewallet.enumeration.CoreMenuEnum;
import com.ewallet.persistence.model.CoreMenu;
import com.ewallet.service.CoreMenuManager;


@Controller
@RequestMapping("core/menu")
public class CoreMenuController extends BaseFormController {
    private CoreMenuManager mgrMenu = null;
    private CoreMenuEnum[] menuType = CoreMenuEnum.values();

    public CoreMenuController() {
        setCancelView("redirect:/core/menu");
        setSuccessView("redirect:/core/menu");
    }

    @Autowired
    public void setCoreMenuManager(CoreMenuManager coreMenuManager) {
        this.mgrMenu = coreMenuManager;
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:READ:*')")
    @RequestMapping(value = "")
    public String handleRequest(Model model) throws Exception {
        model.addAttribute("coreMenus", mgrMenu.getAll());
        return "core/menu/list";
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:INPUT:*')")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) throws Exception {
        model.addAttribute("coreMenu", new CoreMenu());
        model.addAttribute("menus", menuType);
        model.addAttribute("coreMenus", mgrMenu.getAll());
        return "core/menu/add";
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:INPUT:*')")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    protected String save(CoreMenu coreMenu, BindingResult errors,
                          HttpServletRequest request, HttpServletResponse response,
                          Model model) throws Exception {

        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }
        if (validator != null) {
            validator.validate(coreMenu, errors);

            if (errors.hasErrors() && request.getParameter("delete") == null) {


                menuType = CoreMenuEnum.values();
                model.addAttribute("menus", menuType);
                model.addAttribute("coreMenus", mgrMenu.getAll());
                return "core/menu/add";

            }
        }
        
        if(!mgrMenu.getByOrderMenu(coreMenu.getParentId(), coreMenu.getOrderMenu()).isEmpty()){
        	saveError(request,getText("errors.exists", coreMenu.getOrderMenu().toString(),request.getLocale()));
        	model.addAttribute("menus", menuType);
            model.addAttribute("coreMenus", mgrMenu.getAll());
            return "core/menu/add";
        }
        
        if(coreMenu.getType().equals(CoreMenuEnum.PARENT.getMenuValue())){
        	coreMenu.setParentId(0);
        	coreMenu.setIcon("icon-tasks icon-white");
        }
        
        try {
            if (coreMenu.getId() == null) {
                coreMenu.setId(Integer.MIN_VALUE);
            }
            mgrMenu.get(coreMenu.getId());
            saveError(
                    request,
                    getText("errors.exists", coreMenu.getId().toString(),
                            request.getLocale()));
            model.addAttribute("menus", menuType);
            model.addAttribute("coreMenus", mgrMenu.getAll());
            return "core/menu/add";

        } catch (ObjectRetrievalFailureException ex) {
            mgrMenu.save(coreMenu);
            saveMessage(
                    request,
                    getText("coreMenu.saved", coreMenu.getDescription(),
                            request.getLocale()));
        }

        return "redirect:/core/menu";
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:DELETE:*')")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Integer id, Model model) throws Exception {

        model.addAttribute("coreMenu", mgrMenu.get(id));
        model.addAttribute("menus", menuType);
        model.addAttribute("coreMenus", mgrMenu.getAll());
        return "core/menu/delete";
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:DELETE:*')")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    protected String confirmDelete(CoreMenu coreMenu, BindingResult errors, HttpServletRequest request,HttpServletResponse response) throws Exception {
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        mgrMenu.remove(coreMenu.getId());
        saveMessage(request, getText("coreMenu.deleted", coreMenu.getDescription(), request.getLocale()));
        return "redirect:/core/menu";
    }


//    @PreAuthorize("hasAnyRole('CORE:MENU:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Integer id, Model model) throws Exception {

        model.addAttribute("coreMenu", mgrMenu.get(id));
        model.addAttribute("menus", menuType);
        model.addAttribute("coreMenus", mgrMenu.getAll());
        return "core/menu/edit";
    }

//    @PreAuthorize("hasAnyRole('CORE:MENU:INPUT:*')")
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    protected String update(CoreMenu coreMenu, BindingResult errors, HttpServletRequest request,HttpServletResponse response,Model model) throws Exception {
        if (request.getParameter("cancel") != null) {
            if (!StringUtils.equals(request.getParameter("from"), "list")) {
                return getCancelView();
            } else {
                return getSuccessView();
            }
        }

        if (validator != null) {
            validator.validate(coreMenu, errors);

            if (errors.hasErrors() && request.getParameter("delete") == null) {
                return "core/menu/edit";
            }
        }
        CoreMenu ordermenuDb = mgrMenu.get(coreMenu.getId());
		Integer idOrderDb = ordermenuDb.getOrderMenu();
		Integer idOrder = coreMenu.getOrderMenu();
        if(!idOrderDb.equals(idOrder)){
        	if(!mgrMenu.getByOrderMenu(coreMenu.getParentId(), coreMenu.getOrderMenu()).isEmpty()){
        		saveError(request,getText("errors.exists", coreMenu.getOrderMenu().toString(),request.getLocale()));
        		model.addAttribute("menus", menuType);
        		model.addAttribute("coreMenus", mgrMenu.getAll());
            return "core/menu/edit";
        	}
        }
        if(coreMenu.getType().equals(CoreMenuEnum.PARENT.getMenuValue())){
        	coreMenu.setParentId(0);
        	coreMenu.setIcon("icon-tasks icon-white");
        }
        
        mgrMenu.save(coreMenu);
        saveMessage(request, getText("coreMenu.saved", coreMenu.getDescription(), request.getLocale()));
        return "redirect:/core/menu";
    }


}
