/**
 * 
 */
package com.ewallet.util;

/**
 * @author agung.kurniawan
 * Date : 4 Okt 2014
 */
public class StringUtil {

	public final static String[] split(String text, String delimiter) throws Exception{
		if (text == null || delimiter == null) {
			throw new Exception("Text and delimiter both can't be null");
		}
		
		String[] splittedText = text.split(delimiter);
		splittedText = clean(splittedText);
		
		return splittedText;
	}
	
	private static String[] clean(final String[] v) {
		  int r, w, n = r = w = v.length;
		  while (r > 0) {
		    final String s = v[--r];
		    if (!s.trim().equals("")) {
		      v[--w] = s;
		    }
		  }
		  final String[] c = new String[n -= w];
		  System.arraycopy(v, w, c, 0, n);
		  return c;
		}
	
}
