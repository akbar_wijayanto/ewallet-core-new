package com.ewallet.processor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;

import com.ewallet.dto.ExcelFileUploadInputDto;

public class ExcelFileUploadProcessor {

	private ExcelFileUploadInputDto input;

	public List<Object[]> process() {
		Iterator<Row> rowIterator = input.getExcelUtil().getRowIterator();
		if(rowIterator.hasNext()) rowIterator.next();
		List<Object[]> entities = new ArrayList<Object[]>();
		while(rowIterator.hasNext()) {
        	Row row = rowIterator.next();
        	Object[] excelValues = input.getExcelUtil().getRow(row);
        	Object[] values = new Object[input.getColumnSize()];
        	for(int i=0; i < input.getColumnSize(); i++) {
        		if(i>=excelValues.length) 
        			values[i] = null;
        		else
        			values[i] = excelValues[i];
        	}
        	
        	boolean isRowEmpty = true;
        	for (Object object : values) {
				if(object != null) isRowEmpty = false;
			}
        	if(isRowEmpty) continue;
        	
        	entities.add(values);
        }
		
		return entities;
	}

	public ExcelFileUploadInputDto getInput() {
		return input;
	}

	public void setInput(ExcelFileUploadInputDto input) {
		this.input = input;
	}
}
