package com.ewallet.dto;

import com.ewallet.util.ExcelUtil;

public class ExcelFileUploadInputDto {

	private int columnSize;
	private ExcelUtil excelUtil;

	public ExcelFileUploadInputDto(int columnSize, ExcelUtil excelUtil) {
		this.columnSize = columnSize;
		this.excelUtil = excelUtil;
	}

	public int getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}

	public ExcelUtil getExcelUtil() {
		return excelUtil;
	}

	public void setExcelUtil(ExcelUtil excelUtil) {
		this.excelUtil = excelUtil;
	}
}
