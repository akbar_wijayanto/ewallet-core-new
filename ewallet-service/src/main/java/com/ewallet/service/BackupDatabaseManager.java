package com.ewallet.service;

public interface BackupDatabaseManager {
	void backupDatabase();
	boolean isExist(String path);
}
