package com.ewallet.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:24:46 PM
 */
public interface TrxRecordManager extends GenericTransactionManager<TrxRecord, Long> {
 
	List<TrxRecord> getBySellerId(String sellerId);
	List<TrxRecord> getByUsername(String username);
	
	/// report
	List<TrxRecord> getSellerSettlementReport(String sellerId, String name, Date startDate, Date endDate);
	public void recordTransaction(TrxOrder order, BigDecimal totalAmount);
	
}
