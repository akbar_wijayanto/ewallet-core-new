package com.ewallet.service;

import java.util.List;

import com.ewallet.persistence.model.CoreSeller;

public interface CoreSellerManager extends GenericManager<CoreSeller, Long> {
   
	CoreSeller getByQRCode(String qrcode);
	
	List<CoreSeller> getAllAuthSeller();
	
	List<CoreSeller> getInactiveSellerList();
	
}
