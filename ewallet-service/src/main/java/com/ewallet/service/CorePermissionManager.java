package com.ewallet.service;

import java.util.List;

import com.ewallet.persistence.model.CorePermission;

public interface CorePermissionManager extends GenericManager<CorePermission, Integer> {
	List<String> getAplication();
	List<CorePermission> getByAplication(String applicationName);

}
