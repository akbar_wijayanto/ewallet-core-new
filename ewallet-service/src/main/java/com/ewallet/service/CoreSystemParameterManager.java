package com.ewallet.service;

import com.ewallet.persistence.model.CoreSystemParameter;

public interface CoreSystemParameterManager extends GenericManager<CoreSystemParameter, String>{

}
