package com.ewallet.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.TrxOrder;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:14:11 PM
 */
public interface TrxOrderManager extends GenericTransactionManager<TrxOrder, String> {
   
	List<TrxOrder> getOrdersByUser(String username);
	List<TrxOrder> getOrdersBySeller(String sellerId);
	TrxOrder getOrderByOrderId(String orderId);
	int checkValidOrder(String sellerId, String orderId); //custom by silvi.zain
	List<TrxOrder> getLast7DaysOrdersBySeller(String sellerId);
	List<TrxOrder> getLast7DaysOrders();
	List<TrxOrder> getLast7DaysOrdersBySellerUnpaid(String sellerId);
	List<TrxOrder> getLast7DaysOrdersByUser(String username);
	
	List<TrxOrder> getPeriodicRefund(String startDate,String endDate); //untuk laporan refund dengan periodic
	List<TrxOrder> getPeriodicOrder(String startDate,String endDate); //untuk laporan order dengan periodic
	List<TrxOrder> getPeriodOrdersBySeller(String sellerId, String startDate, String endDate); //untuk laporan penjualan dengan periodic
	List<TrxOrder> getPeriodOrders(String startDate, String endDate); //untuk laporan order dengan periodic
	List<TrxOrder> getPeriodOrdersByUser(String username, String startDate, String endDate); // untuk laporan pembeli dengan periodic
	BigDecimal getGrandTotalPeriodicFromOrderReport(String startDate, String endDate); //get grandtotal dari laporan order
	BigDecimal getGrandTotalPeriodicFromRefundReport(String startDate, String endDate); //get grandtotal dari laporan refund
	
	List<TrxOrder> getPeriodicRefundBySellerid(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersBySellerId(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersByUserBuyerId(Long buyerId, String username, String startDate, String endDate);
	
	List<TrxOrder> getLast7DaysOrdersForSettlement();

	List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, Date startDate, Date endDate);
	
	List<TrxOrder> getListOrdersForSettlement(String startDat, String endDat);
}
