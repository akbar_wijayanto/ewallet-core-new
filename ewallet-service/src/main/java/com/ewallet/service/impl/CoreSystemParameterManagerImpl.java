package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.CoreSystemParameterDao;
import com.ewallet.persistence.model.CoreSystemParameter;
import com.ewallet.service.CoreSystemParameterManager;

@Service("coreSystemParameterManager")
public class CoreSystemParameterManagerImpl extends GenericManagerImpl<CoreSystemParameter, String> implements CoreSystemParameterManager{

	private CoreSystemParameterDao coreSystemParameterDao;

	@Autowired
	public void setCoreSystemParameterDao(
			CoreSystemParameterDao coreSystemParameterDao) {
		this.coreSystemParameterDao = coreSystemParameterDao;
		this.dao = coreSystemParameterDao;
	}
	
}
