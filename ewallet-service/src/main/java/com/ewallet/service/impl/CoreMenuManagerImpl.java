package com.ewallet.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.dto.MenuDto;
import com.ewallet.persistence.dao.CoreMenuDao;
import com.ewallet.persistence.model.CoreMenu;
import com.ewallet.service.CoreMenuManager;

@Service("coreMenuManager")
public class CoreMenuManagerImpl extends GenericManagerImpl<CoreMenu, Integer> implements CoreMenuManager{

	private CoreMenuDao coreMenuDao = null;

	@Autowired
	public void setCoreMenuDao(CoreMenuDao coreMenuDao) {
		this.coreMenuDao = coreMenuDao;
		this.dao = coreMenuDao;
	}

    /**
     * Modified by : andi.annas
     * Fix error if parent menu removed without removing child menu access
     * @param username
     * @return
     */
	@Override
	public List<MenuDto> getUserMenus(String username) {
		List<CoreMenu> menus = coreMenuDao.getByUsername(username);
		List<MenuDto> dtos = new ArrayList<MenuDto>();
		SortedMap<Integer, MenuDto> menuMap = new TreeMap<Integer, MenuDto>();
		for (CoreMenu menu : menus) {
			if(menu.getParentId()==0)
			{
				MenuDto dto = new MenuDto();
				dto.setParentMenu(menu);
				menuMap.put(menu.getId(), dto);
			}
			else if(menu.getParentId()!=null /*&& menuMap.get(menu.getParentId())!=null*/)
			{				
				CoreMenu coreMenu = coreMenuDao.getByParentId(menu.getParentId());
				if(coreMenu.getType().equals("C")){
					menuMap.get(coreMenu.getParentId()).getChildSubMenus().add(menu);										
				} else {
					menuMap.get(menu.getParentId()).getChildMenus().add(menu);
				}
			}
		}
		for (MenuDto dto : menuMap.values()) {
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu) {
		return coreMenuDao.getByOrderMenu(parentId, orderMenu);
	}

	@Override
	public Integer getByPermaLink(String link) {
		return coreMenuDao.getByPermaLink(link);
	}

	@Override
	public List<MenuDto> getUserMenusByActive(Integer roleId) {
		List<CoreMenu> menus = coreMenuDao.getByActiveRole(roleId);
		List<MenuDto> dtos = new ArrayList<MenuDto>();
		SortedMap<Integer, MenuDto> menuMap = new TreeMap<Integer, MenuDto>();
		for (CoreMenu menu : menus) {
			if(menu.getParentId()==0)
			{
				MenuDto dto = new MenuDto();
				dto.setParentMenu(menu);
				menuMap.put(menu.getId(), dto);
			}
			else if(menu.getParentId()!=null /*&& menuMap.get(menu.getParentId())!=null*/)
			{				
				CoreMenu coreMenu = coreMenuDao.getByParentId(menu.getParentId());
				if(coreMenu.getType().equals("C")){
					menuMap.get(coreMenu.getParentId()).getChildSubMenus().add(menu);										
				} else {
					menuMap.get(menu.getParentId()).getChildMenus().add(menu);
				}
			}
		}
		for (MenuDto dto : menuMap.values()) {
			dtos.add(dto);
		}
		return dtos;
	}


}
