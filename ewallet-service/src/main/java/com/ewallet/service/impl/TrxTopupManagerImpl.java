package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.TrxTopupDao;
import com.ewallet.persistence.model.TrxTopup;
import com.ewallet.service.TrxTopupManager;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 11:38:10 AM
 */
@Service("trxTopupManager")
public class TrxTopupManagerImpl extends GenericTransactionManagerImpl<TrxTopup, Long>
		implements TrxTopupManager {
	
	private TrxTopupDao trxTopupDao;
	
	@Autowired
	public void setTrxTopupDao(TrxTopupDao trxTopupDao) {
		this.trxTopupDao = trxTopupDao;
		this.dao = trxTopupDao;
		this.transactionDao = trxTopupDao;
	}
	
}