package com.ewallet.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.enumeration.OrderStatusEnum;
import com.ewallet.persistence.dao.TrxOrderDao;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.service.TrxOrderManager;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:04:57 PM
 */
@Service("trxOrderManager")
public class TrxOrderManagerImpl extends GenericTransactionManagerImpl<TrxOrder, String>
		implements TrxOrderManager {

	private TrxOrderDao trxOrderDao;
	
	@Autowired
	public void setTrxOrderDao(TrxOrderDao trxOrderDao) {
		this.trxOrderDao = trxOrderDao;
		this.dao = trxOrderDao;
		this.transactionDao = trxOrderDao;
	}

	@Override
	public List<TrxOrder> getOrdersByUser(String username) {
		return trxOrderDao.getOrdersByUser(username);
	}

	@Override
	public List<TrxOrder> getOrdersBySeller(String sellerId) {
		return trxOrderDao.getOrdersBySeller(sellerId);
	}

	@Override
	public TrxOrder getOrderByOrderId(String orderId) {
		return trxOrderDao.getOrderByOrderId(orderId);
	}

	@Override
	 public int checkValidOrder(String sellerId, String orderId) {
	  return trxOrderDao.checkValidOrder(sellerId, orderId);
	 }

	@Override
	public List<TrxOrder> getLast7DaysOrdersBySeller(String sellerId) {
		return trxOrderDao.getLast7DaysOrdersBySeller(sellerId);
	}

	@Override
	public List<TrxOrder> getLast7DaysOrders() {
		return trxOrderDao.getLast7DaysOrders();
	}

	@Override
	public List<TrxOrder> getLast7DaysOrdersBySellerUnpaid(String sellerId) {
		return trxOrderDao.getLast7DaysOrdersBySellerUnpaid(sellerId);
	}

	@Override
	public List<TrxOrder> getLast7DaysOrdersByUser(String username) {
		return trxOrderDao.getLast7DaysOrdersByUser(username);
	}

	@Override
	public List<TrxOrder> getPeriodicRefund(String startDate, String endDate) {
		return trxOrderDao.getPeriodicRefund(startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodicOrder(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return trxOrderDao.getPeriodicOrder(startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodOrdersBySeller(String sellerId,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return trxOrderDao.getPeriodOrdersBySeller(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodOrders(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return trxOrderDao.getPeriodOrders(startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodOrdersByUser(String username, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return trxOrderDao.getPeriodOrdersByUser(username, startDate, endDate);
	}

	@Override
	public BigDecimal getGrandTotalPeriodicFromOrderReport(String startDate,
			String endDate) {
		List<TrxOrder> listTrx = trxOrderDao.getPeriodicOrder(startDate, endDate);
		BigDecimal gTot = new BigDecimal("0");
		for (TrxOrder trxOrder : listTrx) {
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				gTot = gTot.add(total);
			}
		}
		return gTot;
	}

	@Override
	public BigDecimal getGrandTotalPeriodicFromRefundReport(String startDate,
			String endDate) {
		List<TrxOrder> listTrx = trxOrderDao.getPeriodicRefund(startDate, endDate);
		BigDecimal gTot = new BigDecimal("0");
		for (TrxOrder trxOrder : listTrx) {
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				gTot = gTot.add(total);
			}
		}
		return gTot;
	}

	@Override
	public List<TrxOrder> getPeriodicRefundBySellerid(String sellerId, String startDate, String endDate) {
		return trxOrderDao.getPeriodicRefundBySellerid(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, String startDate, String endDate) {
		return trxOrderDao.getPeriodicOrderBySellerid(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodOrdersBySellerId(String sellerId, String startDate, String endDate) {
		return trxOrderDao.getPeriodOrdersBySellerId(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getLast7DaysOrdersForSettlement() {
		// TODO Auto-generated method stub
		return trxOrderDao.getLast7DaysOrdersForSettlement();
	}

	@Override
	public List<TrxOrder> getPeriodOrdersByUserBuyerId(Long buyerId, String username, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return trxOrderDao.getPeriodOrdersByUserBuyerId(buyerId, username, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getPeriodicOrderBySellerid(String sellerId,
			Date startDate, Date endDate) {
		return trxOrderDao.getPeriodicOrderBySellerid(sellerId, startDate, endDate);
	}

	@Override
	public List<TrxOrder> getListOrdersForSettlement(String startDat,String endDat) {
		return trxOrderDao.getListOrdersForSettlement(startDat, endDat);
	}

}