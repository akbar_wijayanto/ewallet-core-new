package com.ewallet.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.persistence.dao.CoreSellerDao;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.service.CoreSellerManager;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:04:57 PM
 */
@Service("coreSellerManager")
public class CoreSellerManagerImpl extends GenericManagerImpl<CoreSeller, Long>
		implements CoreSellerManager {
	
	private CoreSellerDao coreSellerDao;
	
	@Autowired
	public void setCoreSellerDao(CoreSellerDao coreSellerDao) {
		this.coreSellerDao = coreSellerDao;
		this.dao = coreSellerDao;
	}

	@Override
	public CoreSeller getByQRCode(String qrcode) {
		return coreSellerDao.getByQRCode(qrcode);
	}

	@Override
	public List<CoreSeller> getAllAuthSeller() {
		return coreSellerDao.getAllAuthSeller();
	}

	@Override
	public List<CoreSeller> getInactiveSellerList() {
		return coreSellerDao.getInactiveSellerList();
	}

}