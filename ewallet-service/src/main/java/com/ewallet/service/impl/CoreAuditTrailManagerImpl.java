package com.ewallet.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.AuditTrailDao;
import com.ewallet.persistence.model.AuditTrail;
import com.ewallet.service.CoreAuditTrailManager;


@Service("coreAuditTrailManager")
public class CoreAuditTrailManagerImpl implements CoreAuditTrailManager{

	private AuditTrailDao auditTrailDao;

	@Autowired
	public void setAuditTrailDao(AuditTrailDao auditTrailDao) {
		this.auditTrailDao = auditTrailDao;
	}

	@Override
	public List<AuditTrail> getAll() {
		
		return auditTrailDao.getAll();
	}

	@Override
	public AuditTrail get(Long id) {
		return auditTrailDao.get(id);
	}

//	@Override
//	public List<AuditTrail> getPagingAudit(Long firstResult, Long maxResults,
//			String objectName, String fieldName,Date dateFrom, Date dateTo, String actor, String action) {
//		
//		return auditTrailDao.getPagingResultAudit(firstResult, maxResults, objectName, fieldName,dateFrom, dateTo, actor, action);
//	}
//
	@Override
	public Long getCountAudit(String objectName, String fieldName,Date dateFrom, Date dateTo,
			String actor, String action) {
		return auditTrailDao.getAuditCount(objectName, fieldName,dateFrom, dateTo, actor, action);
	}

	@Override
	public List<AuditTrail> getSearchAuditTrail(String objectName,String fieldName, Date dateFrom, Date dateTo, String actor,
			String action) {
		
		return auditTrailDao.getDataAudittrail(objectName, fieldName, dateFrom, dateTo, actor, action);
	}

	@Override
	public List<AuditTrail> getSearchAuditTrail(Long start, Long end,
			String objectName, String fieldName, Date dateFrom, Date dateTo,
			String actor, String action) {
		return auditTrailDao.getDataAudittrail(start, end, objectName, fieldName, dateFrom, dateTo, actor, action);
	}
	


}
