package com.ewallet.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.VirtualAccountDao;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.VirtualAccount;
import com.ewallet.service.VirtualAccountManager;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:25:35 PM
 */
@Service("virtualAccountManager")
public class VirtualAccountManagerImpl extends GenericTransactionManagerImpl<VirtualAccount, String>
		implements VirtualAccountManager {
	
	private VirtualAccountDao virtualAccountDao;
	
	@Autowired
	public void setVirtualAccountDao(VirtualAccountDao virtualAccountDao) {
		this.virtualAccountDao = virtualAccountDao;
		this.dao = virtualAccountDao;
		this.transactionDao = virtualAccountDao;
	}

	@Override
	public void debit(String accountNumber, BigDecimal amount) {
		VirtualAccount account = virtualAccountDao.get(accountNumber);
		if(account!=null){
			account.setBalance(account.getBalance().subtract(amount));
			virtualAccountDao.save(account); 
		}
	}

	@Override
	public void credit(String accountNumber, BigDecimal amount) {
		VirtualAccount account = virtualAccountDao.get(accountNumber);
		if(account!=null){
			account.setBalance(account.getBalance().add(amount));
			virtualAccountDao.save(account); 
		}
	}

	@Override
	public VirtualAccount getByUser(CoreUser coreUser) {
		return virtualAccountDao.getByUser(coreUser);
	}
	
}