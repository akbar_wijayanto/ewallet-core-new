package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.CoreItemTypeDao;
import com.ewallet.persistence.model.CoreItemType;
import com.ewallet.service.CoreItemTypeManager;


/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:51:48 PM
 */
@Service("coreItemTypeManager")
public class CoreItemTypeManagerImpl extends
		GenericManagerImpl<CoreItemType, Long> implements CoreItemTypeManager {

	private CoreItemTypeDao coreItemTypeDao;

	@Autowired
	public void setCoreItemTypeDao(CoreItemTypeDao coreItemTypeDao) {
		this.coreItemTypeDao = coreItemTypeDao;
		this.dao = coreItemTypeDao;
	}
}
