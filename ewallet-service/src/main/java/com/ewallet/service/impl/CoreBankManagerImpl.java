package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.CoreBankDao;
import com.ewallet.persistence.model.CoreBank;
import com.ewallet.service.CoreBankManager;

/**
 * @author akbar.wijayanto
 * Date Nov 24, 2015 1:46:40 PM
 */
@Service("coreBankManager")
public class CoreBankManagerImpl extends
		GenericManagerImpl<CoreBank, String> implements CoreBankManager {

	private CoreBankDao coreBankDao;
	
	@Autowired
	public void setCoreBankDao(CoreBankDao coreBankDao) {
		this.coreBankDao = coreBankDao;
		this.dao = coreBankDao;
	}
	
}
