package com.ewallet.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.TransactionDao;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.TransactionManager;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:04:57 PM
 */
@Service("transactionManager")
public class TransactionManagerImpl extends GenericTransactionManagerImpl<TrxRecord, Long>
		implements TransactionManager {
	
	private TransactionDao transactionDao;
	
	@Autowired
	public void setTransactionDao(TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
		this.dao = transactionDao;
		this.transactionDao = transactionDao;
	}
	
}