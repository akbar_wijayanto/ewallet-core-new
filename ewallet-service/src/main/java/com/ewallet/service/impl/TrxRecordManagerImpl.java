package com.ewallet.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.persistence.dao.TrxRecordDao;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxRecord;
import com.ewallet.service.TrxRecordManager;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:25:35 PM
 */
@Service("trxRecordManager")
public class TrxRecordManagerImpl extends GenericTransactionManagerImpl<TrxRecord, Long>
		implements TrxRecordManager {
	
	private TrxRecordDao trxRecordDao;
	
	@Autowired
	public void setTrxRecordDao(TrxRecordDao trxRecordDao) {
		this.trxRecordDao = trxRecordDao;
		this.dao = trxRecordDao;
		this.transactionDao = trxRecordDao;
	}

	@Override
	public List<TrxRecord> getBySellerId(String sellerId) {
		return trxRecordDao.getBySellerId(sellerId);
	}

	@Override
	public List<TrxRecord> getByUsername(String username) {
		return trxRecordDao.getByUsername(username);
	}

	@Override
	public List<TrxRecord> getSellerSettlementReport(String sellerId,
			String name, Date startDate, Date endDate) {
		return trxRecordDao.getSellerSettlementReport(sellerId, name, startDate, endDate);
	}

	@Override
	public void recordTransaction(TrxOrder order, BigDecimal totalAmount) {
		TrxRecord record = new TrxRecord();
		record.setAccountNumber(order.getCoreUser().getPhoneNumber());
		record.setAmount(totalAmount);
		record.setCoreSeller(order.getCoreSeller());
		record.setCoreUser(order.getCoreUser());
		record.setPairedAccount(order.getCoreSeller().getAccountNumber());
		record.setTransactionDate(new Date());
		record.setTransactionId(order.getOrderId());
		record.setTransactionType("PAYMENT");
		record = trxRecordDao.save(record);
		
	}
	
}