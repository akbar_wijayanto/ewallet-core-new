package com.ewallet.service;

import java.util.Date;
import java.util.List;

import com.ewallet.enumeration.SystemStatusEnum;
import com.ewallet.persistence.model.CoreSystem;
import com.ewallet.persistence.model.CoreUser;

/**author : aloysius.pradipta
 * version 1.0 15/5/2012
 */

public interface CoreSystemManager extends GenericManager<CoreSystem, String> {
	List<CoreSystem> getCoreSystem();
	List<CoreSystem> getRecordByUser(CoreUser coreUser, String status);
	void setStatus(SystemStatusEnum status);
	boolean isEndOfMonth();
	boolean isEndOfYear();
	boolean isBeginningOfMonth();
	boolean isBeginningOfYear(Date date);
	CoreSystem getCurrentSystem();
	void resetCurrentSystem();
	CoreSystem updateCurrentSystem(CoreSystem coreSystem);
}
