package com.ewallet.service;

import java.util.List;

import com.ewallet.persistence.model.Feedback;

public interface FeedbackManager extends GenericManager<Feedback, Long> {
   
	List<Feedback> getAllSellerFeedbacks();
	List<Feedback> getAllManagementFeedbacks();
	List<Feedback> getSellerFeedbacks(String sellerId);
	
}
