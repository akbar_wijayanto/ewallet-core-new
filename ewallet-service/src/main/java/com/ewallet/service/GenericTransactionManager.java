/**
 * 
 */
package com.ewallet.service;

import java.io.Serializable;

import com.ewallet.persistence.model.BaseTransaction;

public interface GenericTransactionManager<T extends BaseTransaction, PK extends Serializable> 
	extends GenericManager<T, PK> {
	
	public T getByTransactionReference(String transactionReference);

}
