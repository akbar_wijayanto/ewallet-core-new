package com.ewallet.service;

import java.math.BigDecimal;

import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.VirtualAccount;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:24:46 PM
 */
public interface VirtualAccountManager extends GenericTransactionManager<VirtualAccount, String> {
	
	public void debit(String accountNumber, BigDecimal amount);
	public void credit(String accountNumber, BigDecimal amount);
	
	public VirtualAccount getByUser(CoreUser coreUser);
	
}
