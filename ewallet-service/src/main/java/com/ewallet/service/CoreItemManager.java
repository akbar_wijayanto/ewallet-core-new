package com.ewallet.service;

import java.util.List;

import com.ewallet.persistence.model.CoreItem;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:51:00 PM
 */
public interface CoreItemManager extends GenericManager<CoreItem, Long> {
	
	CoreItem getByItemId(String itemId);
	
	List<CoreItem> getBySellerId(String sellerId);
	
	List<CoreItem> getAllAuthItem();

}
