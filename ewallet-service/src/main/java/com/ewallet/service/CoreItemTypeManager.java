package com.ewallet.service;

import com.ewallet.persistence.model.CoreItemType;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:51:00 PM
 */
public interface CoreItemTypeManager extends GenericManager<CoreItemType, Long> {
	
}
