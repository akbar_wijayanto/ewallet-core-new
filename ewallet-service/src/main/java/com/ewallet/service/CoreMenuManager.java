package com.ewallet.service;

import java.util.List;

import com.ewallet.dto.MenuDto;
import com.ewallet.persistence.model.CoreMenu;

public interface CoreMenuManager extends GenericManager<CoreMenu, Integer> {

    List<MenuDto> getUserMenus(String username);
    List<MenuDto> getUserMenusByActive(Integer roleId);

    List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu);

    Integer getByPermaLink(String link);
}
