/**
 * 
 */
package com.ewallet.service.converter;



/**
 * @author rangga.putra
 *
 */
public interface GenericConverter<T,F> {
	
	public T convert (F beforeModel) ;
	
}
