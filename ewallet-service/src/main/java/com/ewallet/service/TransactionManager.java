package com.ewallet.service;

import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:14:11 PM
 */
public interface TransactionManager extends GenericTransactionManager<TrxRecord, Long> {
   
}
