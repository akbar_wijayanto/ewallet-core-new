package com.ewallet.service;

import java.util.Date;
import java.util.List;

import com.ewallet.exception.RetailException;
import com.ewallet.persistence.model.TrxSellerPayment;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:14:11 PM
 */
public interface TrxSellerPaymentManager extends GenericTransactionManager<TrxSellerPayment, String> {
	
	List<TrxSellerPayment> getAllSellerPayment();
	TrxSellerPayment getTrxSellerPaymentByPaymentId(String paymentId);
	List<TrxSellerPayment> getSettlementBySellerId(String sellerid);
	
	TrxSellerPayment getBySellerIdAndPaidDate(String sellerId, Date paidDate);
	List<TrxSellerPayment> getBySellerIdAndDate(String sellerId, Date startDate, Date endDate);
	List<TrxSellerPayment> getAllSellerPaymentPaid();
	
	List<TrxSellerPayment> getAllSellerPaymentReport(String startDate, String endDate);
	
	void saveAll(List<TrxSellerPayment> trxSellerPayments) throws RetailException;
	
}
