package com.ewallet.service;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ewallet.exception.UserExistsException;
import com.ewallet.persistence.dao.CoreUserDao;
import com.ewallet.persistence.model.CoreUser;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 *  Modified by <a href="mailto:dan@getrolling.com">Dan Kibler </a> 
 */
public interface CoreUserManager extends GenericManager<CoreUser, Long> {
  /**
   * Convenience method for testing - allows you to mock the DAO and set it on an interface.
   * @param coreUserDao the CoreUserDao implementation to use
   */
  void setCoreUserDao(CoreUserDao coreUserDao);

  /**
   * Retrieves a user by userId.  An exception is thrown if user not found
   *
   * @param userId the identifier for the user
   * @return CoreUser
   */
  CoreUser getUser(String userId);

  /**
   * Finds a user by their username.
   * @param username the user's username used to login
   * @return CoreUser a populated user object
   * @throws org.springframework.security.core.userdetails.UsernameNotFoundException
   *         exception thrown when user not found
   */
  CoreUser getUserByUsername(String username) throws UsernameNotFoundException;

  /**
   * Retrieves a list of all users.
   * @return List
   */
  List<CoreUser> getUsers();

  /**
   * Saves a user's information.
   *
   * @param coreUser the user's information
   * @throws UserExistsException thrown when user already exists
   * @return user the updated user object
   */
  CoreUser saveUser(CoreUser coreUser) throws UserExistsException;

  /**
   * Removes a user from the database by their userId
   *
   * @param userId the user's id
   */
  void removeUser(String userId) throws ConstraintViolationException;

  String getUserBranch(String username);

  CoreUser getWithListAll(Long id);

  List<CoreUser> getBranch(Long id);

  List<CoreUser> getCheckBranchOnUser(String id);

  List<CoreUser> getCheckRoleOnUser(Integer id);

  List<CoreUser> getCheckPermissionOnUser(Integer id);

  List<CoreUser> getUserRoleList(Integer id);

  Long getRecordCount(Integer id);

  List<CoreUser> getSearchResults(Integer id, Long iDisplayStart,Long iDisplayLength);

  CoreUser updateLoginAttempt(CoreUser coreUser);

  List<CoreUser> getAllOrderByUsername();

  List<CoreUser> getAllWithListAll();

  CoreUser getUserSpring(String username);

  CoreUser change(CoreUser coreUser);

  CoreUser save(CoreUser user);
  List<CoreUser> getByClientUser();
  
  CoreUser getByToken(String token);
  void setHistoryToSeller(Long id);
  
}
