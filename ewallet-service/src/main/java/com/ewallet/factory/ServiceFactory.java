package com.ewallet.factory;

import com.ewallet.service.GenericManager;

public interface ServiceFactory {

	@SuppressWarnings("rawtypes")
	GenericManager getManager(String className);
	
}
