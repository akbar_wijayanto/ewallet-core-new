/**
 * 
 */
package com.ewallet.security.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.stereotype.Service;

import com.ewallet.enumeration.ActionTypeEnum;
import com.ewallet.enumeration.ApplicationTypeEnum;
import com.ewallet.enumeration.ModuleTypeEnum;
import com.ewallet.exception.RetailException;
import com.ewallet.persistence.dao.CoreRoleDao;
import com.ewallet.persistence.dao.CoreUserDao;
import com.ewallet.persistence.model.CorePermission;
import com.ewallet.persistence.model.CoreRole;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.security.bean.UserId;
import com.ewallet.service.CoreMenuManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.util.NameUtil;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 9:50:26 PM
 */
@Service
public class UserDetailContextMapperImpl implements UserDetailsContextMapper{

	private CoreUser domainUser;
	@Autowired
	private CoreUserDao coreUserDao;
	@Autowired
	private CoreRoleDao coreRoleDao;
	@Autowired
	private CoreUserManager coreUserManager;
	@Autowired
	private CoreSystemManager coreSystemManager;
	@Autowired
	private CoreMenuManager coreMenuManager;
	
	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx,
			String username, Collection<? extends GrantedAuthority> authorities) {
		
		String name =  (String) ctx.getObjectAttribute("name");
		String mobile =  (String) ctx.getObjectAttribute("mobile");
		String organization1 =  (String) ctx.getObjectAttribute("CN");
		String title =  (String) ctx.getObjectAttribute("title");
		String email =  (String) ctx.getObjectAttribute("mail");
		String org =  (String) ctx.getObjectAttribute("company");
		
		List<String> nameSplit = NameUtil.splitFullName(name);
		String firstName = nameSplit.get(0);
		String lastName = "";
		if (nameSplit.size()>1) {
			lastName = nameSplit.get(1);
		}
		
		List<GrantedAuthority> mappedAuthorities = new ArrayList<GrantedAuthority>();
		
		for (GrantedAuthority granted : authorities) {
            if (granted.getAuthority().equalsIgnoreCase("All Users Anabatic")) {
                mappedAuthorities.add(new GrantedAuthority(){
                    private static final long serialVersionUID = 4356967414267942910L;

                    @Override
                    public String getAuthority() {
                        return "ROLE_USER";
                    } 

                });
            } else if(granted.getAuthority().equalsIgnoreCase("PRDC Core")) {
                mappedAuthorities.add(new GrantedAuthority() {
                    private static final long serialVersionUID = -5167156646226168080L;

                    @Override
                    public String getAuthority() {
                        return "ROLE_ADMIN";
                    }
                });
            }
        }
		
		Set<CorePermission> permission;
		domainUser = coreUserDao.getUserSpring(username);
		
		if (domainUser != null) {
//			permission = domainUser.getCorePermissions();
			permission = new HashSet<>();
			// DEPRECATED
			// String newSalt = KeyGenerators.string().generateKey();
			// String salt = domainUser.getUsername().concat(newSalt);

			// Check active locale
			if (StringUtils.isEmpty(domainUser.getPreferredLocale())) {
				domainUser.setPreferredLocale("en_US");
			}
			// Set active branch
			// if (domainUser.getActiveBranch() == null &&
			// domainUser.getCoreBranches().size() > 0) {
			// domainUser.setActiveBranch((CoreBranch)
			// domainUser.getCoreBranches().toArray()[0]);
			// }

			// Set active role
			if (domainUser.getActiveRole() == null
					&& domainUser.getCoreRoles().size() > 0) {
				domainUser.setActiveRole((CoreRole) domainUser.getCoreRoles()
						.toArray()[0]);
			}

			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			if (!domainUser.getAccountEnabled()) {
				enabled = false;
			}
			if (domainUser.getAccountExpired()) {
				accountNonExpired = false;
			}
			if (domainUser.getCredentialsExpired()) {
				credentialsNonExpired = false;
			}
			if (domainUser.getAccountExpired()) {
				accountNonLocked = false;
			}
			return new UserId(
					domainUser.getPasswordSalt(), 
					domainUser,
					domainUser.getUsername(),
					domainUser.getPassword(),
					enabled, 
					accountNonExpired,
					credentialsNonExpired,
					accountNonLocked, 
					getAuthorities(domainUser.getActiveRole().getId(), permission));
		} else {
			throw new UsernameNotFoundException(
					"Your account has not been activated");
		}
	}
	
	public List<GrantedAuthority> getAuthorities(Integer role,
			Set<CorePermission> permissions) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role,
				permissions));
		return authList;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(
			List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	public List<String> getRoles(Integer role, Set<CorePermission> permissions) {

		List<String> roles = new ArrayList<String>();
		Integer idActiveRole = role.intValue();
		CoreRole coreRole = coreRoleDao.getById(idActiveRole);

		Set<String> moduleType = setModuleType();
		Set<String> applicationType = setApplicationType();
		Set<String> actionType = setActionType();

		if (role.intValue() != 0) {
			roles.add(coreRole.getName());
		}
		if (permissions != null) {
			for (CorePermission corePermission : permissions) {
				String module = corePermission.getModuleName();
				String application = corePermission.getApplicationName();
				String action = corePermission.getActionName();
				boolean trueModel = module.equals("*");
				boolean trueApplication = application.equals("*");
				boolean trueAction = action.equals("*");
				for (String moduleTypes : moduleType) {
					if (trueModel) {
						module = moduleTypes;
					}
					for (String applicationTypes : applicationType) {
						if (trueApplication) {
							application = applicationTypes;
						}
						for (String actionTypes : actionType) {
							if (trueAction) {
								action = actionTypes;
							}
							roles.add(module + ":" + application + ":" + action
									+ ":" + corePermission.getRecordId());
						}
					}
				}
			}
		}
		return roles;
	}

	private Set<String> setApplicationType() {
		Set<String> applicationType = new HashSet<String>();

		applicationType.add(ApplicationTypeEnum.ALL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SYSTEM.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BRANCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CALENDAR.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CURRENCYRATE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.BATCH.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.CLOSING.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASIS
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTIBT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.MENU.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITY.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.SECURITYPOLICY
				.getApplicationValue());
		/* TODO: Add by Nana */
		applicationType.add(ApplicationTypeEnum.ACCOUNTOFFICER
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DEPARTMENT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.DENOMINATION
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.VAULT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TELLERACCOUNT
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.APPLICATION
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.LEVEL.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTBASE
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.INTERESTPARAMETER
				.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.PRODUCT.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.TAX.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.COA.getApplicationValue());
		applicationType.add(ApplicationTypeEnum.ACCOUNTING
				.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.TRANSACTION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.TRANSACTIONCODE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.VIRTUALMAP.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.AMORTIZATION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYAMORTIZATION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.COLLECTIVE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYCOLLECTIVE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.INDIVIDUAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SUMMARYINDIVIDUAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.ACCOUNT.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.ACCOUNTTEMPLATE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.RECURRING.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.REPORT.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.SOURCE.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.MAPPING.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DEFINITION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DIMENSION.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.DOUBLEENTRY.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.HIERARCHY.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.BATCHJOURNAL.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.COSTCENTER.getApplicationValue());
		// applicationType.add(ApplicationTypeEnum.BUDGET.getApplicationValue());
		return applicationType;
	}

	private Set<String> setModuleType() {
		Set<String> moduleType = new HashSet<String>();
		moduleType.add(ModuleTypeEnum.ALL.getModuleValue());
		moduleType.add(ModuleTypeEnum.CORE.getModuleValue());
		moduleType.add(ModuleTypeEnum.TRX.getModuleValue());
		// moduleType.add(ModuleTypeEnum.LEDGER.getModuleValue());
		return moduleType;
	}

	private Set<String> setActionType() {
		Set<String> actionType = new HashSet<String>();
		actionType.add(ActionTypeEnum.ALL.getId());
		actionType.add(ActionTypeEnum.AUTHORIZE.getId());
		actionType.add(ActionTypeEnum.DELETE.getId());
		actionType.add(ActionTypeEnum.INPUT.getId());
		actionType.add(ActionTypeEnum.EDIT.getId());
		actionType.add(ActionTypeEnum.READ.getId());
		return actionType;
	}
	
	@Override
	public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
		// TODO Auto-generated method stub
		
	}

}
