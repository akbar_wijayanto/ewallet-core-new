package com.ewallet.security.bean;

import org.springframework.security.core.context.SecurityContextHolder;

import com.ewallet.persistence.model.CoreUser;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 4:10:48 PM
 */
public final class LdapUserUtil {

	public synchronized static String getCurrentUsername() {
		LdapUserId subject = (LdapUserId) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(subject.getUsername()!=null){
			return subject.getUsername();
		}else{
			return null;
		}
	}

	public synchronized static CoreUser getCurrentUser() {
		if(SecurityContextHolder.getContext().getAuthentication() == null){
			return null;
		}
		Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(object instanceof UserId)
			return   ((UserId) object).getCoreUser();
		else
			return null;
	}

}

