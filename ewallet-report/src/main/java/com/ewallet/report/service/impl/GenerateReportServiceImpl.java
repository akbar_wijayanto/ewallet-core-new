package com.ewallet.report.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewallet.Constants;
import com.ewallet.persistence.dao.TrxOrderDao;
import com.ewallet.persistence.model.CoreSeller;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxSellerPayment;
import com.ewallet.report.dto.CoreUserTransactionReportDto;
import com.ewallet.report.dto.HistoryDownloadBean;
import com.ewallet.report.dto.OrderDto;
import com.ewallet.report.dto.OrderTransactionBean;
import com.ewallet.report.dto.RefundDto;
import com.ewallet.report.dto.RefundTransactionBean;
import com.ewallet.report.dto.ReportSettlementDetail;
import com.ewallet.report.service.GenerateReportService;
import com.ewallet.service.CoreSellerManager;
import com.ewallet.service.CoreSystemManager;
import com.ewallet.service.CoreUserManager;
import com.ewallet.service.TrxOrderManager;

@Service
public class GenerateReportServiceImpl implements GenerateReportService {
	
	
	@Autowired
	private TrxOrderManager trxOrderManager;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private CoreSellerManager coreSellerManager;
	
	@Autowired
	private TrxOrderDao trxOrderDao;
		
	@Override
	public List<RefundDto> getRefundListPeriodic(String startDate, String endDate) {
		List<TrxOrder> listTrxRefund = this.trxOrderManager.getPeriodicRefund(startDate, endDate);
		List<RefundDto> listrespon = new ArrayList<RefundDto>();
		for (TrxOrder trxOrder : listTrxRefund) {
			RefundDto refund = new RefundDto();
			refund.setRefundId(trxOrder.getOrderId());
			refund.setRefundDate(trxOrder.getOrderDate());
			refund.setSellerName(trxOrder.getCoreSeller().getFirstName()+" "+trxOrder.getCoreSeller().getLastName());
			refund.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
			BigDecimal totalrefund = new BigDecimal("0");
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				totalrefund = totalrefund.add(total);
			}
			refund.setTotalRefund(totalrefund);
			listrespon.add(refund);
		}
		
		return listrespon;
	}


	@Override
	public List<RefundDto> getListRefundReportDownload(String startDate, String endDate) {
		List<TrxOrder> listTrxRefund = this.trxOrderManager.getPeriodicRefund(startDate, endDate);
		BigDecimal gtot = this.trxOrderManager.getGrandTotalPeriodicFromRefundReport(startDate, endDate);
		List<RefundDto> listrespon = new ArrayList<RefundDto>();
		for (TrxOrder trxOrder : listTrxRefund) {
			RefundDto refund = new RefundDto();
			refund.setRefundId(trxOrder.getOrderId());
			refund.setRefundDate(trxOrder.getOrderDate());
			refund.setSellerName(trxOrder.getCoreSeller().getFirstName()+" "+trxOrder.getCoreSeller().getLastName());
			refund.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
			BigDecimal totalrefund = new BigDecimal("0");
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				totalrefund = totalrefund.add(total);				
				//gtot = gtot.add(totalrefund);
			}
			refund.setTotalRefund(totalrefund);
			refund.setGrandTotal(gtot);
			listrespon.add(refund);
		}
		return listrespon;
	}
	
	@Override
	public RefundTransactionBean getListRefundDownload(String startDate, String endDate){
		List<TrxOrder> listRefund = this.trxOrderManager.getPeriodicRefund(startDate, endDate);
		List<RefundDto> listRespon = new ArrayList<RefundDto>();
		RefundTransactionBean dto = new RefundTransactionBean();
		BigDecimal gtot = new BigDecimal("0");
		for (TrxOrder trxOrder : listRefund){
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				RefundDto refund = new RefundDto();
				refund.setRefundId(trxOrder.getOrderId());
				refund.setRefundDate(trxOrder.getOrderDate());
				refund.setSellerId(trxOrder.getCoreSeller().getSellerId());
				refund.setSellerName(trxOrder.getCoreSeller().getFirstName()+" "+trxOrder.getCoreSeller().getLastName());
				refund.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
				BigDecimal totalrefund = new BigDecimal("0");			
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				totalrefund = totalrefund.add(total);				
				gtot = gtot.add(totalrefund);
				refund.setTotalRefund(totalrefund);
				refund.setGrandTotal(gtot);
				listRespon.add(refund);
			}			
		}
		dto.setRefundDtoBean(listRespon);
		dto.setGrandTotal(gtot);
		return dto;		
	}
	
	@Override
	public OrderTransactionBean getListOrderDownload(String startDate, String endDate){
		List<TrxOrder> listOrder = this.trxOrderManager.getPeriodicOrder(startDate, endDate);
		List<OrderDto> listRespon = new ArrayList<OrderDto>();
		OrderTransactionBean dto = new OrderTransactionBean();
		BigDecimal gtot = new BigDecimal("0");
		BigDecimal gtot2 = new BigDecimal("0");
		for (TrxOrder trxOrder : listOrder){
			OrderDto order = new OrderDto();
			BigDecimal totalOrder = new BigDecimal("0");
			BigDecimal totalOrder2 = new BigDecimal("0");
			order.setOrderId(trxOrder.getOrderId());
			order.setStatus(trxOrder.getStatus());
			order.setOrderDate(trxOrder.getOrderDate());
			order.setPaidDate(trxOrder.getPaidDate());
			order.setSellerId(trxOrder.getCoreSeller().getSellerId());
			order.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()){
				BigDecimal total = BigDecimal.valueOf(orderDet.getQuantity()).multiply(orderDet.getPk().getCoreItem().getSellingPrice());
				BigDecimal totalPokok = BigDecimal.valueOf(orderDet.getQuantity()).multiply(orderDet.getPk().getCoreItem().getCostPrice());
				totalOrder = totalOrder.add(total);
				totalOrder2 = totalOrder2.add(totalPokok);
				gtot = gtot.add(total);
				gtot2=gtot2.add(totalPokok);
				order.setTotHargaPokok(totalOrder2);
				order.setTotHargaJual(totalOrder);
				order.setGrandTotal(gtot);
				order.setGrandTotalPokok(gtot2);
			}
			listRespon.add(order);
		}
		dto.setOrderDtoBean(listRespon);
		dto.setGrandTotal(gtot);
		dto.setGrandTotalPokok(gtot2);
		return dto;
	}


	@Override
	public RefundTransactionBean getListRefundBySellerId(String sellerId, String startDate, String endDate) {
		List<TrxOrder> listRefund = this.trxOrderManager.getPeriodicRefundBySellerid(sellerId, startDate, endDate);
		List<RefundDto> listRespon = new ArrayList<RefundDto>();
		RefundTransactionBean dto = new RefundTransactionBean();
		BigDecimal gtot = new BigDecimal("0");
		for (TrxOrder trxOrder : listRefund){
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()) {
				RefundDto refund = new RefundDto();
				refund.setRefundId(trxOrder.getOrderId());
				refund.setRefundDate(trxOrder.getOrderDate());
				refund.setSellerId(trxOrder.getCoreSeller().getSellerId());
				refund.setSellerName(trxOrder.getCoreSeller().getFirstName()+" "+trxOrder.getCoreSeller().getLastName());
				refund.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
				BigDecimal totalrefund = new BigDecimal("0");			
				BigDecimal total = orderDet.getPk().getCoreItem().getSellingPrice().multiply(new BigDecimal(orderDet.getQuantity()));
				totalrefund = totalrefund.add(total);				
				gtot = gtot.add(totalrefund);
				refund.setTotalRefund(totalrefund);
				refund.setGrandTotal(gtot);
				listRespon.add(refund);
			}			
		}
		dto.setRefundDtoBean(listRespon);
		dto.setGrandTotal(gtot);
		return dto;		
	}


	@Override
	public OrderTransactionBean getListOrderBySellerId(String sellerId, String startDate, String endDate) {
		List<TrxOrder> listOrder = this.trxOrderManager.getPeriodicOrderBySellerid(sellerId, startDate, endDate);
		List<OrderDto> listRespon = new ArrayList<OrderDto>();
		OrderTransactionBean dto = new OrderTransactionBean();
		BigDecimal gtot = new BigDecimal("0");
		BigDecimal gtot2 = new BigDecimal("0");
		for (TrxOrder trxOrder : listOrder){
			OrderDto order = new OrderDto();
			BigDecimal totalOrder = new BigDecimal("0");
			BigDecimal totalOrder2 = new BigDecimal("0");
			order.setOrderId(trxOrder.getOrderId());
			order.setStatus(trxOrder.getStatus());
			order.setOrderDate(trxOrder.getOrderDate());
			order.setPaidDate(trxOrder.getPaidDate());
			order.setSellerId(trxOrder.getCoreSeller().getSellerId());
			order.setUserName(trxOrder.getCoreUser().getFirstName()+" "+trxOrder.getCoreUser().getLastName());
			for (TrxOrderItemDetail orderDet : trxOrder.getTrxOrderItemDetails()){
				BigDecimal total = BigDecimal.valueOf(orderDet.getQuantity()).multiply(orderDet.getPk().getCoreItem().getSellingPrice());
				BigDecimal totalPokok = BigDecimal.valueOf(orderDet.getQuantity()).multiply(orderDet.getPk().getCoreItem().getCostPrice());
				totalOrder = totalOrder.add(total);
				totalOrder2 = totalOrder2.add(totalPokok);
				gtot = gtot.add(total);
				gtot2=gtot2.add(totalPokok);
				order.setTotHargaPokok(totalOrder2);
				order.setTotHargaJual(totalOrder);
				order.setGrandTotal(gtot);
				order.setGrandTotalPokok(gtot2);
			}
			listRespon.add(order);
		}
		dto.setOrderDtoBean(listRespon);
		dto.setGrandTotal(gtot);
		dto.setGrandTotalPokok(gtot2);
		return dto;
	}

}
