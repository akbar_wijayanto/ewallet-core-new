/**
 * 
 */
package com.ewallet.report.service;

import java.util.List;

import com.ewallet.report.dto.CoreSellerSettlementReportDto;
import com.ewallet.report.dto.RefundDto;
import com.ewallet.report.dto.SellerTransactionBean;

/**
 * @author akbar.wijayanto
 * Date Jan 4, 2016 2:58:24 PM
 */
public interface CoreSellerReportService {

	List<CoreSellerSettlementReportDto> getAllSellerSettlementReport();
	SellerTransactionBean getAllSellerTransactionReport();
	List<CoreSellerSettlementReportDto> getSellerSettlementReportBySellerId(String sellerId);
	List<CoreSellerSettlementReportDto> getAllSellerSettlementPayment();
	
	SellerTransactionBean getAllSellerTransactionPeriodicReport(String startDate, String endDate);
	SellerTransactionBean getAllSellerTransactionPeriodicReportBySellerId(String sellerId, String startDate, String endDate);
	
	List<CoreSellerSettlementReportDto> getSettlementInfoPeriodic(String startDat, String endDat);
}
