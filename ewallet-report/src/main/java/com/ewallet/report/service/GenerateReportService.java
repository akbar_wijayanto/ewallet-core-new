package com.ewallet.report.service;

/**
 * @author akbar.wijayanto
 * Date Dec 14, 2015 5:17:37 PM
 */

import java.util.List;

import com.ewallet.report.dto.CoreUserTransactionReportDto;
import com.ewallet.report.dto.OrderTransactionBean;
import com.ewallet.report.dto.RefundDto;
import com.ewallet.report.dto.RefundTransactionBean;

public interface GenerateReportService {

	/*public List<CoreUserTransactionReportDto> listPembeli(); */
	
	List<RefundDto> getRefundListPeriodic(String startDate, String endDate);
	
	List<RefundDto> getListRefundReportDownload(String startDate,
			String endDate);
	
	RefundTransactionBean getListRefundDownload(String startDate, String endDate);

	OrderTransactionBean getListOrderDownload(String startDate, String endDate);
	
	RefundTransactionBean getListRefundBySellerId(String sellerId, String startDate, String endDate);
	
	OrderTransactionBean getListOrderBySellerId(String sellerId, String startDate, String endDate);
}
