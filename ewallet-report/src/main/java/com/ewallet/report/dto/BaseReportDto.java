package com.ewallet.report.dto;

import java.sql.Timestamp;

/**
 * Base class report dto, contains common used header and footer of report
 * Detail of report explained in concrete class
 * 
 * @author afuza.sudibyo
 */
public abstract class BaseReportDto {

	protected BaseReportDto() {
		super();
	}

	// header
	protected String reportTitle;
	protected String reportDate;
	protected String branchName;
	protected String branchCode;
	protected String ccy;
	protected Timestamp startDate;
	protected Timestamp endDate;
	protected String path;
	
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	

}
