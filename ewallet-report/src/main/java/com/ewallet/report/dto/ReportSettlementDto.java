package com.ewallet.report.dto;

import java.math.BigDecimal;
import java.util.List;

public class ReportSettlementDto {
	private String idSeller;
	private String namaSeller;
	private BigDecimal grandTotal;
	private BigDecimal totalTrx;
	private BigDecimal percentage;
	private BigDecimal servicecharge;
	private BigDecimal charge;
	private BigDecimal totalPaid;
	private String accountNo;
	private List<ReportSettlementDetail> listDetail;
	
	public String getIdSeller() {
		return idSeller;
	}
	public void setIdSeller(String idSeller) {
		this.idSeller = idSeller;
	}
	public List<ReportSettlementDetail> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<ReportSettlementDetail> listDetail) {
		this.listDetail = listDetail;
	}
	public String getNamaSeller() {
		return namaSeller;
	}
	public void setNamaSeller(String namaSeller) {
		this.namaSeller = namaSeller;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	public BigDecimal getTotalTrx() {
		return totalTrx;
	}
	public void setTotalTrx(BigDecimal totalTrx) {
		this.totalTrx = totalTrx;
	}
	public BigDecimal getPercentage() {
		return percentage;
	}
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public BigDecimal getServicecharge() {
		return servicecharge;
	}
	public void setServicecharge(BigDecimal servicecharge) {
		this.servicecharge = servicecharge;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public BigDecimal getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(BigDecimal totalPaid) {
		this.totalPaid = totalPaid;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	

}
