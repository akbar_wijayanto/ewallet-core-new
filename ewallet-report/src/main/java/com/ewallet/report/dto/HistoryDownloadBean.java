package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HistoryDownloadBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	List<ReportSettlementDetail> listdetail;
	private String idSeller;
	private String namaSeller;
	private BigDecimal grandTotal;
	private BigDecimal totalTrx;
	private BigDecimal percentage;
	private BigDecimal servicecharge;
	private BigDecimal charge;
	private BigDecimal totalPaid;
	private String accountNo;
	
	private String idOrder;
	private String status;
	private String datOrder;
	private String itemName;
	private BigDecimal hargaPokok;
	private String quantity;
	
	public String getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(String idOrder) {
		this.idOrder = idOrder;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDatOrder() {
		return datOrder;
	}
	public void setDatOrder(String datOrder) {
		this.datOrder = datOrder;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getHargaPokok() {
		return hargaPokok;
	}
	public void setHargaPokok(BigDecimal hargaPokok) {
		this.hargaPokok = hargaPokok;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public List<ReportSettlementDetail> getListdetail() {
		return listdetail;
	}
	public void setListdetail(List<ReportSettlementDetail> listdetail) {
		this.listdetail = listdetail;
	}
	public String getIdSeller() {
		return idSeller;
	}
	public void setIdSeller(String idSeller) {
		this.idSeller = idSeller;
	}
	public String getNamaSeller() {
		return namaSeller;
	}
	public void setNamaSeller(String namaSeller) {
		this.namaSeller = namaSeller;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	public BigDecimal getTotalTrx() {
		return totalTrx;
	}
	public void setTotalTrx(BigDecimal totalTrx) {
		this.totalTrx = totalTrx;
	}
	public BigDecimal getPercentage() {
		return percentage;
	}
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public BigDecimal getServicecharge() {
		return servicecharge;
	}
	public void setServicecharge(BigDecimal servicecharge) {
		this.servicecharge = servicecharge;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public BigDecimal getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(BigDecimal totalPaid) {
		this.totalPaid = totalPaid;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	
}
