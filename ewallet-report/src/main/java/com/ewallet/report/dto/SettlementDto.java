package com.ewallet.report.dto;

import java.math.BigDecimal;

public class SettlementDto {
	
	private String sellerName;
	private BigDecimal totalTrx;
	private BigDecimal grandTotal;
	private BigDecimal precentage;
	private BigDecimal charge;
	private BigDecimal totalAmount;
	private String accountNo;
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public BigDecimal getTotalTrx() {
		return totalTrx;
	}
	public void setTotalTrx(BigDecimal totalTrx) {
		this.totalTrx = totalTrx;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	public BigDecimal getPrecentage() {
		return precentage;
	}
	public void setPrecentage(BigDecimal precentage) {
		this.precentage = precentage;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	

}
