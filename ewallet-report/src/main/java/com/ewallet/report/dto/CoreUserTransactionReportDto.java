package com.ewallet.report.dto;

import java.math.BigDecimal;

public class CoreUserTransactionReportDto {
	private String id;
	private String company;
	private String nama;
	private BigDecimal totalTrx;
	private String status;
	private BigDecimal simpananwajib;
	private BigDecimal simpananpokok;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public BigDecimal getTotalTrx() {
		return totalTrx;
	}
	public void setTotalTrx(BigDecimal totalTrx) {
		this.totalTrx = totalTrx;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getSimpananwajib() {
		return simpananwajib;
	}
	public void setSimpananwajib(BigDecimal simpananwajib) {
		this.simpananwajib = simpananwajib;
	}
	public BigDecimal getSimpananpokok() {
		return simpananpokok;
	}
	public void setSimpananpokok(BigDecimal simpananpokok) {
		this.simpananpokok = simpananpokok;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	private BigDecimal grandTotal;

}
