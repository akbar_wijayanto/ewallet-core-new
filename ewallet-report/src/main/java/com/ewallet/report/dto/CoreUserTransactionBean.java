package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CoreUserTransactionBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	List<CoreUserTransactionReportDto> buyerDtoBean;
	BigDecimal grandTotal;
	
	public List<CoreUserTransactionReportDto> getBuyerDtoBean() {
		return buyerDtoBean;
	}
	public void setBuyerDtoBean(List<CoreUserTransactionReportDto> buyerDtoBean) {
		this.buyerDtoBean = buyerDtoBean;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	
}
