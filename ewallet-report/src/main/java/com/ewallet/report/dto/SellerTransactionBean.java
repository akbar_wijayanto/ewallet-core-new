package com.ewallet.report.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class SellerTransactionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<CoreSellerTransactionReportDto> coreSellerTransactionReportDtoBean;
	private BigDecimal grandTotal;

	public List<CoreSellerTransactionReportDto> getCoreSellerTransactionReportDtoBean() {
		return coreSellerTransactionReportDtoBean;
	}

	public void setCoreSellerTransactionReportDtoBean(
			List<CoreSellerTransactionReportDto> coreSellerTransactionReportDtoBean) {
		this.coreSellerTransactionReportDtoBean = coreSellerTransactionReportDtoBean;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

}
