/**
 * 
 */
package com.ewallet.report.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author akbar.wijayanto
 * Date Dec 20, 2015 11:45:59 AM
 */
public class CoreSellerSettlementReportDto extends BaseReportDto {

	private String sellerId;
	private String sellerName;
	private BigDecimal grandTotal;
	private BigDecimal totalTransaction;
	private BigDecimal percentage;
	private BigDecimal serviceCash;
	private BigDecimal charge;
	private BigDecimal totalAmountPaid;
	private String accountNumber;
	private Timestamp settlementDate;
	private String flagPaid;
	
	private String startDat;
	private String endDat;
	
	public String getFlagPaid() {
		return flagPaid;
	}
	public void setFlagPaid(String flagPaid) {
		this.flagPaid = flagPaid;
	}
	public String getSellerId() {
		return sellerId;
	}
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}
	public BigDecimal getTotalTransaction() {
		return totalTransaction;
	}
	public void setTotalTransaction(BigDecimal totalTransaction) {
		this.totalTransaction = totalTransaction;
	}
	public BigDecimal getPercentage() {
		return percentage;
	}
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public BigDecimal getServiceCash() {
		return serviceCash;
	}
	public void setServiceCash(BigDecimal serviceCash) {
		this.serviceCash = serviceCash;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Timestamp getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Timestamp settlementDate) {
		this.settlementDate = settlementDate;
	}
	public BigDecimal getTotalAmountPaid() {
		return totalAmountPaid;
	}
	public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}
	
	public String getStartDat() {
		return startDat;
	}

	public void setStartDat(String startDat) {
		this.startDat = startDat;
	}

	public String getEndDat() {
		return endDat;
	}

	public void setEndDat(String endDat) {
		this.endDat = endDat;
	}
}
