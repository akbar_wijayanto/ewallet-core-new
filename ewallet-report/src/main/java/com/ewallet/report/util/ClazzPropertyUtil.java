package com.ewallet.report.util;
/**
 * 
 */
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ewallet.exception.RetailException;

/**Class reflection functions, full support for Java reflection
 * 
 * @author agung.kurniawan
 * Date : @26 Sep 2014
 */
public class ClazzPropertyUtil {
	
	private static final Logger log = LoggerFactory.getLogger(ClazzPropertyUtil.class);
		
	/**Set field of class, like invoke setter method
	 * 
	 * @param clazz
	 * @param fieldName
	 * @param value
	 * @throws MiniLoanException
	 * @return clazz
	 */
	public static Object setField(Object clazz, String fieldName, String value) throws RetailException {
		try {
			Field field = null;
			if (clazz.getClass().getSuperclass()==null) 
				field = clazz.getClass().getDeclaredField(fieldName);
			else 
				field = clazz.getClass().getSuperclass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(clazz, value);
			return clazz;
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (NoSuchFieldException e) {
			try {
				Field field = clazz.getClass().getSuperclass().getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(clazz, value);
				return clazz;
			} catch (Exception e1) {
				log.error(e.getMessage(), e);
				throw new RetailException(e);
			}
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		}

	}
	
	/**Get field of class, like invoke getter method
	 * 
	 * @param clazz
	 * @param fieldName
	 * @param value
	 * @return Object
	 * @throws MiniLoanException
	 */
	public static Object getField(Object clazz, String fieldName, String value) throws RetailException {
		try {
			Field field = null;
			if (clazz.getClass().getSuperclass()!=null)
				field = clazz.getClass().getDeclaredField(fieldName);
			else 
				field = clazz.getClass().getSuperclass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(clazz);
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (NoSuchFieldException e) {
			try {
				Field field = clazz.getClass().getSuperclass().getDeclaredField(fieldName);
				field.setAccessible(true);
				return field.get(clazz);
			} catch (Exception e1) {
				log.error(e.getMessage(), e);
				throw new RetailException(e);
			}
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		}
	}
	
	/**Get all methods of instance
	 * 
	 * @param clazz
	 * @return list of methods
	 * @throws MiniLoanException
	 */
	public static Method[] getMethods(Object clazz) throws RetailException {
		try {
			return clazz.getClass().getDeclaredMethods();
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		}
	}
	
	/**Create new instance of class name
	 * 
	 * @param className
	 * @return new instance of class
	 * @throws MiniLoanException
	 */
	public static Object createInstance(String className) throws RetailException {
		try {
			return Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage(), e);
			throw new RetailException(e.getMessage());
		}
	}
	
	/**Invoke method on object instance
	 * 
	 * @param interfaceClazz
	 * @param clazzParameter
	 * @param method
	 * @return returned object from method
	 * @throws Exception
	 */
	public static Object invokeMethod(Object interfaceClazz, Object clazzParameter, String method) throws Exception {
    	Method requestMethod = interfaceClazz.getClass().getMethod(method, new Class[]{clazzParameter.getClass()});
        Object response = requestMethod.invoke(interfaceClazz, clazzParameter);
        return response;
	}
	
	/**Get declared field of object
	 * 
	 * @param object
	 * @return
	 */
	public static Field[] getFields(Object object) {
		List<Field> fields = new ArrayList<Field>();
		fields.addAll(Arrays.asList(object.getClass().getDeclaredFields()));
		boolean hasSuperclass = object.getClass().getSuperclass()==null ? false : true;
		Field[] subClassFields = object.getClass().getDeclaredFields();
		Field[] supClassFields = object.getClass().getSuperclass()!=null ? object.getClass().getSuperclass().getDeclaredFields() : null ;
		Field[] result = (Field[]) ArrayUtils.addAll(subClassFields, supClassFields);
		return hasSuperclass ? result : subClassFields;
	}
	
}
