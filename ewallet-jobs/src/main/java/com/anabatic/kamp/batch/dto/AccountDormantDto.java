/**
 * 
 */
package com.anabatic.kamp.batch.dto;

import java.util.Date;

import com.anabatic.kamp.enumeration.AccountStatusEnum;

/**
 * 
 * @version 1.0, Aug 19, 2015 9:41:05 PM
 */
public class AccountDormantDto {
	private Long id;
	private String codeAccount;
	private Date dormantDueDate;
	private AccountStatusEnum statusAccount;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodeAccount() {
		return codeAccount;
	}
	public void setCodeAccount(String codeAccount) {
		this.codeAccount = codeAccount;
	}
	public Date getDormantDueDate() {
		return dormantDueDate;
	}
	public void setDormantDueDate(Date dormantDueDate) {
		this.dormantDueDate = dormantDueDate;
	}
	public AccountStatusEnum getStatusAccount() {
		return statusAccount;
	}
	public void setStatusAccount(AccountStatusEnum statusAccount) {
		this.statusAccount = statusAccount;
	}
}
