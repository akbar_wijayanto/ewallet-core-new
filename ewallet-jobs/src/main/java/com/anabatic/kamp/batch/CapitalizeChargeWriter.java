package com.anabatic.kamp.batch;

import java.math.BigDecimal;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.anabatic.kamp.batch.dto.CapitalizeChargeDto;
import com.anabatic.kamp.enumeration.ActivityEnum;
import com.anabatic.kamp.enumeration.TransactionTypeEnum;
import com.anabatic.kamp.persistence.model.CustomerAccountOpening;
import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.CustomerAccountOpeningManager;
import com.anabatic.kamp.service.JournalTransactionManager;
import com.anabatic.kamp.service.TransactionCodeManager;
import com.anabatic.kamp.service.TrxRecordManager;

public class CapitalizeChargeWriter implements ItemWriter<CapitalizeChargeDto> {

	private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        if (jdbcTemplate == null) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }
    }
    
	@Autowired 
	private CustomerAccountOpeningManager customerAccountOpeningManager;
	
	@Autowired 
	private CoreSystemManager coreSystemManager;
	
	@Autowired 
	private TransactionCodeManager transactionCodeManager;
	
	@Autowired 
	private JournalTransactionManager journalTransactionManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
    
	@Override
	public void write(final List<? extends CapitalizeChargeDto> items) throws Exception {
		if(items.size()>0) {
			for (CapitalizeChargeDto item : items) {
				CustomerAccountOpening accountTo = customerAccountOpeningManager.get(item.getAccountId());
				TrxRecord record = new TrxRecord();
				record.setBookingDate(coreSystemManager.getCurrentSystem().getTodayDate());
				record.setPostingDate(coreSystemManager.getCurrentSystem().getPostingDate());
				record.setRate(BigDecimal.ONE);
				record.setAccountFrom(null);
				record.setAccountTo(accountTo.getCodeAccount());
				record.setAmount(item.getChargeAmount());
				record.setDescription("CAPTILAZE CHARGE : " + accountTo.getCodeAccount());
//				record.setCoreBranch(accountTo.getIdCif().getBranchOpenCif().getId());
				record.setCoreBranch(accountTo.getBranchOpen().getId());
				record.setCoreCurrency(accountTo.getCurrency().getId());
				record.setPairedCurrency(accountTo.getCurrency().getId());
				record.setTeller("TL002");
				record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.CHARGE_CAPITALIZE.getTransactionType()));
				
				record = trxRecordManager.save(record);
				customerAccountOpeningManager.updateBalance(accountTo, record.getAmount(), ActivityEnum.ACCOUNT_SUBSTARCT);
				journalTransactionManager.createJurnal(record.getTrxCode().getTransactionCode(), record, null, accountTo);
			}
		}
	}

}
