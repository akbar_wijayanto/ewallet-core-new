package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.persistence.model.CoreBranch;

public class CoreBranchRowMapper implements RowMapper<CoreBranch> {

	@Override
	public CoreBranch mapRow(ResultSet rs, int rowNum) throws SQLException {
		CoreBranch branch = new CoreBranch();
		branch.setId(rs.getString("id"));
		branch.setOnlineState(rs.getInt("online_state"));
		return branch;
	}


}
