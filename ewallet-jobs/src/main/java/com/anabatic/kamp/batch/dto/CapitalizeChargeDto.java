/**
 * 
 */
package com.anabatic.kamp.batch.dto;

import java.math.BigDecimal;

/**
 * 
 * @version 1.0, Aug 21, 2015 4:02:49 PM
 */
public class CapitalizeChargeDto {
	private Long accountId;
	private String codeAccount;
	private BigDecimal balance;
	private String chargeId;
	private BigDecimal chargeAmount;
	private Long chargeCode;
	private String creditAccount;
	private String debitAccount;
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getCodeAccount() {
		return codeAccount;
	}
	public void setCodeAccount(String codeAccount) {
		this.codeAccount = codeAccount;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getChargeId() {
		return chargeId;
	}
	public void setChargeId(String chargeId) {
		this.chargeId = chargeId;
	}
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	public Long getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(Long chargeCode) {
		this.chargeCode = chargeCode;
	}
	public String getCreditAccount() {
		return creditAccount;
	}
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
}
