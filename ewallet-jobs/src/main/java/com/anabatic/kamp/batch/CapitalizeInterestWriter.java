package com.anabatic.kamp.batch;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.batch.dto.CapitalizeInterestDto;
import com.anabatic.kamp.enumeration.ActivityEnum;
import com.anabatic.kamp.enumeration.TransactionTypeEnum;
import com.anabatic.kamp.enumeration.TypeInterestEnum;
import com.anabatic.kamp.persistence.model.CustomerAccountOpening;
import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.security.bean.UserUtil;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.CustomerAccountOpeningManager;
import com.anabatic.kamp.service.JournalTransactionManager;
import com.anabatic.kamp.service.TransactionCodeManager;
import com.anabatic.kamp.service.TrxRecordManager;

/**
 * 
 * @version 1.0, Aug 18, 2015 4:14:53 AM
 */
public class CapitalizeInterestWriter implements ItemWriter<CapitalizeInterestDto> {

	@Autowired 
	private CustomerAccountOpeningManager customerAccountOpeningManager;
	
	@Autowired
	private CoreSystemManager coreSystemManager;
	
	@Autowired
	private TransactionCodeManager transactionCodeManager;
	
	@Autowired
	private JournalTransactionManager journalTransactionManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
    
	@Override
	public void write(final List<? extends CapitalizeInterestDto> items) throws Exception {
		if(items.size()>0) {
			for (CapitalizeInterestDto item : items) {
				CustomerAccountOpening accountTo = customerAccountOpeningManager.get(item.getId());
				TrxRecord record = new TrxRecord();
				record.setBookingDate(coreSystemManager.getCurrentSystem().getTodayDate());
				record.setPostingDate(coreSystemManager.getCurrentSystem().getPostingDate());
				record.setRate(BigDecimal.ONE);
				record.setAccountFrom(null);
				record.setAccountTo(accountTo.getCodeAccount());
				record.setAmount(item.getCapitalizeAmount());
				record.setDescription("CAPITALIZE INTEREST TO ACCOUNT : " + accountTo.getCodeAccount());
				record.setCoreBranch(UserUtil.getCurrentUserBranchId().getId());
				record.setCoreCurrency(accountTo.getCurrency().getId());
				record.setPairedCurrency(accountTo.getCurrency().getId());
				record.setTeller("TL002");
				
				if(item.getTypeInterest() == TypeInterestEnum.CREDIT)
					record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.INTEREST_CAPITALIZE_CREDIT.getTransactionType()));
				else
					record.setTrxCode(transactionCodeManager.get(TransactionTypeEnum.INTEREST_CAPITALIZE_DEBIT.getTransactionType()));
				
//				if(customerAccountOpeningManager.isTaxAble(accountTo)) {
//					BigDecimal percentage = accountTo.getTax().getPercentage();
//					BigDecimal taxAmount = record.getAmount().multiply(percentage.divide(new BigDecimal(100)));
//					BigDecimal interestAfterTaxed = record.getAmount().subtract(taxAmount.setScale(2, RoundingMode.HALF_UP));
//					record.setAmount(interestAfterTaxed);
//					record.setConvertedAmount(interestAfterTaxed);
//				}
				
				record = trxRecordManager.save(record);
				
				if(record.getTrxCode().getTransactionCode().equals(TransactionTypeEnum.INTEREST_CAPITALIZE_CREDIT.getTransactionType()))
					customerAccountOpeningManager.updateBalance(accountTo, record.getAmount(), ActivityEnum.ACCRUE_ADD);
				else
					customerAccountOpeningManager.updateBalance(accountTo, record.getAmount(), ActivityEnum.ACCRUE_SUBSTARCT);
				
				journalTransactionManager.createJurnal(record.getTrxCode().getTransactionCode(), record, null, accountTo);
			}
		}
	}

}
