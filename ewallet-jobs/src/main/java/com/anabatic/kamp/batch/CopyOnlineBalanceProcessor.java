package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.anabatic.kamp.persistence.model.CustomerAccountOpening;


/**
 * 
 * @version 1.0, Aug 16, 2015 10:46:15 PM
 */
public class CopyOnlineBalanceProcessor implements ItemProcessor<CustomerAccountOpening, CustomerAccountOpening> {

	
	@Override
	public CustomerAccountOpening process(CustomerAccountOpening item) throws Exception {
    	return item;
	}

}
