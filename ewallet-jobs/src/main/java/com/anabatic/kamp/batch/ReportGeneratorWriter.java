/**
 * 
 */
package com.anabatic.kamp.batch;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.persistence.model.CoreReport;
import com.anabatic.kamp.persistence.model.CoreSystem;
import com.anabatic.kamp.persistence.model.FileDirectory;
import com.anabatic.kamp.report.service.GenerateReportService;
import com.anabatic.kamp.report.util.ClientReportGenerator;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.FileDirectoryManager;

/**
 * 
 * @version 1.0, Oct 6, 2015 10:59:52 AM
 */
public class ReportGeneratorWriter implements ItemWriter<CoreReport> {

	@Autowired
	private FileDirectoryManager fileDirectoryManager;
	
	@Autowired
	private GenerateReportService generateReportService;
	
	@Autowired
	private CoreSystemManager coreSystemManager;
	
	@Override
	public void write(List<? extends CoreReport> items) throws Exception {
		for (CoreReport item : items) {
			CoreSystem coreSystem = coreSystemManager.getCurrentSystem();
			FileDirectory fd = fileDirectoryManager.getLiveById("FILE_DIRECTORY"); 
			List list = getData(item.getId(), null, coreSystem.getTodayDate(), coreSystem.getTodayDate());
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String path = dateFormat.format(coreSystem.getTodayDate()) + File.separator + item.getReportName().replace(" ", "_");
			ClientReportGenerator.generatePDF(item.getReportFileName(), list, fd.getInputDirectory() + "/" + path);
		}
	}
	
	 private List getData(Long id, String parameter, Date startDate, Date endDate) {
		switch (id.intValue()) {
			case 1: return generateReportService.generateAccountStatementReport(startDate,endDate,parameter);
			case 2: return generateReportService.generateAccountOpeningReport(startDate,endDate);
			case 3: return generateReportService.generateAccountMaintenanceReport(startDate,endDate);
			case 4: return generateReportService.generateAccountClosingReport(startDate,endDate);
			case 5: return generateReportService.generateAccountBalanceOutsatandingByAOReport(startDate,endDate,parameter);
			case 6: return generateReportService.generateAccountBalanceOutsatandingByCurrencyReport(startDate,endDate,parameter);
			case 7: return generateReportService.generateAccountBalanceOutsatandingByProductReport(startDate,endDate,parameter);
//			case 8: return generateReportService.generateClearingTransactionReport();
			case 8: return generateReportService.generateTransactionReport(startDate,endDate);
			case 9: return generateReportService.generateTransactionByUserReport(startDate,endDate,parameter);
			case 10: return generateReportService.generateTransactionWithOverrideReport(startDate,endDate);
			case 11: return generateReportService.generateTransactionWithOverrideByUserReport(startDate,endDate,parameter);
		}
		
		return null;
	} 
}
