package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.persistence.model.CustomerAccountOpening;


/**
 * 
 * @version 1.0, Aug 16, 2015 10:45:48 PM
 */
public class CopyOnlineBalanceRowMapper implements RowMapper<CustomerAccountOpening> {

	@Override
	public CustomerAccountOpening mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomerAccountOpening account = new CustomerAccountOpening();
		account.setId(rs.getLong("id"));
		account.setCodeAccount(rs.getString("code_account"));
		account.setOnlineBalance(rs.getBigDecimal("online_balance"));
		account.setWorkingBalance(rs.getBigDecimal("working_balance"));
		account.setAvailableBalance(rs.getBigDecimal("available_balance"));
		return account;
	}

}
