package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.batch.dto.AccountDormantDto;
import com.anabatic.kamp.enumeration.AccountStatusEnum;
import com.anabatic.kamp.service.CoreSystemManager;

/**
 * 
 * @version 1.0, Aug 19, 2015 9:56:32 PM
 */
public class ChangeAccountStatusDormantProcessor implements ItemProcessor<AccountDormantDto, AccountDormantDto>{

	@Autowired
	private CoreSystemManager coreSystemManager;
	
	@Override
	public AccountDormantDto process(AccountDormantDto item) throws Exception {
		if(coreSystemManager.getCurrentSystem().getTodayDate().compareTo(item.getDormantDueDate()) > 0) 
			item.setStatusAccount(AccountStatusEnum.DORMANT);
		return item;
	}

}
