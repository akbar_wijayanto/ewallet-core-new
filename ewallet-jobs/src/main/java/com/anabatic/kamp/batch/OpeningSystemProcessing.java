package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.anabatic.kamp.enumeration.SystemStatusEnum;
import com.anabatic.kamp.persistence.model.CoreSystem;

public class OpeningSystemProcessing implements ItemProcessor<CoreSystem, CoreSystem>{

	@Override
	public CoreSystem process(CoreSystem item) throws Exception {
		item.setSystemStatus(SystemStatusEnum.ONLINE.getIndex());
		return item; 
	}

}
