package com.anabatic.kamp.batch.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.enumeration.SystemStatusEnum;
import com.anabatic.kamp.service.CoreSystemManager;


public class CloseOfBusinessJobListener implements JobExecutionListener{

//	private CoreSystemManager coreSystemManager;
//
//	
//	@Autowired
//	public void setCoreSystemManager(CoreSystemManager coreSystemManager) {
//		this.coreSystemManager = coreSystemManager;
//	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
//		coreSystemManager.setStatus(SystemStatusEnum.Offline);
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if(jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)){
//			coreSystemManager.setStatus(SystemStatusEnum.Online);
		}
	}
	
}
