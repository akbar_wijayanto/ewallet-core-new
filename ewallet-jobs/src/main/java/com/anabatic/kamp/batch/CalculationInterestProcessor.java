package com.anabatic.kamp.batch;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.businesslogic.calculator.balance.AverageBalanceCalculator;
import com.anabatic.kamp.businesslogic.calculator.balance.DailyBalanceCalculator;
import com.anabatic.kamp.businesslogic.calculator.balance.MinBalanceCalculator;
import com.anabatic.kamp.businesslogic.calculator.interest.InterestCalculator;
import com.anabatic.kamp.businesslogic.calculator.interest.InterestTier;
import com.anabatic.kamp.businesslogic.calculator.interest.InterestTierDto;
import com.anabatic.kamp.businesslogic.calculator.interest.InterestTierSingleton;
import com.anabatic.kamp.businesslogic.util.DateCalculatorUtil;
import com.anabatic.kamp.enumeration.BalanceTypeEnum;
import com.anabatic.kamp.enumeration.InterestTypeEnum;
import com.anabatic.kamp.enumeration.TypeInterestEnum;
import com.anabatic.kamp.persistence.model.CoreInterestParameter;
import com.anabatic.kamp.persistence.model.CoreInterestParameterDetail;
import com.anabatic.kamp.persistence.model.CoreSystem;
import com.anabatic.kamp.persistence.model.CustomerAccountDailyBalance;
import com.anabatic.kamp.persistence.model.CustomerAccountOpening;
import com.anabatic.kamp.persistence.model.Product;
import com.anabatic.kamp.service.CoreInterestParameterManager;
import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.CustomerAccountDailyBalanceManager;
import com.anabatic.kamp.service.CustomerCifManager;
import com.anabatic.kamp.service.ProductManager;
import com.anabatic.kamp.service.TrxRecordManager;
import com.anabatic.kamp.service.TrxReversalManager;

/**
 * 
 * @version 1.0, Aug 6, 2015 9:20:49 PM
 */
public class CalculationInterestProcessor implements ItemProcessor<CustomerAccountOpening, List<CustomerAccountDailyBalance>> {

	@Autowired 
	private ProductManager productManager;
	
	@Autowired 
	private CoreSystemManager coreSystemManager;
	
	@Autowired 
	private CustomerCifManager customerCifManager;
	
	@Autowired
	private CustomerAccountDailyBalanceManager customerAccountDailyBalanceManager;
	
	@Autowired
	private CoreInterestParameterManager coreInterestParameterManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Autowired
	private TrxReversalManager trxReversalManager;
	
	@Override
	public List<CustomerAccountDailyBalance> process(CustomerAccountOpening item) throws Exception {
    	CoreSystem coreSystem = coreSystemManager.getCurrentSystem();
    	Product product = productManager.get(item.getProductId().getId());
    	
    	List<BigDecimal> total = customerAccountDailyBalanceManager.getCurrentBalance(item.getId(), product.getInterestCreditDate(), item.getWorkingBalance());
    	
    	DailyBalanceCalculator dailyCalculator = new DailyBalanceCalculator();
    	AverageBalanceCalculator averageCalculator = new AverageBalanceCalculator();
    	MinBalanceCalculator minCalculator = new MinBalanceCalculator();
    	
    	BigDecimal daily   = dailyCalculator.calculate(total);
    	BigDecimal average = averageCalculator.calculate(total);
    	BigDecimal minimum = minCalculator.calculate(total);
    	
		BigDecimal amount = BigDecimal.ZERO;
		BigDecimal checkingAmount = BigDecimal.ZERO;
		CoreInterestParameter selectedInterest = null;
		Boolean same = false;
		for (CoreInterestParameter interest : product.getInterestType()) {
			switch (interest.getBalanceTypeForCalculation()) {
				case DAILY: amount = daily; break;
				case MINIMUM: amount = minimum; break;
				case AVERAGE: amount = average; break;
			}

			switch (interest.getBalanceTypeForChecking()) {
				case DAILY: checkingAmount = daily; break;
				case MINIMUM: checkingAmount = minimum; break;
				case AVERAGE: checkingAmount = average; break;
			}

			same = interest.getBalanceTypeForCalculation().compareTo(interest.getBalanceTypeForChecking()) == 0;
			
        	selectedInterest = (CoreInterestParameter) BeanUtils.cloneBean(interest);
        	
        	if(amount.compareTo(BigDecimal.ZERO) > 0 && interest.getTypeInterest() == TypeInterestEnum.CREDIT) break;
        	if(amount.compareTo(BigDecimal.ZERO) < 0 && interest.getTypeInterest() == TypeInterestEnum.DEBIT) break;
		}
		
		List<InterestTierDto> tier = new ArrayList<InterestTierDto>();
		for (CoreInterestParameterDetail detail : coreInterestParameterManager.getDetail(selectedInterest.getId())) {
			InterestTier interestTier = new InterestTier();
			interestTier.setTier(detail.getLevelmin(), detail.getLevelmax(), detail.getSpreadRate());
			tier.add(new InterestTierDto(selectedInterest.getInterestBaseId().getRate(), interestTier));
		}
		
    	InterestTierSingleton interestTierSingleton = InterestTierSingleton.getInstance();
    	interestTierSingleton.setTier(tier);
		
    	int dayDiff = DateCalculatorUtil.getDifferenceDay(coreSystem.getTodayDate(), coreSystem.getNextDate());
    	int to  = selectedInterest.getCompound() && selectedInterest.getBalanceTypeForCalculation() == BalanceTypeEnum.DAILY ? 1 : dayDiff;
		int day = selectedInterest.getCompound() && selectedInterest.getBalanceTypeForCalculation() == BalanceTypeEnum.DAILY ? dayDiff : 1;
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(coreSystem.getTodayDate());
		
		InterestCalculator calculator = new InterestCalculator();
    	List<CustomerAccountDailyBalance> result = new ArrayList<CustomerAccountDailyBalance>();
    	
		boolean isTier = selectedInterest.getInterestType() == InterestTypeEnum.TIER ? true : false;
		int accday = total.size();
    	for (int j = 0; j < to; j++) {
    		CustomerAccountDailyBalance dailyBalance = new CustomerAccountDailyBalance();
    		dailyBalance.setTimeStamp(calendar.getTime());
    		dailyBalance.setCurrentBalance(item.getWorkingBalance());
    		if(j > 0) total.add(dailyBalance.getCurrentBalance());
    		dailyBalance.setAverageBalance(averageCalculator.calculate(total));
    		dailyBalance.setMinimumBalance(minimum);
    		dailyBalance.setAccrualDay(selectedInterest.getBalanceTypeForCalculation() != BalanceTypeEnum.DAILY ? (accday+j) : day);
    		dailyBalance.setCalculationType(selectedInterest.getBalanceTypeForCalculation());
    		dailyBalance.setCheckingType(selectedInterest.getBalanceTypeForChecking());
    		dailyBalance.setBranchName(item.getBranchOpen().getId());
    		dailyBalance.setCurrency(item.getCurrency().getId());
    		if(selectedInterest.getBalanceTypeForCalculation() == BalanceTypeEnum.AVERAGE) amount = dailyBalance.getAverageBalance();
    		if(selectedInterest.getBalanceTypeForChecking() == BalanceTypeEnum.AVERAGE) checkingAmount = dailyBalance.getAverageBalance();
    		dailyBalance.setAccrualBalance(amount);
    		dailyBalance.setPositiveInterestAccrued(calculator.calculate(amount.abs(), checkingAmount.abs(), dailyBalance.getAccrualDay(), selectedInterest.getBasisDay().getDivider(), isTier, same).setScale(2, RoundingMode.HALF_UP));
        	dailyBalance.setAccount(item);
        	dailyBalance.setInterestType(selectedInterest.getInterestType());
        	dailyBalance.setTypeInterest(selectedInterest.getTypeInterest());
        	dailyBalance.setIsCompound(selectedInterest.getCompound());
        	result.add(dailyBalance);
        	
        	calendar.add(Calendar.DATE, 1);
		}
		
    	return result;
	}
}
