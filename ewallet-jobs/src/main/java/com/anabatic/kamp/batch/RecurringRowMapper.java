package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.enumeration.TransactionStatusEnum;
import com.anabatic.kamp.persistence.model.TrxRecurring;

/**
 * 
 * @version 1.0, Sep 16, 2015 1:44:56 AM
 */
public class RecurringRowMapper implements RowMapper<TrxRecurring> {

	@Override
	public TrxRecurring mapRow(ResultSet rs, int rowNum) throws SQLException {
		TrxRecurring recurring = new TrxRecurring();
		recurring.setId(rs.getLong("id"));
		recurring.setAccountTo(rs.getString("account_to"));
		recurring.setAccountFrom(rs.getString("account_from"));
		recurring.setCcyAccountTo(rs.getString("ccy_account_to"));
		recurring.setCcyAccountFrom(rs.getString("ccy_account_from"));
	/*	recurring.setBranchAccountTo(rs.getString("branch_account_to"));
		recurring.setBranchAccountFrom(rs.getString("branch_account_from"));*/
		recurring.setAmount(rs.getBigDecimal("amount"));
		recurring.setPostingFrequency(rs.getString("posting_frequency"));
		recurring.setPostingDateTrx(rs.getInt("posting_date_trx"));
		recurring.setValidityDateFrom(rs.getDate("validity_date_from"));
		recurring.setValidityDateTo(rs.getDate("validity_date_to"));
		recurring.setTrxStatus(TransactionStatusEnum.get(rs.getString("trx_status")));
		return recurring;
	}

}
