package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.batch.dto.CapitalizeChargeDto;


public class CapitalizeChargeRowMapper implements RowMapper<CapitalizeChargeDto> {

	@Override
	public CapitalizeChargeDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CapitalizeChargeDto dto = new CapitalizeChargeDto();
		dto.setAccountId(rs.getLong("account_id"));
		dto.setCodeAccount(rs.getString("code_account"));
		dto.setBalance(rs.getBigDecimal("online_balance"));
		dto.setChargeCode(rs.getLong("charge_code"));
		dto.setChargeAmount(rs.getBigDecimal("charge_amount"));
		dto.setChargeId(rs.getString("charge_id"));
		dto.setCreditAccount(rs.getString("credit_account"));
		dto.setDebitAccount(rs.getString("debet_account"));
		return dto;
	}

}
