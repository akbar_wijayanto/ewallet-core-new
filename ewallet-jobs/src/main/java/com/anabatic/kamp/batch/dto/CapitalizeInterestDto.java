/**
 * 
 */
package com.anabatic.kamp.batch.dto;

import java.math.BigDecimal;

import com.anabatic.kamp.enumeration.BalanceTypeEnum;
import com.anabatic.kamp.enumeration.TypeInterestEnum;

/**
 * 
 * @version 1.0, Aug 18, 2015 4:21:24 AM
 */
public class CapitalizeInterestDto {
	private Long id;
	private TypeInterestEnum typeInterest;
	private BalanceTypeEnum calculationType;
	private BigDecimal capitalizeAmount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public TypeInterestEnum getTypeInterest() {
		return typeInterest;
	}
	public void setTypeInterest(TypeInterestEnum typeInterest) {
		this.typeInterest = typeInterest;
	}
	public BalanceTypeEnum getCalculationType() {
		return calculationType;
	}
	public void setCalculationType(BalanceTypeEnum calculationType) {
		this.calculationType = calculationType;
	}
	public BigDecimal getCapitalizeAmount() {
		return capitalizeAmount;
	}
	public void setCapitalizeAmount(BigDecimal capitalizeAmount) {
		this.capitalizeAmount = capitalizeAmount;
	}
}
