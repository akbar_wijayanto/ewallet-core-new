package com.anabatic.kamp.batch.listener;

import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.service.CoreSystemManager;
import com.anabatic.kamp.service.CustomerAccountHistoryBalanceManager;


public class HistoryBalanceStepListener implements StepExecutionListener{
    protected final Log log = LogFactory.getLog(HistoryBalanceStepListener.class);
	
    @Autowired
	private CoreSystemManager coreSystemManager;
	
    @Autowired
    private CustomerAccountHistoryBalanceManager customerAccountHistoryBalanceManager;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
		Integer period = Integer.valueOf(df.format(coreSystemManager.getCurrentSystem().getTodayDate()));
		customerAccountHistoryBalanceManager.generateDailyHistoryBalance(period);
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
	}

}
