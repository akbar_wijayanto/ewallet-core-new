package com.anabatic.kamp.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.anabatic.kamp.batch.dto.CapitalizeInterestDto;
import com.anabatic.kamp.enumeration.TypeInterestEnum;


/**
 * 
 * @version 1.0, Aug 18, 2015 4:09:20 AM
 */
public class CapitalizeInterestRowMapper implements RowMapper<CapitalizeInterestDto> {

	@Override
	public CapitalizeInterestDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CapitalizeInterestDto dto = new CapitalizeInterestDto();
		dto.setId(rs.getLong("account_id"));
		dto.setCapitalizeAmount(rs.getBigDecimal("capitalize_amount"));
		dto.setTypeInterest(TypeInterestEnum.get(rs.getString("type_interest")));
		return dto;
	}

}
