package com.anabatic.kamp.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.anabatic.kamp.persistence.model.TrxRecord;
import com.anabatic.kamp.service.TrxRecordManager;
import com.anabatic.kamp.service.TrxReversalManager;

public class ReverseJournalWriter implements ItemWriter<String> {

	@Autowired
	private TrxReversalManager trxReversalManager;
	
	@Autowired
	private TrxRecordManager trxRecordManager;
	
	@Override
	public void write(final List<? extends String> items) throws Exception {
		if(items.size()>0) {
			for (String item : items) {
				TrxRecord record = trxRecordManager.getByTransactionReference(item);
				if(record != null) {
					trxReversalManager.reverseTransaction(record, false);
				}
			}
		}
	}
}
