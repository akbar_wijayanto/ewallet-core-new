/**
 * 
 */
package com.anabatic.kamp.batch;

import org.springframework.batch.item.ItemProcessor;

import com.anabatic.kamp.persistence.model.CoreReport;

/**
 * 
 * @version 1.0, Oct 6, 2015 11:00:07 AM
 */
public class ReportGeneratorProcessor implements ItemProcessor<CoreReport, CoreReport> {

	@Override
	public CoreReport process(CoreReport item) throws Exception {
		return item;
	}

}
