package com.ewallet.factory.dao;

import com.ewallet.persistence.dao.GenericDao;

public interface DaoFactory {
	
	@SuppressWarnings("rawtypes")
	GenericDao getDao(String className);
	
}
