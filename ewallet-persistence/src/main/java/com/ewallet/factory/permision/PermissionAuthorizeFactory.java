package com.ewallet.factory.permision;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

public class PermissionAuthorizeFactory implements ClassFactory {

	@Override
	public Boolean checkPermission(
			String className,
			SecurityContextHolderAwareRequestWrapper securityContextHolderAwareRequestWrapper) {

	  if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:ACTIVITY:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreActivity")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:BRANCH:AUTHORIZE:*")
				&& (className.equalsIgnoreCase("corebranch") || className.equalsIgnoreCase("corebrachtype"))) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CALENDAR:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecalendar")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CATEGORY:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreCategory")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CHARGE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecharge")) {
			return true;
		}  else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:COMPANY:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecompany")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CURRENCY:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecurrency")) {
			return true;
		}  else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CURRENCYRATE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecurrencyrate")) {
			return true;
		}  else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:INTERESTBASIS:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coreinterestbasis")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:MENU:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coremenu")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:OVERDUE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coreoverdue")) {
			return true;
		}  else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:SYSTEM:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corepermission")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:REPORT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corereport")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:SYSTEM:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coresystem")) {
			return true;
		}  else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:USER:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coreuser")) {
			return true;
		} 
		/*TODO: ADD PERMISSION BY NANA*/
		else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:ACCOUNTOFFICER:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreAccountOfficer")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:DEPARTMENT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreDepartment")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:TRANSACTIONCODE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("TransactionCode")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:DENOMINATION:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreDenomination")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:VAULT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreVault")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:TELLERACCOUNT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreTellerAccount")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:APPLICATION:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreApplication")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:LEVEL:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreLevel")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:INTERESTBASE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreBaseInterestParameter")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:INTERESTPARAMETER:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreInterestParameter")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:PRODUCT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("Product")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:TAX:AUTHORIZE:*")
				&& className.equalsIgnoreCase("Product")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:ACCOUNTING:AUTHORIZE:*")
				&& className.equalsIgnoreCase("CoreAccounting")) {
			return true;
		}
		  
		/*TODO: All comment by Nana*/
		/*else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:COLLATERALCODE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecollateralcode")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:COLLATERALTYPE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecollateraltype")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:COLLECTIBILITY:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecollectibility")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:CURRENCYMARKET:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corecurrencymarket")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:FINANCINGPRODUCT:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corefinancingproduct")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:LIMITPARAMETER:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corelimitparameter")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:LIMITREFERENCE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corelimitreference")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:LOAN:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coreloan")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:LANGUAGE:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corelanguage")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:PERIODICINTEREST:AUTHORIZE:*")
				&& className.equalsIgnoreCase("coreperiodicinterest")) {
			return true;
		} else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:SCORING:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corescoring")) {
			return true;
		}else if (securityContextHolderAwareRequestWrapper
				.isUserInRole("CORE:WRITEOFF:AUTHORIZE:*")
				&& className.equalsIgnoreCase("corewriteoff")) {
			return true;
		}
		
		*/
		
		else {
			return false;
		}
	}

}
