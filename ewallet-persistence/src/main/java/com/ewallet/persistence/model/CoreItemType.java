package com.ewallet.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto 
 * Date Oct 16, 2015 2:26:29 PM
 */
@Entity
@Table(name = "core_item_type")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_item_type set status='HIST' where id=?")
public class CoreItemType extends BaseGenericObject {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private Long id;
	private String name;

	@Column(name = "id")
    @GeneratedValue
    @Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreItemType other = (CoreItemType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CoreProductType [id=" + id + ", name=" + name + "]";
	}

}