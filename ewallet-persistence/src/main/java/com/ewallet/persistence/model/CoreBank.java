/**
 * 
 */
package com.ewallet.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 30, 2015 1:40:24 PM
 */
@Entity
@Table(name = "core_bank")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_bank set status='HIST' where id=?")
public class CoreBank extends BaseGenericObject {

	private static final long serialVersionUID = -5015617880405251922L;
	
	private String code;
	private String name;
//	private String bankType;
	
	@Id
	@Column(name="code", nullable=false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name="name", nullable=false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	@Column(name="bank_type", nullable=false)
//	public String getBankType() {
//		return bankType;
//	}
//
//	public void setBankType(String bankType) {
//		this.bankType = bankType;
//	}

	@Override
	public String toString() {
		return "CoreBank [code=" + code + ", name=" + name + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreBank other = (CoreBank) obj;
//		if (bankType == null) {
//			if (other.bankType != null)
//				return false;
//		} else if (!bankType.equals(other.bankType))
//			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result
//				+ ((bankType == null) ? 0 : bankType.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

}
