package com.ewallet.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 22, 2015 10:36:31 AM
 */
@Entity
@Table(name = "trx_topup")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update trx_topup set status='HIST' where id=?")
public class TrxTopup extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private Long id;
	private String topupId;
	private Date topupDate;
	private BigDecimal amount;
	private CoreUser coreUser;

	@Column(name = "id")
    @GeneratedValue
    @Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}

	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}

	@Column(name = "topup_id", unique = true)
	public String getTopupId() {
		return topupId;
	}

	public void setTopupId(String topupId) {
		this.topupId = topupId;
	}

	@Column(name = "topup_date")
	public Date getTopupDate() {
		return topupDate;
	}

	public void setTopupDate(Date topupDate) {
		this.topupDate = topupDate;
	}

	@Column(name = "amount")
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((topupDate == null) ? 0 : topupDate.hashCode());
		result = prime * result + ((topupId == null) ? 0 : topupId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxTopup other = (TrxTopup) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (topupDate == null) {
			if (other.topupDate != null)
				return false;
		} else if (!topupDate.equals(other.topupDate))
			return false;
		if (topupId == null) {
			if (other.topupId != null)
				return false;
		} else if (!topupId.equals(other.topupId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxTopup [id=" + id + ", topupId=" + topupId + ", topupDate="
				+ topupDate + ", amount=" + amount + ", coreUser=" + coreUser
				+ "]";
	}
	
}