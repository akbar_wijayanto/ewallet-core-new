package com.ewallet.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 2:26:29 PM
 */
@Entity
@Table(name = "core_item")
//@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update core_item set status='HIST' where id=?")
public class CoreItem extends BaseGenericObject {
	
	private static final long serialVersionUID = 4165661136334067426L;
	private Long id;
	private String itemId;
	private String itemName;
	private CoreItemType coreItemType;
	private BigDecimal costPrice;
	private BigDecimal sellingPrice;
	private Date entryDate;
	private Date expiredDate;
	private String description;
	private CoreSeller coreSeller;
	private String filePath;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotEmpty
	@Column(name = "item_id", nullable = false, insertable = true, updatable = true)
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@NotEmpty
	@Column(name = "item_name")
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToOne
	@JoinColumn(name = "item_type_id")
	public CoreItemType getCoreItemType() {
		return coreItemType;
	}

	public void setCoreItemType(CoreItemType coreItemType) {
		this.coreItemType = coreItemType;
	}
	
	@Column(name = "cost_price")
	public BigDecimal getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}

	@Column(name = "selling_price")
	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	
	@Column(name = "entry_date")
	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	@Column(name = "expired_date")
	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}

	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}

	@Column(name = "file_path")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coreItemType == null) ? 0 : coreItemType.hashCode());
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((costPrice == null) ? 0 : costPrice.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((entryDate == null) ? 0 : entryDate.hashCode());
		result = prime * result
				+ ((expiredDate == null) ? 0 : expiredDate.hashCode());
		result = prime * result
				+ ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		result = prime * result
				+ ((itemName == null) ? 0 : itemName.hashCode());
		result = prime * result
				+ ((sellingPrice == null) ? 0 : sellingPrice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoreItem other = (CoreItem) obj;
		if (coreItemType == null) {
			if (other.coreItemType != null)
				return false;
		} else if (!coreItemType.equals(other.coreItemType))
			return false;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (costPrice == null) {
			if (other.costPrice != null)
				return false;
		} else if (!costPrice.equals(other.costPrice))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (entryDate == null) {
			if (other.entryDate != null)
				return false;
		} else if (!entryDate.equals(other.entryDate))
			return false;
		if (expiredDate == null) {
			if (other.expiredDate != null)
				return false;
		} else if (!expiredDate.equals(other.expiredDate))
			return false;
		if (filePath == null) {
			if (other.filePath != null)
				return false;
		} else if (!filePath.equals(other.filePath))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		if (sellingPrice == null) {
			if (other.sellingPrice != null)
				return false;
		} else if (!sellingPrice.equals(other.sellingPrice))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CoreItem [id=" + id + ", itemId=" + itemId + ", itemName="
				+ itemName + ", coreItemType=" + coreItemType + ", costPrice="
				+ costPrice + ", sellingPrice=" + sellingPrice + ", entryDate="
				+ entryDate + ", expiredDate=" + expiredDate + ", description="
				+ description + ", coreSeller=" + coreSeller + ", filePath="
				+ filePath + "]";
	}

}