package com.ewallet.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Dec 14, 2015 5:50:37 PM
 */
@Entity
@Table(name = "trx_seller_payment")
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update trx_seller_payment set status='HIST' where id=?")
public class TrxSellerPayment extends BaseTransaction {
	
	private static final long serialVersionUID = 4165661136334067426L;
//	private Long id;
	private String paymentId;
	private Date paymentDate;
	private BigDecimal grandTotal;
	private BigDecimal totalTransaction;
	private BigDecimal percentage;
	private BigDecimal charge;
	private BigDecimal totalAmount;
	private CoreSeller coreSeller;
	
	private String startDat;
	private String endDat;
	
	

//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
	@Column(name = "payment_id", unique = true)
    @Id
	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	@Column(name = "payment_date")
	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Column(name = "total_amount")
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@ManyToOne
	@JoinColumn(name = "seller_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}

	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}

	@Column(name = "grand_total")
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	@Column(name = "total_transaction")
	public BigDecimal getTotalTransaction() {
		return totalTransaction;
	}

	public void setTotalTransaction(BigDecimal totalTransaction) {
		this.totalTransaction = totalTransaction;
	}

	@Column(name = "charge")
	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	@Column(name = "percentage")
	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	
	public String getStartDat() {
		return startDat;
	}

	public void setStartDat(String startDat) {
		this.startDat = startDat;
	}

	public String getEndDat() {
		return endDat;
	}

	public void setEndDat(String endDat) {
		this.endDat = endDat;
	}
	

	@Override
	public String toString() {
		return "TrxSellerPayment [paymentId=" + paymentId + ", paymentDate="
				+ paymentDate + ", grandTotal=" + grandTotal
				+ ", totalTransaction=" + totalTransaction + ", percentage="
				+ percentage + ", charge=" + charge + ", totalAmount="
				+ totalAmount + ", coreSeller=" + coreSeller + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((charge == null) ? 0 : charge.hashCode());
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((grandTotal == null) ? 0 : grandTotal.hashCode());
		result = prime * result
				+ ((paymentDate == null) ? 0 : paymentDate.hashCode());
		result = prime * result
				+ ((paymentId == null) ? 0 : paymentId.hashCode());
		result = prime * result
				+ ((percentage == null) ? 0 : percentage.hashCode());
		result = prime * result
				+ ((totalAmount == null) ? 0 : totalAmount.hashCode());
		result = prime
				* result
				+ ((totalTransaction == null) ? 0 : totalTransaction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxSellerPayment other = (TrxSellerPayment) obj;
		if (charge == null) {
			if (other.charge != null)
				return false;
		} else if (!charge.equals(other.charge))
			return false;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (grandTotal == null) {
			if (other.grandTotal != null)
				return false;
		} else if (!grandTotal.equals(other.grandTotal))
			return false;
		if (paymentDate == null) {
			if (other.paymentDate != null)
				return false;
		} else if (!paymentDate.equals(other.paymentDate))
			return false;
		if (paymentId == null) {
			if (other.paymentId != null)
				return false;
		} else if (!paymentId.equals(other.paymentId))
			return false;
		if (percentage == null) {
			if (other.percentage != null)
				return false;
		} else if (!percentage.equals(other.percentage))
			return false;
		if (totalAmount == null) {
			if (other.totalAmount != null)
				return false;
		} else if (!totalAmount.equals(other.totalAmount))
			return false;
		if (totalTransaction == null) {
			if (other.totalTransaction != null)
				return false;
		} else if (!totalTransaction.equals(other.totalTransaction))
			return false;
		return true;
	}

}