package com.ewallet.persistence.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:06:56 PM
 */
@Embeddable
public class TrxOrderItemId implements Serializable{

	private static final long serialVersionUID = 5896744623450759962L;
	
	private TrxOrder trxOrder;
	private CoreItem coreItem;
	
	@ManyToOne
	@JsonIgnore
	public CoreItem getCoreItem() {
		return coreItem;
	}
	public void setCoreItem(CoreItem coreItem) {
		this.coreItem = coreItem;
	}
	
	@ManyToOne
	@JsonIgnore
	public TrxOrder getTrxOrder() {
		return trxOrder;
	}
	public void setTrxOrder(TrxOrder trxOrder) {
		this.trxOrder = trxOrder;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coreItem == null) ? 0 : coreItem.hashCode());
		result = prime * result
				+ ((trxOrder == null) ? 0 : trxOrder.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxOrderItemId other = (TrxOrderItemId) obj;
		if (coreItem == null) {
			if (other.coreItem != null)
				return false;
		} else if (!coreItem.equals(other.coreItem))
			return false;
		if (trxOrder == null) {
			if (other.trxOrder != null)
				return false;
		} else if (!trxOrder.equals(other.trxOrder))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "TrxOrderItemId [trxOrder=" + trxOrder + ", coreItem="
				+ coreItem + "]";
	}
	
}
