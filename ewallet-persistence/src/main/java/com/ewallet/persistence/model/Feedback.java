package com.ewallet.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 2:50:17 PM
 */
@Table(name = "feedback")
@Entity
@Where(clause = " status <> 'HIST' OR  status is null ")
@SQLDelete(sql = "update feedback set status='HIST' where id=?")
public class Feedback extends BaseGenericObject {
	
	private static final long serialVersionUID = 3832626162173359411L;
	
	private Long id;
	private String email;
	private String phoneNumber;
	private String message;
	private Integer rating;
	private String management;
	private CoreUser coreUser;
	private CoreSeller coreSeller;
	
	@Column(name = "id")
    @GeneratedValue
    @Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name = "message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name = "rating")
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	public CoreUser getCoreUser() {
		return coreUser;
	}
	public void setCoreUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
	
	@Column(name = "management")
	public String getManagement() {
		return management;
	}
	public void setManagement(String management) {
		this.management = management;
	}
	
	@ManyToOne
	@JoinColumn(name = "seller_id")
	public CoreSeller getCoreSeller() {
		return coreSeller;
	}
	public void setCoreSeller(CoreSeller coreSeller) {
		this.coreSeller = coreSeller;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coreSeller == null) ? 0 : coreSeller.hashCode());
		result = prime * result
				+ ((coreUser == null) ? 0 : coreUser.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((management == null) ? 0 : management.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feedback other = (Feedback) obj;
		if (coreSeller == null) {
			if (other.coreSeller != null)
				return false;
		} else if (!coreSeller.equals(other.coreSeller))
			return false;
		if (coreUser == null) {
			if (other.coreUser != null)
				return false;
		} else if (!coreUser.equals(other.coreUser))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (management == null) {
			if (other.management != null)
				return false;
		} else if (!management.equals(other.management))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Feedback [id=" + id + ", email=" + email + ", phoneNumber="
				+ phoneNumber + ", message=" + message + ", rating=" + rating
				+ ", management=" + management + ", coreUser=" + coreUser
				+ ", coreSeller=" + coreSeller + "]";
	}
	
}
