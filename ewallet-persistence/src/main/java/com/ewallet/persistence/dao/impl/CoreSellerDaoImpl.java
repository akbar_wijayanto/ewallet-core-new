package com.ewallet.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.persistence.dao.CoreSellerDao;
import com.ewallet.persistence.model.CoreSeller;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:01:43 PM
 */
@Repository
public class CoreSellerDaoImpl extends GenericDaoImpl<CoreSeller, Long> implements CoreSellerDao {

	public CoreSellerDaoImpl() {
		super(CoreSeller.class);
	}

	@Override
	public CoreSeller getByQRCode(String qrcode) {
		String sql = "from CoreUser u WHERE u.class="+CoreSeller.class.getName()+" AND u.sellerId=:qrcode";
		try {
			return (CoreSeller) getEntityManager()
					.createQuery(sql)
					.setParameter("qrcode", qrcode)
					.getSingleResult();
		} catch (Exception e) {return null;}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CoreSeller> getAllAuthSeller() {
		// TODO Auto-generated method stub
		//String sql = "from CoreUser u WHERE u.status=:status or u.status:=status2";
		String sql = "from CoreUser u WHERE u.status in (:status,:status2) ";
		try {
			return (List<CoreSeller>) getEntityManager()
					.createQuery(sql)
					.setParameter("status", RecordStatusEnum.INAU.getName())
					.setParameter("status2", "INAUACT")
					.getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public List<CoreSeller> getInactiveSellerList() {
		String sql = "from CoreUser u WHERE u.status=:status ";
		try {
			return (List<CoreSeller>) getEntityManager()
					.createQuery(sql)
					.setParameter("status", RecordStatusEnum.INACT.getName())
					.getResultList();
		} catch (Exception e) {return null;}
	}
	
}
