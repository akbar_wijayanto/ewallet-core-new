package com.ewallet.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.TrxSellerPayment;

/**
 * @author akbar.wijayanto
 * Date Dec 31, 2015 3:20:39 PM
 */
public interface TrxSellerPaymentDao extends GenericTransactionDao<TrxSellerPayment, String> {

	List<TrxSellerPayment> getAllSellerPayment();
	TrxSellerPayment getTrxSellerPaymentByPaymentId(String paymentId);

	public List<TrxSellerPayment> getSettlementBySellerId(String sellerid);
	
	TrxSellerPayment getBySellerIdAndPaidDate(String sellerId, Date paidDate);
	
	List<TrxSellerPayment> getBySellerIdAndDate(String sellerId, Date startDate, Date endDate);
	List<TrxSellerPayment> getAllSellerPaymentPaid();
	
	List<TrxSellerPayment> getAllSellerPaymentReport(String startDate, String endDate);
}
