package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.TrxOrderItemDetailDao;
import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:25:55 PM
 */
@Repository
public class TrxOrderItemDetailDaoImpl extends GenericTransactionDaoImpl<TrxOrderItemDetail, TrxOrderItemId> implements TrxOrderItemDetailDao {

	public TrxOrderItemDetailDaoImpl() {
		super(TrxOrderItemDetail.class);
	}

}
