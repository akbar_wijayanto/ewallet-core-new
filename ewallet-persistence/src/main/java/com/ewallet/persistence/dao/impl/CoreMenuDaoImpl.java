package com.ewallet.persistence.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.persistence.dao.CoreMenuDao;
import com.ewallet.persistence.model.CoreMenu;

@Repository("coreMenuDao")
public class CoreMenuDaoImpl extends GenericDaoImpl<CoreMenu, Integer> implements CoreMenuDao {

    public CoreMenuDaoImpl() {
        super(CoreMenu.class);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreMenu> getByUsername(String username) {
        String sql = "select distinct c from CoreUser a " +
                "join a.coreRoles b " +
                "join b.coreMenus c " +
                "where a.username = :username and c.status='LIVE' order by c.orderMenu asc";
        List<CoreMenu> menus = getEntityManager().createQuery(sql).setParameter("username", username).getResultList();
        return menus;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreMenu> getByOrderMenu(Integer parentId, Integer orderMenu) {
    	return getEntityManager()
                .createQuery("From CoreMenu c where c.parentId=:parentId and orderMenu=:ORDERMENU")
                .setParameter("ORDERMENU", orderMenu)
                .setParameter("parentId", parentId)
                .getResultList();
    }

	@Override
	public Integer getByPermaLink(String link) {
		try {
			Integer id = (Integer) getEntityManager()
					.createQuery("Select parentId from CoreMenu where permalinks=:a")
					.setParameter("a", link)
					.getSingleResult();
				return id;
		} catch (NoResultException nre) {
			return 0;
		}
		
	}
	
	@Override
	public CoreMenu getByParentId(Integer parentId) {
		
		CoreMenu coreMenu = (CoreMenu) getEntityManager()
					.createQuery("FROM CoreMenu WHERE id=:parentId")
					.setParameter("parentId", parentId)
					.getSingleResult();
		
		return coreMenu;
	}

	@Override
	public List<CoreMenu> getByActiveRole(Integer roleId) {
		String sql = "select distinct b from CoreRole a " +
                "join a.coreMenus b " +
                "where a.id = :roleId and b.status='LIVE' order by b.orderMenu asc";
        List<CoreMenu> menus = getEntityManager().createQuery(sql).setParameter("roleId", roleId).getResultList();
        return menus;
	}

	/*@Override
	@Transactional
	public CoreMenu save(CoreMenu object) {
		
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		
		super.remove(id);
	}

	@Override
	@Transactional
	public CoreMenu authorize(CoreMenu oldObject, CoreMenu newObject,
			boolean isApproved) {
		
		return super.authorize(oldObject, newObject, isApproved);
	}*/

}
