/**
 * 
 */
package com.ewallet.persistence.dao;

import java.io.Serializable;

import com.ewallet.persistence.model.BaseTransaction;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:12:02 PM
 * @param <T>
 * @param <PK>
 */
public interface GenericTransactionDao<T extends BaseTransaction, PK extends Serializable>
		extends GenericDao<T, PK> {
	
	public T getByTransactionReference(String transactionReference);

}
