package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.TrxOrderItemDetail;
import com.ewallet.persistence.model.TrxOrderItemId;

/**
 * @author akbar.wijayanto
 * Date Nov 25, 2015 5:23:05 PM
 */
public interface TrxOrderItemDetailDao extends GenericTransactionDao<TrxOrderItemDetail, TrxOrderItemId>{

}
