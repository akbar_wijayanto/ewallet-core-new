package com.ewallet.persistence.dao.impl;

import java.util.List;

import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.Constants;
import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.persistence.dao.CoreUserDao;
import com.ewallet.persistence.model.CoreUser;

/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve User objects.
 *
 * @author <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 */
@Repository("coreUserDao")
public class CoreUserDaoImpl extends GenericDaoImpl<CoreUser, Long> implements CoreUserDao {
    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    /**
     * Constructor that sets the entity to User.class.
     */
    public CoreUserDaoImpl() {
        super(CoreUser.class);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<CoreUser> getUsers() {
        return getEntityManager().createQuery("from CoreUser u order by upper(u.username)").getResultList();
    }

    /**
     * {@inheritDoc}
     */
    public CoreUser saveUser(CoreUser coreUser) {
        CoreUser u = super.save(coreUser);
        getEntityManager().flush();
        return u;
    }

    /**
     * Overridden simply to call the saveUser method. This is happenening 
     * because saveUser flushes the session and saveObject of BaseDaoHibernate 
     * does not.
     *
     * @param coreUser the user to save
     * @return the modified user (with a primary key set if they're new)
     */
//    @Override
//    public CoreUser save(CoreUser coreUser) {
//        return this.saveUser(coreUser);
//    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("rawtypes")
	public CoreUser loadUserByUsername(String username) {
        List users = getEntityManager().createQuery("from CoreUser u "
//                + "left outer join fetch u.coreBranches b "
                + "left outer join fetch u.coreRoles r "
                + "where username=:username")
                .setParameter("username", username)
                .getResultList();
        if (users == null || users.isEmpty()) {
            return null;
        } else {
            return (CoreUser) users.get(0);
        }
    }

    /**
     * {@inheritDoc}
     */
    public String getUserPassword(String username) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Table table = AnnotationUtils.findAnnotation(CoreUser.class, Table.class);
        return jdbcTemplate.queryForObject(
                "select password from " + table.name() + " where username=?", String.class, username);

    }

    @SuppressWarnings("unchecked")
	@Override
    public CoreUser getWithListAll(Long id) {
        String sql = "select u from CoreUser u left outer join fetch u.corePermissions p "
//                + "left outer join fetch u.coreBranches b "
                + "left outer join fetch u.coreRoles r "
                + "where u.id=:ID";
        List<CoreUser> listCoreUsers = getEntityManager()
                .createQuery(sql)
                .setParameter("ID", id)
                .getResultList();

        return listCoreUsers.size() == 0 ? null : listCoreUsers.get(0);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getBranchById(Long id) {

        return getEntityManager()
                .createQuery("select distinct c,d from CoreUser c join fetch c.coreBranches d where c.id=:id")
                .setParameter("id", id)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getCheckBranchOnUser(String id) {
        return getEntityManager()
                .createQuery("from CoreUser u  join fetch u.coreBranches b where b.id=:branchId")
                .setParameter("branchId", id)
                .getResultList();


    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getCheckRoleUsedByUser(Integer id) {
        return getEntityManager()
                .createQuery("from CoreUser u join fetch u.coreRoles b where b.id=:roleId")
                .setParameter("roleId", id)
                .getResultList();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getCheckPermissionUsedByUser(Integer id) {
        List<CoreUser> list = getEntityManager()
                .createQuery("from CoreUser u join fetch u.corePermissions b where b.id=:permissionId")
                .setParameter("permissionId", id)
                .getResultList();
        System.out.println(list);
        return list;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getUserRoleList(Integer id) {
        return getEntityManager()
                .createQuery("from CoreUser u join fetch u.coreRoles b where b.id=:roleId")
                .setParameter("roleId", id)
                .getResultList();
    }


    @Override
    public Long getRecordCount(Integer id) {

        return Long.valueOf(getUserRoleList(id).size());
    }


    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getSearchResult(Integer id, Long firstResult, Long maxResults) {


        List<CoreUser> user = getEntityManager()
                .createQuery("from CoreUser u join fetch u.coreRoles b where b.id=:roleId")
                .setParameter("roleId", id)
                .setFirstResult(NumberUtils.toInt(String.valueOf(firstResult)))
                .setMaxResults(NumberUtils.toInt(String.valueOf(maxResults), Constants.PAGING_MAX_RECORD))
                .getResultList();

        return user;
    }

    @Override
    @Transactional
    public CoreUser updateLoginAttempt(CoreUser coreUser) {
        CoreUser u = super.save(coreUser);
        getEntityManager().flush();
        return u;

    }

    @SuppressWarnings("unchecked")
	@Override
    public List<CoreUser> getAllOrderByUsername() {
        return getEntityManager().createQuery("from CoreUser order by username asc").getResultList();
    }

    @Override
    public CoreUser saveCurrentUser(CoreUser object) {
        if (StringUtils.isEmpty(object.getStatus())) {
            object.setStatus(RecordStatusEnum.LIVE.name());
        }

        if (object.getVersion() == null) {
            object.setVersion(0);
        }
        CoreUser u = getEntityManager().merge(object);
        getEntityManager().flush();
        return u;
    }

    @Override
    public List<CoreUser> getPagingResults(Long firstResult, Long maxResults) {
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CoreUser> cq = qb.createQuery(CoreUser.class);
        Root<CoreUser> cc = cq.from(CoreUser.class);
        cq.select(cc);
        cq.orderBy(qb.asc(cc.get("username")));
        return getEntityManager()
                .createQuery(cq)
                .setFirstResult(NumberUtils.toInt(String.valueOf(firstResult)))
                .setMaxResults(NumberUtils.toInt(String.valueOf(maxResults), Constants.PAGING_MAX_RECORD))
                .getResultList();
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<CoreUser> getAllWithListAll() {
		List<CoreUser> users = getEntityManager().createQuery("from CoreUser u left outer join fetch u.corePermissions p "
                + "left outer join fetch u.coreBranches b "
                + "left outer join fetch u.coreRoles r ")
                .getResultList();
		return users;
	}

    @SuppressWarnings("unchecked")
	@Override
    public CoreUser getUserSpring(String username) {
    	
        List<CoreUser> list = getEntityManager().createQuery("from CoreUser u where u.username = :login")
                .setParameter("login", username).getResultList();
        if (list.size() > 0)
            return list.get(0);  
        else  
            return null;   
    }

	/*@Override
	@Transactional
	public CoreUser save(CoreUser object) {
		
		return super.save(object);
	}

	@Override
	@Transactional
	public void remove(Long id) {
		
		super.remove(id);
	}

	@Override
	@Transactional
	public CoreUser authorize(CoreUser oldObject, CoreUser newObject,
			boolean isApproved) {
		
		return super.authorize(oldObject, newObject, isApproved);
	}*/

	@Override
	@Transactional
	public CoreUser change(CoreUser object) {
		if (StringUtils.isEmpty(object.getStatus())) {
			object.setStatus(RecordStatusEnum.LIVE.name());
		}

		if (object.getVersion() == null) {
			object.setVersion(0);
		}
		return this.getEntityManager().merge(object);
	}

	@Override
	public List<CoreUser> getByClientUser() {
		String sql = "from CoreUser u where u.customerCif.id is not null";
		try {
			return getEntityManager().createQuery(sql).getResultList();
		} catch (Exception e) {return null;}
	}

	@Override
	public CoreUser getByToken(String token) {
		String sql = "from CoreUser u where u.token=:token";
		try {
			return (CoreUser) getEntityManager()
					.createQuery(sql)
					.setParameter("token", token)
					.getSingleResult();
		} catch (Exception e) {return null;}
	}

	@Override
	public void setHistoryToSeller(Long id) {
		String sql = "update from CoreUser set status='HIST' where id:=id";
		try {
			getEntityManager()
					.createQuery(sql)
					.setParameter("id", id)
					.getSingleResult();
		} catch (Exception e) {}
	}
	
}
