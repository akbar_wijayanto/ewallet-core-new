package com.ewallet.persistence.dao;

import java.util.List;

import com.ewallet.persistence.model.CoreItem;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:47:19 PM
 */
public interface CoreItemDao extends GenericDao<CoreItem, Long> {
	
	CoreItem getByItemId(String itemId);
	
	List<CoreItem> getBySellerId(String sellerId);
	
	List<CoreItem> getAllAuthItem();

}
