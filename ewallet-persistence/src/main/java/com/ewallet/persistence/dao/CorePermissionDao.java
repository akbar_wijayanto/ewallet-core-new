package com.ewallet.persistence.dao;

import java.util.List;

import com.ewallet.persistence.model.CorePermission;

public interface CorePermissionDao extends GenericDao<CorePermission, Integer> {
	List<String> getAplication();
	List<CorePermission> getByAplication(String applicationName);
}
