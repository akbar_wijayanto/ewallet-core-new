package com.ewallet.persistence.dao.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.VirtualAccountDao;
import com.ewallet.persistence.model.CoreUser;
import com.ewallet.persistence.model.TrxOrder;
import com.ewallet.persistence.model.VirtualAccount;

/**
 * @author akbar.wijayanto 
 * Date Oct 23, 2015 11:16:47 PM
 */
@Repository
public class VirtualAccountDaoImpl extends
		GenericTransactionDaoImpl<VirtualAccount, String> implements VirtualAccountDao {

	public VirtualAccountDaoImpl() {
		super(VirtualAccount.class);
	}

	@Override
	public VirtualAccount getByUser(CoreUser coreUser) {
		String sql = "from VirtualAccount o where o.coreUser=:coreUser";
		try {
			return (VirtualAccount) getEntityManager()
					.createQuery(sql)
					.setParameter("coreUser", coreUser)
					.getSingleResult();
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}

}
