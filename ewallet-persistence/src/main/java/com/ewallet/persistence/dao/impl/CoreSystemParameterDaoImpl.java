package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.CoreSystemParameterDao;
import com.ewallet.persistence.model.CoreSystemParameter;

@Repository("coreSystemParameterDao")
public class CoreSystemParameterDaoImpl extends
		GenericDaoImpl<CoreSystemParameter, String> implements
		CoreSystemParameterDao {

	public CoreSystemParameterDaoImpl() {
		super(CoreSystemParameter.class);
	}

}
