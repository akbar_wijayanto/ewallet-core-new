package com.ewallet.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Oct 23, 2015 11:16:09 PM
 */
public interface TrxRecordDao extends GenericTransactionDao<TrxRecord, Long> {

	List<TrxRecord> getBySellerId(String sellerId);
	List<TrxRecord> getByUsername(String username);
	
	// report
	List<TrxRecord> getSellerSettlementReport(String sellerId, String name, Date startDate, Date endDate);
}
