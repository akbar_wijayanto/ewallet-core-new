package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.TransactionDao;
import com.ewallet.persistence.model.TrxRecord;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:01:43 PM
 */
@Repository
public class TransactionDaoImpl extends GenericTransactionDaoImpl<TrxRecord, Long> implements TransactionDao {

	public TransactionDaoImpl() {
		super(TrxRecord.class);
	}
	
}
