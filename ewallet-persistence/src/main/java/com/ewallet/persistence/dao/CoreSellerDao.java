package com.ewallet.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.CoreSeller;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:00:37 PM
 */
public interface CoreSellerDao extends GenericDao<CoreSeller, Long> {

	CoreSeller getByQRCode(String qrcode);

	List<CoreSeller> getAllAuthSeller();
	
	List<CoreSeller> getInactiveSellerList();
	
}
