package com.ewallet.persistence.dao;

import java.util.Date;
import java.util.List;

import com.ewallet.persistence.model.TrxOrder;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:00:37 PM
 */
public interface TrxOrderDao extends GenericTransactionDao<TrxOrder, String> {

	List<TrxOrder> getOrdersByUser(String username);
	List<TrxOrder> getOrdersBySeller(String sellerId);
	TrxOrder getOrderByOrderId(String orderId);
	int checkValidOrder(String sellerId, String orderId);
	List<TrxOrder> getLast7DaysOrdersBySeller(String sellerId);
	List<TrxOrder> getLast7DaysOrders();
	List<TrxOrder> getLast7DaysOrdersBySellerUnpaid(String sellerId);
	List<TrxOrder> getLast7DaysOrdersByUser(String username);
	
	List<TrxOrder> getPeriodOrdersBySeller(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrders(String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersBySellerUnpaid(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersByUser(String username, String startDate, String endDate);
	List<TrxOrder> getPeriodicRefund(String startDate,String endDate);
	List<TrxOrder> getPeriodicOrder(String startDate,String endDate);
	
	List<TrxOrder> getPeriodicRefundBySellerid(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersBySellerId(String sellerId, String startDate, String endDate);
	List<TrxOrder> getPeriodOrdersByUserBuyerId(Long buyerId, String username, String startDate, String endDate);
	
	List<TrxOrder> getLast7DaysOrdersForSettlement();
	
	List<TrxOrder> getPeriodicOrderBySellerid(String sellerId, Date startDate, Date endDate);
	List<TrxOrder> getListOrdersForSettlement(String startDat, String endDat);
}
