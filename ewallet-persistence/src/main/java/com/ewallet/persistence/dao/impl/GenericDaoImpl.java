package com.ewallet.persistence.dao.impl;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;

import com.ewallet.Constants;
import com.ewallet.enumeration.RecordStatusEnum;
import com.ewallet.persistence.dao.GenericDao;
import com.ewallet.persistence.model.BaseGenericObject;
import com.ewallet.util.DateUtil;

/**
 * @param <T>
 *            a type variable
 * @param <PK>
 *            the primary key for that type
 * @author : <a href="mailto:if08007@gmail.com">Arni Sihombing</a>
 * @version : 1.0, 8/24/12, 2:09 PM
 */
public class GenericDaoImpl<T extends BaseGenericObject, PK extends Serializable>
		implements GenericDao<T, PK> {
	/**
	 * Log variable for all child classes. Uses LogFactory.getLog(getClass())
	 * from Commons Logging
	 */
	protected final Log log = LogFactory.getLog(getClass());

	public static final String PERSISTENCE_UNIT_NAME = "ApplicationEntityManager";

	/**
	 * Entity manager, injected by Spring using @PersistenceContext annotation
	 * on setEntityManager()
	 */
	@PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
	private EntityManager entityManager;
	private Class<T> persistentClass;

	/**
	 * Constructor that takes in a class to see which type of entity to persist.
	 * Use this constructor when subclassing or using dependency injection.
	 *
	 * @param persistentClass
	 *            the class type you'd like to persist
	 */
	public GenericDaoImpl(final Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	/**
	 * Constructor that takes in a class to see which type of entity to persist.
	 * Use this constructor when subclassing or using dependency injection.
	 *
	 * @param persistentClass
	 *            the class type you'd like to persist
	 * @param entityManager
	 *            the configured EntityManager for JPA implementation.
	 */
	public GenericDaoImpl(final Class<T> persistentClass,
			EntityManager entityManager) {
		this.persistentClass = persistentClass;
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return this.entityManager.createQuery(
				"select obj from " + this.persistentClass.getName() + " obj  ")
				.getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> getAllDistinct() {
		Collection result = new LinkedHashSet(getAll());
		return new ArrayList(result);
	}

	/**
	 * {@inheritDoc}
	 */
	public T get(PK id) {
		T entity = this.entityManager.find(this.persistentClass, id);

		if (entity == null) {
			String msg = "Uh oh, '" + this.persistentClass
					+ "' object with id '" + id + "' not found...";
			log.warn(msg);
			throw new EntityNotFoundException(msg);
		}

		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean exists(PK id) {
		T entity = this.entityManager.find(this.persistentClass, id);
		return entity != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	public T save(T object) {

		if (StringUtils.isEmpty(object.getStatus())) {
			object.setStatus(RecordStatusEnum.LIVE.name());
		}

		if (object.getVersion() == null) {
			object.setVersion(0);
		}
		return this.entityManager.merge(object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	public void remove(PK id) throws ConstraintViolationException{
		T entity = get(id);
		if (entity != null) {
			log.debug("Removing entity " + id.toString());
			this.entityManager.remove(entity);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	// @SuppressWarnings("unchecked")
	// public T authorize(T object, boolean isApproved) {
	//
	// if (!isApproved) {
	// if (object.getNewValue() == null &&
	// object.getStatus().equals(RecordStatusEnum.INAU.getName()))
	// object.setStatus(RecordStatusEnum.HIST.getName());
	// else {
	// object.setNewValue(null);
	// object.setStatus(RecordStatusEnum.LIVE.getName());
	// }
	//
	// } else {
	// if (object.getStatus().equals(RecordStatusEnum.INAU.getName())) {
	// if (object.getNewValue() != null)
	// object = (T) object.getNewValue();
	// object.setStatus(RecordStatusEnum.LIVE.name());
	// } else if (object.getStatus().equals(RecordStatusEnum.DELAU.getName()))
	// object.setStatus(RecordStatusEnum.HIST.name());
	// }
	// return this.entityManager.merge(object);
	//
	// }

	/**
	 * {@inheritDoc}
	 */

	public List<T> getPagingResults(Long firstResult, Long maxResults) {

		CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = qb.createQuery(this.persistentClass);
		cq.select(cq.from(this.persistentClass));

		return getEntityManager()
				.createQuery(cq)
				.setFirstResult(NumberUtils.toInt(String.valueOf(firstResult)))
				.setMaxResults(
						NumberUtils.toInt(String.valueOf(maxResults),
								Constants.PAGING_MAX_RECORD)).getResultList();
	}

	@SuppressWarnings("rawtypes")
	public Long getRecordCount() {
		CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		Root<T> cc = cq.from(this.persistentClass);
		cq.select(qb.count(cc));
		List list = getEntityManager().createQuery(cq).getResultList();
		return list.size() == 0 ? null : (Long) list.get(0);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> getUnauthList() {
		CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = qb.createQuery(this.persistentClass);
		Root<T> cc = cq.from(this.persistentClass);
		cq.select(cc);

		Expression<String> exp = cc.get("status");
		List<String> stats = new ArrayList<String>();
		stats.add(RecordStatusEnum.INAU.getName());
		stats.add(RecordStatusEnum.DELAU.getName());
		Predicate statusPredicate = exp.in(stats);
		cq.where(statusPredicate);

		List list = getEntityManager().createQuery(cq).getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllLive() {
		return this.entityManager
				.createQuery(
						"select obj from " + this.persistentClass.getName()
								+ " obj where obj.status = :status ")
//								 "(obj.status=:inauStatus and obj.newValue is not null)")
//								"(obj.status=:inauStatus)")
				.setParameter("status", RecordStatusEnum.LIVE.getName())
//				.setParameter("inauStatus", RecordStatusEnum.INAU.getName())
				.getResultList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public T getLiveById(PK id) {

		String query = " select obj from " + this.persistentClass.getName()
				+ " obj where (obj.status = :status or " +
				// " (obj.status=:inauStatus and obj.newValue is not null)) " +
				" (obj.status=:inauStatus)) " + " AND obj.id=:ID ";

		List list = this.entityManager.createQuery(query)
				.setParameter("status", RecordStatusEnum.LIVE.getName())
				.setParameter("inauStatus", RecordStatusEnum.INAU.getName())
				.setParameter("ID", id).getResultList();

		return list.size() == 0 ? null : (T) list.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T authorize(T oldObject, T newObject, boolean isApproved) {
		if (!isApproved) {
			Calendar createdTime = DateUtil.toCalendar(oldObject.getCreatedTime());
			Calendar updatedTime = DateUtil.toCalendar(oldObject.getUpdatedTime());
			if (createdTime.equals(updatedTime) && oldObject.getStatus().equals(RecordStatusEnum.INAU.getName()) && newObject == null){
				this.remove((PK) extractId(oldObject));
				return oldObject;
			} else {
				oldObject.setStatus(RecordStatusEnum.LIVE.getName());
			}

		} else {
			oldObject = newObject;
			if (oldObject.getStatus() == null)
				oldObject.setStatus(RecordStatusEnum.LIVE.name());
			else if (oldObject.getStatus().equals(
					RecordStatusEnum.INAU.getName())) {
				oldObject.setStatus(RecordStatusEnum.LIVE.name());
			} else if (oldObject.getStatus().equals(
					RecordStatusEnum.DELAU.getName()))
				oldObject.setStatus(RecordStatusEnum.HIST.name());
		}
		return this.entityManager.merge(oldObject);
	}

	@Override
	@Transactional
	public void removeAll() {
		List<T> list = getAll();
		for (T t : list) {
			this.entityManager.remove(t);
		}
	}
	
	private Object extractId(Object data) {
		if (!(data instanceof BaseGenericObject))
			throw new RuntimeException("Wrong data type");
		Class<?> clazz = data.getClass();
		Method[] methods = clazz.getMethods();
		for (Method m : methods) {
			if (m.isAnnotationPresent(javax.persistence.Id.class)
					|| m.isAnnotationPresent(javax.persistence.EmbeddedId.class)) {
				try {
					log.debug("Found id in method : " + m.getName());
					return m.invoke(data);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	@Transactional
	public T saveWithAuthorize(T object) {
		object.setStatus(RecordStatusEnum.INAU.getName());
		return this.save(object);
	}

}
