package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.CoreItemType;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 7:47:19 PM
 */
public interface CoreItemTypeDao extends GenericDao<CoreItemType, Long> {
	
}
