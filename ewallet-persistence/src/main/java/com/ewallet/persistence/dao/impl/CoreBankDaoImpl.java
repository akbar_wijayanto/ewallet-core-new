package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.CoreBankDao;
import com.ewallet.persistence.model.CoreBank;

/**
 * @author akbar.wijayanto
 * Date Nov 24, 2015 1:49:13 PM
 */
@Repository("coreBankDao")
public class CoreBankDaoImpl extends GenericDaoImpl<CoreBank, String> implements
		CoreBankDao {

	public CoreBankDaoImpl() {
		super(CoreBank.class);
	}
	
}
