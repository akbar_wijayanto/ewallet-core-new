package com.ewallet.persistence.dao;

import com.ewallet.persistence.model.TrxRefund;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:00:37 PM
 */
public interface TrxRefundDao extends GenericTransactionDao<TrxRefund, Long> {

}
