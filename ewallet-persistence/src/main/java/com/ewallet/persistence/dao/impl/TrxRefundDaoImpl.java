package com.ewallet.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.ewallet.persistence.dao.TrxRefundDao;
import com.ewallet.persistence.model.TrxRefund;

/**
 * @author akbar.wijayanto
 * Date Oct 16, 2015 8:01:43 PM
 */
@Repository
public class TrxRefundDaoImpl extends GenericTransactionDaoImpl<TrxRefund, Long> implements TrxRefundDao {

	public TrxRefundDaoImpl() {
		super(TrxRefund.class);
	}
	
}
