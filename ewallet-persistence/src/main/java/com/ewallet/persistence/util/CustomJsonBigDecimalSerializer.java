package com.ewallet.persistence.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.springframework.context.i18n.LocaleContextHolder;

import com.ewallet.Constants;

public class CustomJsonBigDecimalSerializer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal decimal, JsonGenerator aJsonGenerator, SerializerProvider aSerializerProvider)
            throws IOException, JsonProcessingException {

        Locale locale = LocaleContextHolder.getLocale();
        DecimalFormat format = new DecimalFormat(ResourceBundle.getBundle(Constants.BUNDLE_KEY, locale)
                .getString("currency.format"));
        String dateString = format.format(decimal);
        aJsonGenerator.writeString(dateString);
    }
}
