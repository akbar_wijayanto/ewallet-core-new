/**
 * 
 */
package com.anabatic.retail.adapter.api;

import com.anabatic.retail.dto.AccountDto;
import com.anabatic.retail.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 11:20:01 PM
 */
public interface AccountAdapter {
	
	AccountDto balanceInquiry(String accountNumber) throws RetailException;
	AccountDto createUserAccount(String username) throws RetailException;
	AccountDto updateLimitPayroll(AccountDto dto) throws RetailException;
	AccountDto getLimitPayroll(AccountDto dto) throws RetailException;
}
