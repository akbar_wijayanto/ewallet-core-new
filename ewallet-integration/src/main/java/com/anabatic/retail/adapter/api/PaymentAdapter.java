/**
 * 
 */
package com.anabatic.retail.adapter.api;

import com.anabatic.retail.dto.PaymentDto;
import com.anabatic.retail.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 10:09:18 PM
 */
public interface PaymentAdapter {
	
	PaymentDto createTransaction(PaymentDto paymentDto) throws RetailException;
	PaymentDto refundTransaction(String paymentReference) throws RetailException;
}
