/**
 * 
 */
package com.anabatic.retail.adapter.impl;

import org.springframework.stereotype.Service;

import com.anabatic.kass.service.contract.AccountInformationRequest;
import com.anabatic.kass.service.contract.AccountInformationResponse;
import com.anabatic.kass.service.contract.CreateAccountRequest;
import com.anabatic.kass.service.contract.CreateAccountResponse;
import com.anabatic.kass.service.contract.GetLimitPayrollRequest;
import com.anabatic.kass.service.contract.GetLimitPayrollResponse;
import com.anabatic.kass.service.contract.UpdateLimitPayrollRequest;
import com.anabatic.kass.service.contract.UpdateLimitPayrollResponse;
import com.anabatic.retail.adapter.api.AccountAdapter;
import com.anabatic.retail.dto.AccountDto;
import com.anabatic.retail.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Oct 27, 2015 11:20:18 PM
 */
@Service("accountAdapter")
public class AccountAdapterImpl extends BaseKKASAdapterImpl implements AccountAdapter{

	@Override
	public AccountDto balanceInquiry(String accountNumber) throws RetailException {
		
		AccountInformationRequest request = new AccountInformationRequest();
		request.setAccountNumber(accountNumber);
		
		AccountDto dto = new AccountDto();
		
		try {
			AccountInformationResponse response = getKassService().accountInformation(request);
			if (response != null) {
				dto.setAccountNumber(response.getVitualAccount().getId());
//				dto.setAccountStatus(response.getVitualAccount().getAccountStatus());
				dto.setAccountType(response.getVitualAccount().getVirtualAccountType());
				dto.setAmount(response.getVitualAccount().getAmount());
				dto.setLimitDailyTransaction(response.getVitualAccount().getLimitDailyTransaction());
				dto.setLimitSingletransaction(response.getVitualAccount().getLimitSingleTransaction());
				dto.setLimitTransactionFrequency(response.getVitualAccount().getLimitTransactionFrequency());
				dto.setNickname(response.getVitualAccount().getNickname());
				dto.setCompany(response.getVitualAccount().getCompany());
				dto.setAsMember(response.isAsMember());
			}
			
		} catch (RetailException e) {
			throw new RetailException(e.getMessage());
		}
		
		return dto;
	}

	@Override
	public AccountDto createUserAccount(String username) throws RetailException {

		CreateAccountRequest request = new CreateAccountRequest();
		request.setUsername(username);
		
		AccountDto accountDto = new AccountDto();
		CreateAccountResponse response = getKassService().createAccount(request);
		if (response.isAsMember()) {
			accountDto.setAccountNumber(response.getVitualAccount().getId());
		}
		accountDto.setAsMember(response.isAsMember());
		return accountDto;
	}

	@Override
	public AccountDto updateLimitPayroll(AccountDto dto) throws RetailException {
		
		UpdateLimitPayrollRequest request = new UpdateLimitPayrollRequest();
		request.setAccountNumber(dto.getAccountNumber());
		request.setAllowedLimit(dto.isAllowedPayroll());
		request.setLimitAmount(dto.getLimitPayroll());
		
		AccountDto accountDto = new AccountDto();
		UpdateLimitPayrollResponse response = getKassService().updateLimitPayroll(request);
		if (response != null) {
			accountDto.setAccountNumber(response.getAccountNumber());
			accountDto.setAllowedPayroll(response.isAllowedLimit());
			accountDto.setLimitPayroll(response.getLimitAmount());
		}		
		return accountDto;
	}

	@Override
	public AccountDto getLimitPayroll(AccountDto dto) throws RetailException {

		GetLimitPayrollRequest request = new GetLimitPayrollRequest();
		request.setAccountNumber(dto.getAccountNumber());
		
		AccountDto accountDto = new AccountDto();
		GetLimitPayrollResponse response = getKassService().getLimitPayroll(request);
		if (response != null) {
			accountDto.setAccountNumber(response.getAccountNumber());
			accountDto.setAllowedPayroll(response.isAllowedLimit());
			accountDto.setLimitPayroll(response.getLimitAmount());
		}
		return accountDto;
	}

}
