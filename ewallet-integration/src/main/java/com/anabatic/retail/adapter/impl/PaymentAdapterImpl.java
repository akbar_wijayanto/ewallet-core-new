/**
 * 
 */
package com.anabatic.retail.adapter.impl;

import org.springframework.stereotype.Service;

import com.anabatic.kass.service.contract.PaymentTransactionRequest;
import com.anabatic.kass.service.contract.PaymentTransactionResponse;
import com.anabatic.kass.service.contract.RefundTransactionRequest;
import com.anabatic.kass.service.contract.RefundTransactionResponse;
import com.anabatic.retail.adapter.api.PaymentAdapter;
import com.anabatic.retail.dto.PaymentDto;
import com.anabatic.retail.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 10:15:02 PM
 */
@Service("paymentAdapter")
public class PaymentAdapterImpl extends BaseKKASAdapterImpl implements PaymentAdapter{

	@Override
	public PaymentDto createTransaction(PaymentDto dto)
			throws RetailException {
		
		PaymentTransactionRequest request = new PaymentTransactionRequest();
		request.setAccountFrom(dto.getAccountFrom());
		request.setDescription(dto.getDescription());
		request.setAmount(dto.getAmount());
		
		PaymentDto paymentDto = new PaymentDto();
		
		PaymentTransactionResponse response = getKassService().paymentTransaction(request);
		if (response != null) {
			paymentDto.setReference(response.getTransactionId());
		}
		
		return paymentDto;
	}

	@Override
	public PaymentDto refundTransaction(String paymentReference)
			throws RetailException {

		RefundTransactionRequest request = new RefundTransactionRequest();
		request.setPaymentReference(paymentReference);
		
		PaymentDto paymentDto = new PaymentDto();
		
		RefundTransactionResponse response = getKassService().refundTransaction(request);
		if (response.getRefundId() != null) {
			paymentDto.setReference(response.getRefundId());
		}
		
		return paymentDto;
	}
	
}
