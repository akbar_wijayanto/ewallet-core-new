
package com.anabatic.kass.service.contract.bean;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VirtualAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VirtualAccount">
 *   &lt;complexContent>
 *     &lt;extension base="{http://kass.anabatic.com/service/contract/bean}TransactionWithToken">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nickname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="limitTransactionFrequency" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="limitSingleTransaction" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="limitDailyTransaction" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="accountStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="virtualAccountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VirtualAccount", propOrder = {
    "id",
    "nickname",
    "customerId",
    "amount",
    "limitTransactionFrequency",
    "limitSingleTransaction",
    "limitDailyTransaction",
    "type",
    "accountStatus",
    "virtualAccountType",
    "agentAccount",
    "company"
})
public class VirtualAccount
    extends TransactionWithToken
{

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String nickname;
    @XmlElement(required = true)
    protected String customerId;
    @XmlElement(required = true)
    protected BigDecimal amount;
    @XmlElement(required = true)
    protected BigDecimal limitTransactionFrequency;
    @XmlElement(required = true)
    protected BigDecimal limitSingleTransaction;
    @XmlElement(required = true)
    protected BigDecimal limitDailyTransaction;
    protected int type;
    protected int accountStatus;
    @XmlElement(required = true)
    protected String virtualAccountType;
    protected boolean agentAccount;
    @XmlElement(required = true)
    protected String company;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the nickname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the value of the nickname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickname(String value) {
        this.nickname = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the limitTransactionFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLimitTransactionFrequency() {
        return limitTransactionFrequency;
    }

    /**
     * Sets the value of the limitTransactionFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLimitTransactionFrequency(BigDecimal value) {
        this.limitTransactionFrequency = value;
    }

    /**
     * Gets the value of the limitSingleTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLimitSingleTransaction() {
        return limitSingleTransaction;
    }

    /**
     * Sets the value of the limitSingleTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLimitSingleTransaction(BigDecimal value) {
        this.limitSingleTransaction = value;
    }

    /**
     * Gets the value of the limitDailyTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLimitDailyTransaction() {
        return limitDailyTransaction;
    }

    /**
     * Sets the value of the limitDailyTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLimitDailyTransaction(BigDecimal value) {
        this.limitDailyTransaction = value;
    }

    /**
     * Gets the value of the type property.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }

    /**
     * Gets the value of the accountStatus property.
     * 
     */
    public int getAccountStatus() {
        return accountStatus;
    }

    /**
     * Sets the value of the accountStatus property.
     * 
     */
    public void setAccountStatus(int value) {
        this.accountStatus = value;
    }

    /**
     * Gets the value of the virtualAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualAccountType() {
        return virtualAccountType;
    }

    /**
     * Sets the value of the virtualAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualAccountType(String value) {
        this.virtualAccountType = value;
    }

    /**
     * Gets the value of the agentAccount property.
     * 
     */
    public boolean isAgentAccount() {
        return agentAccount;
    }

    /**
     * Sets the value of the agentAccount property.
     * 
     */
    public void setAgentAccount(boolean value) {
        this.agentAccount = value;
    }

    /**
     * Gets the value of the company property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the value of the company property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

}
