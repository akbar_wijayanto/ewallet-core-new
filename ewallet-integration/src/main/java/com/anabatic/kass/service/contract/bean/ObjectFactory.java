
package com.anabatic.kass.service.contract.bean;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.anabatic.kass.service.contract.bean package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VirtualAccount_QNAME = new QName("http://kass.anabatic.com/service/contract/bean", "VirtualAccount");
    private final static QName _TransactionWithToken_QNAME = new QName("http://kass.anabatic.com/service/contract/bean", "TransactionWithToken");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.anabatic.kass.service.contract.bean
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VirtualAccount }
     * 
     */
    public VirtualAccount createVirtualAccount() {
        return new VirtualAccount();
    }

    /**
     * Create an instance of {@link TransactionWithToken }
     * 
     */
    public TransactionWithToken createTransactionWithToken() {
        return new TransactionWithToken();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VirtualAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kass.anabatic.com/service/contract/bean", name = "VirtualAccount")
    public JAXBElement<VirtualAccount> createVirtualAccount(VirtualAccount value) {
        return new JAXBElement<VirtualAccount>(_VirtualAccount_QNAME, VirtualAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionWithToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kass.anabatic.com/service/contract/bean", name = "TransactionWithToken")
    public JAXBElement<TransactionWithToken> createTransactionWithToken(TransactionWithToken value) {
        return new JAXBElement<TransactionWithToken>(_TransactionWithToken_QNAME, TransactionWithToken.class, null, value);
    }

}
