/**
 * 
 */
package com.ewallet.businesslogic.calculator.balance;

import java.math.BigDecimal;
import java.util.List;


/**
 * 
 * @version 1.0, Aug 26, 2015 2:25:56 PM
 */
public class MinBalanceCalculator implements IBalance {

	@Override
	public BigDecimal calculate(List<BigDecimal> balances) {

		BigDecimal min = balances.get(0);
		for (BigDecimal balance : balances) { 
			if(balance.compareTo(min) < 0) 
				min = balance;
		}
		
		return min;
	}

}
