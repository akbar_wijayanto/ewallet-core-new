/**
 * 
 */
package com.ewallet.businesslogic.calculator.balance;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @version 1.0, Aug 26, 2015 2:52:40 PM
 */
public class MaxBalanceCalculator implements IBalance {

	@Override
	public BigDecimal calculate(List<BigDecimal> balances) {

		BigDecimal max = BigDecimal.ZERO;
		for (BigDecimal balance : balances) { 
			if(balance.compareTo(max) > 0) 
				max = balance;
		}
		
		return max;
	}

}
