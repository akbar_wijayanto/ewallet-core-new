/**
 * 
 */
package com.ewallet.businesslogic.handler.api;

import com.ewallet.exception.RetailException;

/**
 * @author akbar.wijayanto
 * Date Nov 2, 2015 9:45:47 PM
 */
public interface ITransactionHandler<T extends Object> {
	
	public T process(T t) throws RetailException;

}
