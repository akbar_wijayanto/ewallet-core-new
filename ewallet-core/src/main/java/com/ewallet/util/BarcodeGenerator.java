/**
 * 
 */
package com.ewallet.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * @author akbar.wijayanto 
 * Date Oct 19, 2015 1:44:17 PM
 */
public class BarcodeGenerator {

	public static void main(String[] args) {
		String qrCodeData = "Hello World!";
		String filePath = "QRCode.png";
		String charset = "UTF-8"; // or "ISO-8859-1"
		Map hintMap = new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		
		try {
//			createQRCode(qrCodeData, filePath, charset, hintMap, 200, 200);
//			System.out.println("QR Code image created successfully!");
//
//			System.out.println("Data read from QR Code: "
//					+ readQRCode(filePath, charset, hintMap));

			System.out.println("Generate to byte array "+ generateQRCodeToByteArray("081299039695"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	public static void createQRCode(String qrCodeData, String filePath,
			String charset, Map hintMap, int qrCodeheight, int qrCodewidth)
			throws WriterException, IOException {
		BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeData.getBytes(charset), charset),
				BarcodeFormat.QR_CODE, qrCodewidth, qrCodeheight, hintMap);
		MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath.lastIndexOf('.') + 1), new File(filePath));
	}

	public static String readQRCode(String filePath, String charset, Map hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(
						ImageIO.read(new FileInputStream(filePath)))));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap,hintMap);
		return qrCodeResult.getText();
	}

	public static final byte[] generateQRCodeToByteArray(String value) throws WriterException,IOException{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			StringBuffer buffer = new StringBuffer();
			buffer.append(value);
			MatrixToImageWriter.writeToStream(new QRCodeWriter().encode(buffer.toString(),BarcodeFormat.QR_CODE, 400, 400), 
			        "PNG", outputStream);
		return outputStream.toByteArray();
	}
	
}
