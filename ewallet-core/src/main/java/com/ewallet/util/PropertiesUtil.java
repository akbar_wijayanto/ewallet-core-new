package com.ewallet.util;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author <a href="mailto:akbar.wijayanto94@gmail.com">Akbar Wijayanto</a>
 * Date Nov 20, 2016 2:13:00 PM
 */
public class PropertiesUtil {

	private static final String FILENAME = "app";
	
	protected static final Log log = LogFactory.getLog(PropertiesUtil.class);

	public static String get(String key) {
		Properties prop = new Properties();
		String file = FILENAME;
		InputStream input = PropertiesUtil.class.getClassLoader().getResourceAsStream(file+".properties");
		try {
			prop.load(input);
			return prop.getProperty(key);
		} catch (IOException e) {
			if (log.isTraceEnabled()) 
				log.error(e,e);
			else 
				log.error(e);
			return "We are unable to process your request at this point in time. Please call our 24-hour Anabatic Contact Center";
		} finally{
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					if (log.isTraceEnabled()) 
						log.error(e,e);
					else 
						log.error(e);
				}
			}
        }
	}
	
}
