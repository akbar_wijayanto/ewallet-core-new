package com.ewallet.enumeration;

public enum ReligionEnum {
	MOSLEM("MOSLEM"),
	CHRISTIAN("CHRISTIAN"),
	CATHOLIC("CATHOLIC"),
	HINDUISM("HINDUISM"),
	BUDDHISM("BUDDHISM"),
	;
	
	private String name;
	
	
	private ReligionEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
