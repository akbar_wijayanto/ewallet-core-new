package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : <a href="mailto:ab.annas@gmail.com">Andi Baso Annas</a>
 * @version : 1.0, 9/3/12, 5:29 PM
 */
public enum SupportedLocaleEnum {
    en_US("en_US", "English"),
    id_ID("id_ID", "Indonesia");

    private String value;
    private String type;

    private static final Map<String, SupportedLocaleEnum> lookup = new HashMap<String, SupportedLocaleEnum>();
    static {
        for (SupportedLocaleEnum d : SupportedLocaleEnum.values())
            lookup.put(d.getValue(), d);
    }
    private SupportedLocaleEnum(String value, String name) {
        this.value = value;
        this.type = name;
    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public static SupportedLocaleEnum get(String value) {
        return lookup.get(value);
    }
}
