package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum RecordTypeEnum {
	ALL("ALL (*)", "*");
	
	private String recordValue;
	private String recordType;
	
	private RecordTypeEnum(String recordType, String recordValue) {
		this.recordValue = recordValue;
		this.recordType = recordType;
	}

	public String getRecordValue() {
		return recordValue;
	}
	
	public String getRecordType() {
		return recordType;
	}
	
	private static final Map<String, RecordTypeEnum> lookup = new HashMap<String, RecordTypeEnum>();
    static {
        for (RecordTypeEnum d : RecordTypeEnum.values())
            lookup.put(d.getRecordValue(), d);
    }
    
    public static RecordTypeEnum get(String id) {
        return lookup.get(id);
    }
}
