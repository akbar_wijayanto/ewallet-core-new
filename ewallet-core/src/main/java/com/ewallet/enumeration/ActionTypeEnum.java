package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum ActionTypeEnum {
	ALL("ALL (*)", "*"),
	READ("READ", "READ"),
	INPUT("INPUT", "INPUT"),
	EDIT("EDIT", "EDIT"),
	DELETE("DELETE", "DELETE"),
    AUTHORIZE("AUTHORIZE", "AUTHORIZE");

    private String id;
	private String lable;
	
	private ActionTypeEnum(String lable, String id) {
		this.id = id;
		this.lable = lable;
	}

	public String getId() {
		return id;
	}
	
	public String getLable() {
		return lable;
	}
	
	private static final Map<String, ActionTypeEnum> lookup = new HashMap<String, ActionTypeEnum>();
    static {
        for (ActionTypeEnum d : ActionTypeEnum.values())
            lookup.put(d.getId(), d);
    }
    
    public static ActionTypeEnum get(String id) {
        return lookup.get(id);
    }
}
