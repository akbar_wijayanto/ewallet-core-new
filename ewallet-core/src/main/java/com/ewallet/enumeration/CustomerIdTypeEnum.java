/**
 * 
 */
package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author akbar.wijayanto
 * Date Oct 15, 2014 10:56:05 AM
 */
public enum CustomerIdTypeEnum {

	KTP(0, "KTP"),
	SIM(1, "SIM"),
	PASSPORT(2, "PASSPORT"),
	SIUP(3, "SIUP");
	
	
	private int value;
	private String name;
	
	/**
	 * @param value
	 * @param name
	 */
	private CustomerIdTypeEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	private static final Map<Integer, CustomerIdTypeEnum> lookup = new HashMap<Integer, CustomerIdTypeEnum>();
    static {
        for (CustomerIdTypeEnum d : CustomerIdTypeEnum.values())
            lookup.put(d.getValue(), d);
    }
    
    public static CustomerIdTypeEnum get(Integer id) {
        return lookup.get(id);
    }
	
}
