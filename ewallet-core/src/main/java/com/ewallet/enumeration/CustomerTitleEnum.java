package com.ewallet.enumeration;

/**
 * @author akbar.wijayanto
 * Date Oct 19, 2015 1:02:45 PM
 */
public enum CustomerTitleEnum {

	Mr(1, "Mr."), Mrs(2, "Mrs."), Miss(3, "Miss");

	private int statusValue;
	private String statusType;

	private CustomerTitleEnum(int statusValue, String statusType) {
		this.statusType = statusType;
		this.statusValue = statusValue;
	}

	public int getStatusValue() {
		return statusValue;
	}

	public String getStatusType() {
		return statusType;
	}
	
}
