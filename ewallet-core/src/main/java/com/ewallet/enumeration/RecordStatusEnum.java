package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : <a href="mailto:if08007@gmail.com">Arni Sihombing</a>
 * @version : 1.0 2/24/12 7:00 PM
 */
public enum RecordStatusEnum {
    NEW("NEW"),
    HIST("HIST"),
    LIVE("LIVE"),
    ERR("ERROR"),
    INAU("INAU"),
    DELAU("DELAU"),
    INACT("INACT");
    private String name;

    private RecordStatusEnum(String name){
      this.name=name;
    }

    public String getName() {
        return name;
    }
    
    private static final Map<String, RecordStatusEnum> lookup = new HashMap<String, RecordStatusEnum>();
    static {
        for (RecordStatusEnum d : RecordStatusEnum.values())
            lookup.put(d.getName(), d);
    }
    
    public static RecordStatusEnum get(String id) {
        return lookup.get(id);
    }
}
