package com.ewallet.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum BalanceTypeEnum {

	DAILY(0, "DAILY"),
	MINIMUM(1, "MINIMUM"),
//	MAXIMUM(2, "MAXIMUM"),
	AVERAGE(3, "AVERAGE");
	
	private Integer key;
	private String value;
	
	private BalanceTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	
	private static final Map<Integer, BalanceTypeEnum> lookup = new HashMap<Integer, BalanceTypeEnum>();
    static {
        for (BalanceTypeEnum d : BalanceTypeEnum.values())
            lookup.put(d.getKey(), d);
    }
    
    public static BalanceTypeEnum get(String type) {
        return lookup.get(type);
    }
}
