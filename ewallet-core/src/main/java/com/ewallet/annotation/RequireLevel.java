/**
 * 
 */
package com.ewallet.annotation;

import java.lang.annotation.Inherited;

/**Annotation scanned by Aspect for validate level of user
 * 
 * @author agung.kurniawan
 * Date : 9 Okt 2014
 */
@Inherited
public @interface RequireLevel {

}
