package com.ewallet.exception;

import java.util.List;

import com.ewallet.ErrorCode;

public class RetailException extends Exception {
	private static final long serialVersionUID = 4025464027622523453L;
	private List<ErrorCode> errorCodes;

	public RetailException() {
		super();
	}

	public RetailException(String message, Throwable cause) {
		super(message, cause);
	}

	public RetailException(String message) {
		super(message);
	}
	
	public RetailException(List<ErrorCode> errorCodes) {
		this.errorCodes = errorCodes;
	}
	
	public RetailException(Throwable cause) {
		super(cause);
	}
	
	public void setErrorCodes(List<ErrorCode> errorCodes ){
		this.errorCodes = errorCodes;
	}

	/**
	 * @return the errorCodes
	 */
	public List<ErrorCode> getErrorCodes() {
		return errorCodes;
	}

}
