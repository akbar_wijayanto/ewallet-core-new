package com.ewallet.exception;

public class TransactionException extends LoanException {

	private static final long serialVersionUID = -2475913015979783650L;

	public TransactionException(String message) {
        super(message);
    }

    public TransactionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
