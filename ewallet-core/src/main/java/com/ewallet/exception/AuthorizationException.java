/**
 * 
 */
package com.ewallet.exception;

/**
 * @author agung.kurniawan
 * Date : 16 Okt 2014
 */
public class AuthorizationException extends RetailException {

	public AuthorizationException(String message) {
		super(message);
	}

	public AuthorizationException(Throwable cause) {
		super(cause);
	}

}
