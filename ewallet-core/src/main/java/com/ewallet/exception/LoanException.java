package com.ewallet.exception;

public class LoanException extends Exception {

	private static final long serialVersionUID = -2475913015979783650L;

	public LoanException(String message) {
        super(message);
    }

    public LoanException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
