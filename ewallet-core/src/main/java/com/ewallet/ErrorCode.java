package com.ewallet;

import java.io.Serializable;

public class ErrorCode implements Serializable {

	private static final long serialVersionUID = 6790928893042437649L;
	
	private String arg;
	private String key;
	private String defaultMessage;

	public ErrorCode() {
		super();
	}

	public ErrorCode(String arg, String key, String defaultMessage) {
		super();
		this.arg = arg;
		this.key = key;
		this.defaultMessage = defaultMessage;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	/**
	 * @return the arg
	 */
	public String getArg() {
		return arg;
	}

	/**
	 * @param arg the arg to set
	 */
	public void setArg(String arg) {
		this.arg = arg;
	}
}
